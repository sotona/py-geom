#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# geom.py
from collections import namedtuple
from math import log10, sin
import numpy
import scipy.linalg
from scipy.optimize import minimize
from pylab import *

"""
S - естественное давление на глубине залегания выработки
S_p - естественное давление, зависящее от угла (по-умолч боковое?)
kp - коэффициент перегрузки
Em -
Em_p -
SSm_c - предел прочности массива на сжатие
ke - киэффициент длительной прочности
ko - коэффициент структурного ослабления
ksr -
kd -
ku - 
uw -
n,a,b,k - параметры буровзрывных работ;
gamma - собственный вес пород,
G - модуль деформации
ssgp - предельная остаточная прочность [0..1], доли остаточной прочности
theta_l - предельный уровень разрыхления
alpha - [0,1] для теоремы о среднем
K - 2 - сферическая задача, 1 - цилиндр 
С, C1, C2 - наборы констант;  С2 - для условия прочности, C1 - для дилатансионного соотношения.

для ост. инфы смотри метод загрузки данных из файла
"""
# Наяпряжения в МПа, углы в радианах

# старое


# с добавлениями для неосесиметричной задачи
# Shaft = namedtuple('Shaft', 'name S S_p kp Elab Em Em_p Em_ra SSl_c SSl_t ssgp ko ke    SSm_c mu mu_p Ra Rb n a k b gamma G beta fi0 fi1 theta_l T   ksr kd uw C C1 C2         lam')
# старое + неоссим
Shaft = namedtuple('Shaft', 'name S S_p kp Elab Em Em_p Em_ra SSl_c SSl_t ssgp ko ke ku SSm_c mu mu_p Ra Rb n a k b gamma G beta fi0 fi1 theta_l T L ksr kd uw C C1 C2 alpha K lam')


# зачем это тут?
# sh = Shaft(self.name, S, S_p, kp, Elab, Em, Em_p, Er, ssl_c, ssl_t, ssgp, ko, ke, ku, ssm_c, mm, mm / (1.0 - mm),   ra, rb, n, a, k, b,
#                    #                                                            !
#                    gamma=gg, G=G, beta=bb, fi0=ff0, fi1=ff1, theta_l=ttk, T=T, L=tm, ksr=ksr, kd=kd, uw=uw, C=C, C1=C1, C2=C2, alpha = 0.5, 
#                    K = 2,   # кофэ определяющий сферич или плоск. осесимметрич задачу. 1 - сфера, 2 - цилиндр (по-умолч)
#                    lam=lam)

# Detail = namedtuple('Detail', 'r ro drRo Er SScm SSo SSr SSt Tau                    drTau U V EEr EEt drSSr droSSr drU drV TT EEt_k EEt_h ') # Describes Strain-Stress State
Detail = namedtuple('Detail', 'r ro drRo Er SScm SSo SSr SSt Tau SS1 SS3 SSeq SSsub drTau U V EEr EEt drSSr droSSr drU drV TT EEt_k EEt_h ') # Describes Strain-Stress State


eps = 0.000001  # epsilon for eet
table_head = "БВР,    mu,        Ua,        SSR,     SST,        Tau,     EET_k,     EET_h, закон Гука,   E_s,   E_hs"
print_mask = ", %8.4f, %8.4f, %8.4f,%8.4f, %8.6f, %8.6f, %s, %8.2f, %12.11f"
print_mask2 = ", U= %8.4f, SSR= %8.4f, SST= %8.4f,Tau=%8.4f, EET_k=%8.6f, EET_h=%8.6f, %s, E_s=%8.2f, E_h=%12.11f"



def new_NDS_detail(N: int):
    """
    Создаёт именованый кортеж с массивами для хранения НДС. Чтобы везде не писать эту длинную строку
    :return:
    """
    Z = np.zeros(N)
    return Detail(np.array([0]*N),Z.copy(),Z.copy(),Z.copy(),Z.copy(),Z.copy(),Z.copy(), Z.copy(),Z.copy(), Z.copy(), Z.copy(),
               Z.copy(),Z.copy(),Z.copy(),Z.copy(),Z.copy(),Z.copy(),Z.copy(),Z.copy(),Z.copy(), Z.copy(), Z.copy(), Z.copy(), 1,1)



# НДС
class SSS:
    pass


# Горное Давление
class RP:
    pass



def calc_Nz(sh, Rz, N):
    """
    Nz [mm]
    возвращает количество точек в запредельной зоне
    округлебние до большего
    """
    Nz = int(ceil((Rz-sh.Ra)/(sh.Rb - sh.Ra)*N))
    return Nz if Nz != 0 else 1


def func_1d(x, sh, Rz, N, fdm):
    """
    целевая функция нелинейного программирования. p = 0, ssr[b] = s
    :param x:
    :param sh:
    :param Rz:
    :param N:
    :param fdm:
    :return:
    """
    return funcIV_1D(x, sh, N, fdm)


# Sb!=0
def funcIV_1D(x, sh, N, fdm):
    x[1] = 0
    d = fdm(x, sh, sh.Ra, N)
    return (d.SSr[-1] + sh.S)**2 + (d.SSt[-1] + sh.S)**2


# Sb=0, Sa!=0
def funcII_1D_ssr0(x, sh, N, fdm):
    x[1] = sh.S
    d = fdm(x, sh, sh.Ra, N)
    return (d.SSr[-1])**2


def func_1d_ssr0(x, sh, Rz, N, fdm):
    return funcII_1D_ssr0(x, sh, N, fdm)

# целевая функция нелинейного программирования. линейная функция прочности
def func_2d_lin(x, sh, Rz, N, fdm):
    """ Целевая функция: граничное условие на контуре b; линейное условие прочности на контуре r*
    :param x:  [U0, SSr0];
    :param sh:
    :param N:
    :param Rz: Радиус запредельной зоны в [мм]
    :param fdm:
    """
    S = sh.S; d = fdm(x, sh, Rz, N)
    A = d.SSr[-1] + S
    B = d.SSt[0] - sh.beta * d.SSr[0] + d.SScm[0]
    return A**2 + B**2


def func_plain_2(x, sh, Rz, N, fdm):
    d = fdm(x, sh, Rz, N)
    A = d.SSr[-1] + sh.S * (1 - sh.lam)/2
    B = d.SSt[-1] + sh.S * (1 - sh.lam)/2
    C = d.Tau[-1] - sh.S * (1 - sh.lam) / 2

    return A**2 + B**2 + C**2


# целевая функция нелинейного программирования. нелинейная функция прочности
def func_2d_quad(x, sh, Rz, N, fdm):
    """     условие на контуре b; Квадратичная функция прочности;     """
    S = sh.S
    d = fdm(x, sh, Rz, N)
    A = d.SSr[-1] + S
    C = sh.C2
    B = d.SSt[0] + d.SScm[0] - C[1]*d.SSr[0] - C[2]*d.SSr[0]**2
    return A**2 + B**2


def func_plain_2(x, sh, Rz, N, fdm):
    """ Функция прочности для неосесимметричной задачи ??? """
    d = fdm(x, sh, Rz, N)
    A = d.SSr[-1] + sh.S * (1 - sh.lam)/2
    B = d.SSt[-1] + sh.S * (1 - sh.lam)/2
    C = d.Tau[-1] - sh.S * (1 - sh.lam) / 2

    return A**2 + B**2 + C**2


def bur(br, ssl, ra):
    """
    вычисляет параметры буровзрывных работ
    :param br:
    :param ssl:
    :param ra:
    :return:
    """
    M__ = 0.0
    m_ = 0.0
    if (ssl > 20.) and (ssl <= 40.): M__ = 1.8;  m_ = 0.85;
    elif (ssl > 40.) and (ssl <= 60.): M__ = 1.3;  m_ = 0.75;
    elif (ssl > 60.) and (ssl <= 80.): M__ = 1.0;  m_ = 0.70;
    elif (ssl > 80): M__ = 0.9;   m_ = 0.60;
    n = k = 1./log10( 1. + M__ / (ra / 1000.0 * (br/1000.0)**m_))
    b = 0.9
    a = 0.98**n
    return n,a,k,b


def U_I(S,E, mm, r):
    return -S/E * (1 - mm)*r


def U_bak(q, p, E, n, a, r):
    # используется E не для плоской деформации
    return 3 / 2 * (q - p) / E * (n + 2) / (n + 2 - 2 * a) / r


def SStII_bak(q, p, n, a, r):
    return -(p - q) * (n + 2 - 2 * a *(n+1)* r ** (-n)) / (n + 2 - 2 * a) / r ** 2

def SSrII_bak(q, p, n, a, r):
    return (p - q) * (n + 2 - 2 * a * r ** (-n)) / (n + 2 - 2 * a) / r ** 2


def EET(E, sst, ssr, S, mu):
    return 1.0 / E * (sst + S - mu * (ssr + S))


def EER(E, sst, ssr, S, mu):
    return 1 / E * (ssr + S - mu * (sst + S))

def E_plain(Em, mu):
    return Em / (1.0 - mu * mu)

def mu_plain(mu):
    return mu/(1.0-mu)

def ERA(E,a,n,r):
    return E * (1- a* np.power(r,-n))

def ERA_cem(E,a,n,r):
    return E * (1+ a*r**(-n))

def SScm_r(SSm_c, b,k,r):
    return SSm_c*(1.0 - b * (r**(-k)))

def print_all(ssr, sst, ua):
    pass


def calc_hook(ssr, sst, ua, ra, E, mu):
    eet_k = au/ra
    eet_h = 1/E * (sst - mu*ssr)
    return eet_k, eet_h


def calc_bak(sh, N, S):
    global eps


    # d = Detail([sh.Ra] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N,
    #            [0] * N, [0] * N, [0] * N, [0] * N, 1, 1)
    # d = new_NDS_detail(N)

    d = Detail([sh.Ra] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N,
               [0] * N, [0] * N,[0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, 1, 1)

    note = Note(sh)
    dr = (sh.Rb-sh.Ra)/(N-1)
    for i in range(N):
        d.r[i] = sh.Ra + i * dr
        # используется E не для плоской деформации
        d.U[i] = U_bak(-sh.S, 0, sh.Em, sh.n, sh.a, r=d.r[i]/sh.Ra)*sh.Ra
        d.SSr[i] = SSrII_bak(-sh.S, 0, sh.n, sh.a, r=d.r[i]/sh.Ra)-sh.S
        d.SSt[i] = SStII_bak(-sh.S, 0, sh.n, sh.a, r=d.r[i]/sh.Ra)-sh.S
        d.EEt[i] = d.U[i]/sh.Ra
        E = sh.Em_p if sh.a==0 else sh.Em_ra
        d.EEt[i] = EET(E,d.SSt[i],d.SSr[i],S=sh.S, mu=sh.mu)
    # print (note + print_mask%(u0, ssr0+S, sst0+S, eet_k, eet_h, '+' if compare(eet_k, eet_h, eps) else '-', 0, E))
    return d


# исправленный интерфейс, чтобы соответствовал eFDM
# def eFDM_ln(x, sh, Rz, N):
def calc_bak2(x, sh, Rz, N,):
    """
    Решение баклашова
    :param x:  не используется. для совместимости
    :param sh:
    :param Rz: Не используется? для свместимости
    :param N:
    :return:
    """
    global eps

    # d = Detail([sh.Ra] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N, [0] * N,
    #            [0] * N, [0] * N, [0] * N, [0] * N, 1, 1)
    d = new_NDS_detail(N)
    note = Note(sh)
    dr = (sh.Rb - sh.Ra) / (N - 1)
    dr = (sh.Rb - Rz) / (N - 1)
    for i in range(N):
        d.r[i] = sh.Ra + i * dr
        d.U[i] = U_bak(-sh.S, 0, sh.Em, sh.n, sh.a, r=d.r[i]/sh.Ra)*sh.Ra   # Em_p или Em ?
        d.SSr[i] = SSrII_bak(-sh.S, 0, sh.n, sh.a, r=d.r[i]/sh.Ra)-sh.S
        d.SSt[i] = SStII_bak(-sh.S, 0, sh.n, sh.a, r=d.r[i]/sh.Ra)-sh.S
        d.EEt[i] = d.U[i]/sh.Ra
        E = sh.Em_p if sh.a==0 else sh.Em_ra
        d.EEt[i] = EET(E,d.SSt[i],d.SSr[i],S=sh.S, mu=sh.mu)
    # print (note + print_mask%(u0, ssr0+S, sst0+S, eet_k, eet_h, '+' if compare(eet_k, eet_h, eps) else '-', 0, E))
    return d



# calc II, I+II, III
def calc_all_fdm(shoring):
    return detail


# calc II, I+II
def calc_all_bak(shoring):
    return detail


def compare(a,b,eps):
    if a <= b+eps and a >= b-eps:
        return True
    else: return False


def Note(sh):
    return ("    БВР" if sh.n+sh.a+sh.k+sh.b > eps else "без БВР")+",mu=%.3f"%sh.mu


def calc_at_rz(sh, Rz):
    """
    Вычисляет значение некоторых параметров на радиусе. 
    :type Rz: mm
    :return: [p-q , SSrz, SStz, Uz]
    """
    a,n = sh.a,sh.n
    r=Rz/sh.Ra;
    n2a = n + 2. - 2.* sh.a;
    C1 = (n+2.-2.*a*r**(-n))/n2a/(r*r)    #1
    C2 = (n+2.-2.*a*(n+1.)*r**(-n))/n2a/(r*r); #d-0.76
    C3 = (n+2.)/n2a/r; #d1.39
    ss_c8=sh.SSm_c*(1 - sh.b*r**(-n))
    numpy.set_printoptions(precision=4, suppress=True)
    A = numpy.matrix([
    [C1,  1, 0, 0],
    [-C2, 0, 1, 0],
    [-C3/(2*sh.G)*sh.Ra, 0, 0, 1],
    [0, -sh.beta, 1, 0]])
    B = numpy.matrix([[-sh.S], [-sh.S], [0], [-ss_c8]])
    X = numpy.matrix([[0], [0], [0]])
    X = scipy.linalg.solve(A, B)
    return X


def pl_at_r(sh, r, ssr, u, ssm_ci, uz, Rz, Omega, rp=False, rk=False):
    """
    :return: НДС для радиуса r [мм]: [drSSr, theta, SSo, SSt, drU]
    """
    A = numpy.matrix([
        [1,0,0, -1/r  ,0],
        [0,1,0,0, 0 if rk else -sin(sh.fi0)],
        [0,0,1,1,0],
        [0,1,0,0,-1],
        [0,0 if rp else sh.T,1,0,0],
    ])
    B = numpy.matrix([[-ssr/r + sh.gamma*sin(Omega)],
                      [sh.theta_l if rk else sin(sh.fi0) * ( - u/r + 2* uz/Rz )], [sh.beta * ssr], [u/r],
                      [sh.SSm_c*sh.ssgp if rp else ssm_ci]])
    X = numpy.matrix([[0], [0], [0]])
    try:
        X = scipy.linalg.solve(A, B)
    except BaseException as e: print(e)
    x = transpose(X)
    return transpose(X)[0]


# Задача упругости
def el(sh, Rz, Ne, el_sys, NLP_func):
    Xrz = calc_at_rz(sh, Rz)
    x0 = [Xrz[3], Xrz[1]]
    res = minimize(NLP_func, x0, args=(sh, Rz, Ne, el_sys))
    x0 = res.x
    if NLP_func == func_1d: x0[1]=0
    f = NLP_func(x0, sh, Rz, Ne, el_sys)
    return el_sys(x0, sh, Rz, Ne), res


def eFDM_S0(x, sh, Ra, N):
    """     Упругая зона, явный МКР, S=0    """
    u0,ssr0 = x

    # d = Detail([sh.Ra]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0] * N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    # d = new_NDS_detail(N)

    d = Detail([sh.Ra]*N,[0]*N,[0] * N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0] * N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)

    dr = (sh.Rb-sh.Ra)/N
    d.U[0] = u0
    d.SSr[0] = ssr0
    S = 0
    for i in range(0,N-1):
         d.r[i] = sh.Ra + dr*i
         # d.Er[i] = ERA(sh.Em_p,sh.a,sh.n,d.r[i]/sh.Ra)
         d.EEt[i] =  d.U[i] / d.r[i]
         d.SSt[i] = d.EEt[i] * d.Em - S + sh.mu * ( d.SSr[i] + S )
         d.EEr[i] = ( d.SSr[i] + S - sh.mu * ( d.SSt[i] + S ) ) / d.Er[i]
         d.drU[i] = d.EEr[i]
         d.U[i + 1] = d.U[i] + dr * d.drU[i]
         d.drSSr[i] = ( - ( d.SSr[i] - d.SSt[i] ) ) / d.r[i]
         d.SSr[i + 1] = d.SSr[i] + dr * d.drSSr[i]
         d.TT[i] = d.EEr[i] + d.EEt[i]
    return d

def eFDM(x, sh, Rz, N):
    """
    Упругая задача\зона, явный МКР; деформации Коши"""
    u0,ssr0 = x
    # d = Detail([Rz]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0] * N,[0]*N,[0] * N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    d = new_NDS_detail(N)

    # u0 = x[0]
    # ssr0 = 0
    # d = Detail([Rz]*N,[0]*N,[0]*N,[0] * N,[0]*N,[0]*N,[0]*N,[0]*N,[0] * N,[0]*N,[0] * N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)

    dr = (sh.Rb - Rz)/N
    S = sh.S
    d.U[0] = u0
    d.SSr[0] = ssr0
    d.r[-1]=sh.Rb
    for i in range(0,N-1):
         d .r[i] = Rz + dr*i
         d.Er[i] = ERA(sh.Em_p,sh.a,sh.n,d.r[i]/sh.Ra)
         d.EEt[i] =  d.U[i] / d.r[i]
         d.SSt[i] = d.EEt[i] * d.Er[i] - S + sh.mu_p * ( d.SSr[i] + S )
         d.EEr[i] = ( d.SSr[i] + S - sh.mu_p * ( d.SSt[i] + S ) ) / d.Er[i]
         d.drU[i] = d.EEr[i]
         d.U[i + 1] = d.U[i] + dr * d.drU[i]
         d.drSSr[i] = - ( d.SSr[i] - d.SSt[i] ) / d.r[i]
         d.SSr[i + 1] = d.SSr[i] + dr * d.drSSr[i]
         d.TT[i] = d.EEr[i] + d.EEt[i]
         d.SScm[i] = sh.SSm_c*(1.0 - sh.b * (d.r[i]/sh.Ra)**(-sh.k))  # сигма сжатия массива
         d.SSo[i] = d.SScm[i]  # ?
    return d


def eFDM_sph_koshi(x, sh, Rz, N):
    """Упругая задача\зона: деформации Коши. сфера"""
    u0,ssr0 = x
    # d = Detail([Rz]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0] * N,[0]*N,[0] * N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    d = new_NDS_detail(N)
    dr = (sh.Rb - Rz)/N
    S = sh.S
    d.U[0] = u0
    d.SSr[0] = ssr0
    d.r[-1]=sh.Rb

    mu = sh.mu if sh.K == 2 else sh.mu_p
    Em = sh.Em if sh.K == 2 else sh.Em_p
    K1 = 1 - mu * (sh.K - 1)

    for i in range(0,N-1):
         d .r[i] = Rz + dr*i
         d.Er[i] = ERA(sh.Em_p,sh.a,sh.n,d.r[i]/sh.Ra)
         d.EEt[i] =  d.U[i] / d.r[i]
         # d.SSt[i] = d.EEt[i] * d.Er[i] - S + sh.mu_p * ( d.SSr[i] + S )
         d.SSt[i] = 1 / K1 * d.EEt[i] * d.Er[i] - S + 1 / K1 * mu * (d.SSr[i] + S)
         # d.EEr[i] = ( d.SSr[i] + S - sh.mu_p * ( d.SSt[i] + S ) ) / d.Er[i]
         d.EEr[i] = (d.SSr[i] + S - sh.K * mu * (d.SSt[i] + S)) / d.Er[i]
         d.drU[i] = d.EEr[i]
         d.U[i + 1] = d.U[i] + dr * d.drU[i]
         # d.drSSr[i] = - ( d.SSr[i] - d.SSt[i] ) / d.r[i]
         d.drSSr[i] =  sh.K * (d.SSt[i] - d.SSr[i]) / d.r[i]
         d.SSr[i + 1] = d.SSr[i] + dr * d.drSSr[i]
         # d.TT[i] = d.EEr[i] + d.EEt[i]
         d.TT[i] = d.EEr[i] + sh.K * d.EEt[i]
         d.SScm[i] = sh.SSm_c*(1.0 - sh.b * (d.r[i]/sh.Ra)**(-sh.k))  # сигма сжатия массива
    return d


def eFDM_ln(x, sh, Rz, N):
    """Упругая задача\зона: логарифмические деформации"""
    u0,ssr0 = x

    # d = Detail([Rz]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N, [0]*N,[0] * N, [0] * N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    # d = new_NDS_detail(N)

    d = Detail([Rz]*N,[0]*N,[0]*N,[0] * N,[0]*N,[0]*N,[0]*N,[0]*N, [0]*N,[0] * N, [0] * N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)

    dr = (sh.Rb - Rz)/N
    S = sh.S
    d.U[0] = u0
    d.SSr[0] = ssr0
    d.r[N-1]=sh.Rb
    for i in range(0,N-1):
         d.r[i] = Rz + dr*i
         d.Er[i] = ERA(sh.Em_p,sh.a,sh.n,d.r[i]/sh.Ra)
         d.ro[i] = d.r[i] + d.U[i]
         d.EEt[i] = log(d.ro[i]/d.r[i])  # log = ln
         d.SSt[i] = d.EEt[i] * d.Er[i] - S + sh.mu_p * ( d.SSr[i] + S )
         d.EEr[i] = ( d.SSr[i] + S - sh.mu_p * ( d.SSt[i] + S ) ) / d.Er[i]
         d.drRo[i] = exp(d.EEr[i])
         d.drU[i] = d.drRo[i] - 1
         d.U[i + 1] = d.U[i] + dr * d.drU[i]
         d.drSSr[i] = ( d.SSt[i] - d.SSr[i]) / d.ro[i] * d.drRo[i]
         d.SSr[i + 1] = d.SSr[i] + dr * d.drSSr[i]
         d.TT[i] = d.EEr[i] + d.EEt[i]
         d.SScm[i] = sh.SSm_c*(1.0 - sh.b * (d.r[i]/sh.Ra)**(-sh.k))
    return d




def eFDM_sph(x, sh, Rz, N):
    """Упругая задача\зона, явный МКР; логарифмические деформации. Сферическая\цилиндрическая полость"""
    u0,ssr0 = x

    d = new_NDS_detail(N)
    dr = (sh.Rb - Rz)/N
    S = sh.S
    d.U[0] = u0
    d.SSr[0] = ssr0
    d.r[N-1]=sh.Rb

    mu = sh.mu if sh.K == 2 else sh.mu_p
    Em = sh.Em if sh.K == 2 else sh.Em_p

    K1 = 1 - mu * (sh.K - 1)
    for i in range(0,N-1):
         d.r[i] = Rz + dr * i
         d.Er[i] = ERA(Em,sh.a,sh.n,d.r[i]/sh.Ra)
         d.ro[i] = d.r[i] + d.U[i]
         d.EEt[i] = np.log(d.ro[i]/d.r[i])  # log = ln
         # d.SSt[i] = 1/sh.K * d.EEt[i] * d.Er[i] - S + 1/sh.K * sh.mu * ( d.SSr[i] + S )
         d.SSt[i] = 1 / K1 * d.EEt[i] * d.Er[i] - S + 1 / K1 * mu * (d.SSr[i] + S)
         d.EEr[i] = ( d.SSr[i] + S - sh.K * mu * ( d.SSt[i] + S ) ) / d.Er[i]
         d.drRo[i] = np.exp(d.EEr[i])
         d.drU[i] = d.drRo[i] - 1
         d.U[i + 1] = d.U[i] + dr * d.drU[i]
         d.drSSr[i] =  sh.K * ( d.SSt[i] - d.SSr[i]) / d.ro[i] * d.drRo[i]
         d.SSr[i + 1] = d.SSr[i] + dr * d.drSSr[i]
         d.TT[i] = d.EEr[i] + sh.K*d.EEt[i]
         # d.SScm[i] = sh.SSm_c*(1.0 - sh.b * (d.r[i]/sh.Ra)**(-sh.k))
         d.SScm[i] = sh.SSm_c*(1.0 - sh.b * np.power(d.r[i]/sh.Ra, -sh.k))
         d.SSo[i] = d.SScm[i]  # ?
    return d



# def eFDM_plain(x, sh, Rz, N):
#     """Упругая задача\зона: плоская задача"""
#     u0,ssr0 = x
#     # d = Detail([Rz]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0] * N, [0] * N,[0]*N,[0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
#     d = new_NDS_detail(N)
#     dr = (sh.Rb - Rz)/N
# ???


def eFDM_plain_2(x, sh, Rz, N):
    """     Упругая задача\зона, явный МКР; плоская задача (неосесимметричная);     """
    u0,v0 = x
    d = Detail([Rz]*N,[0]*N,[0]*N,[0] * N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0] * N, [0] * N,[0]*N,[0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    dr = (sh.Rb - Rz) / N

    S = sh.S

    lam =sh.lam
    d.SSr[0] = 0
    d.Tau[0] = 0
    d.U[0] = u0
    d.V[0] = v0
    d.r[N-1]=sh.Rb
    for i in range(0,N-1):

        d.Er[i] = ERA_cem(sh.Em_p, sh.a, sh.n, d.r[i] / sh.Ra)

        d.r[i] = Rz + dr * i
        r= d.r[i]
        d.SSt[i] = S * (1-lam) / 2 + d.Er[i] * (d.U[i] / r + 2 / r * d.V[i] )+ sh.mu_p*(d.SSr[i]+S*(1-lam)/2)
        d.drV[i] = 2 * (1 + sh.mu_p) / d.Er[i] * ( d.Tau[i] - S * (1 - lam) / 2 ) + d.V[i] / r + 2/r * d.U[i]
        d.drU[i] = 1 / d.Er[i] * ( ( d.SSr[i] + S * (1-lam) / 2) - sh.mu_p * (d.SSt[i] - S * (1-lam) / 2) )

        d.drSSr[i] = - 2 / r * d.Tau[i] - (d.SSr[i] - d.SSt[i]) / r
        d.drTau[i] = 2 / r * d.SSt[i] - 2/r*d.Tau[i]

        d.U[i + 1] = d.U[i] + dr * d.drU[i]
        d.V[i + 1] = d.V[i] + dr * d.drV[i]
        d.SSr[i + 1] = d.SSr[i] + dr * d.drSSr[i]
        d.Tau[i + 1] = d.Tau[i] + dr * d.drTau[i]

    return d

def pFDM(sh, D, Rz, N, omega):
    """
    Дополняет массивы D данными из запредельной зоны, просто старая версия???
    :param sh: Shaft
    :param D: elastic Details (arrays)
    :param Rz: radius of plastic region
    :param omega: угол [Rad]
    :return: D with plastic data
    """
    # D = Detail([sh.Ra]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    Iz = int(N*(Rz-sh.Ra)/sh.Rb)  # первая точка упругой области
    Rz = sh.Ra + (sh.Rb-sh.Ra)*N/Iz
    dr = (Rz-sh.Ra)/Iz

    thetta_l = 0.12
    thetta_z = D.EEr[Iz]+D.EEt[Iz]


    for i in range(Iz-1,0,-1):
        D.drSSr[i] = - (D.SSr[i]-D.SSt[i])/D.r[i] + sh.gamma*sin(omega);
        D.U[i-1]   = D.U[i]   - dr*D.drU[i]
        D.SSr[i-1] = D.SSr[i] - dr*D.drSSr[i]
        if (D.TT[i] >= thetta_l - 1e-10): D.drU[i-1] = -D.u[i-1]/D.r[i-1] + thetta_l;
        else: D.drU[i-1] = -D.U[i-1]/D.r[i-1] + thetta_z + \
                                    (1-m.c[1]-2*m.c[2]*D.SSr[i-1])*(D.U[i-1]/D.r[i-1]-eetz);
        D.EEr[i-1] = D.drU[i-1];
        D.EEt[i-1] =  D.U[i-1] / D.r[i-1];

        D.TT[i-1] = D.drU[i-1] + D.U[i-1]/D.r[i-1];
        if (D.TT[i-1]>thetta_l):
            D.TT[i-1] = thetta_l;
            if (rk):
                sh.rk=D.r[i-1]
                rk=0
        D.SSo[i-1] = m.SSm - m.T*(D.TT[i-1]-thetta_z)
        if D.SSo[i-1] < 0:
            D.SSo[i-1] = 0
            if rp:
                sh.rp = D.r[i-1]
                rp=0
        D.SSt[i-1]= - D.SSo[i-1] + m.c[1]*D.SSr[i-1]+m.c[2]*D.SSr[i-1]*D.SSr[i-1];


def pFDM2_ln(sh, ed, Rz, N, omega):
    """Запредельная зона: нелинейная функция разупрочнения, логарифмические деформации
    Дополняет массивы ed данными из запредельной зоны,
    :param sh: Shaft
    :param ed: elastic Details (arrays)
    :param Rz: radius of plastic region
    :param N: число точек в запредельной зоне
    :param omega: угол [Rad]
    :return: ed with plastic data
    """
    # Nz = calc_Nz(sh, Rz, N)

    # pd = Detail([Rz]*N,[0]*N,[0]*N,[0]*N,[0]*N, [0]*N,[0] * N, [0]*N,[0]*N, [0] * N, [0]*N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    # d = new_NDS_detail(N)

    pd = Detail([Rz]*N,[0]*N,[0]*N,[0] * N,[0]*N,[0]*N, [0]*N,[0] * N, [0]*N,[0]*N, [0] * N, [0]*N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)

    dr = (Rz-sh.Ra)/(N-1) if N>1 else 0
    C = sh.C1

    # условие непрерывности
    pd.U[N-1] = ed.U[0]
    pd.SSr[N-1] = ed.SSr[0]
    pd.SSt[N-1] = ed.SSt[0]
    pd.EEr[N-1] = ed.EEr[0]

    pd.drRo[N-1]  = exp(pd.EEr[N-1])
    pd.drU[N-1]   = pd.drRo[N-1] - 1
    pd.ro[N-1]    = pd.U[N-1] + Rz
    pd.r[N-1] = sh.Ra + dr * (N-1)
    pd.drSSr[N-1] = ( (pd.SSt[N-1] + pd.SSr[N-1])/pd.ro[N-1] + sh.gamma * sin(omega) ) * pd.drRo[N-1]
    pd.TT[N-1] = ed.EEr[0]+ed.EEt[0]
    pd.SSo[N-1] = pd.SScm[N-1] - sh.T*pd.TT[N-1]

    EEtz = ed.EEt[0]
    thetta_l = sh.theta_l
    thetta_z = ed.EEr[0]+ed.EEt[0]

    rk, rp = 0, 0
    for i in range(N-1,0,-1):
        pd.r[i-1] = sh.Ra + dr*(i-1)
        pd.U[i-1]   = pd.U[i]   - dr*pd.drU[i]
        pd.SSr[i-1] = pd.SSr[i] - dr*pd.drSSr[i]
        pd.ro[i-1] = pd.r[i-1] + pd.U[i-1]
        pd.EEt[i-1] = log(pd.ro[i-1]/pd.r[i-1])  # log = ln
        pd.TT[i-1]  = (1 - C[1] - 2 * C[2] * pd.SSr[i-1])*(pd.EEt[i-1] - EEtz)
        if (pd.TT[i-1] >= thetta_l - 1e-10):
            pd.TT[i - 1] = thetta_l
            if rk==0: rk=pd.r[i-1]
        pd.EEr[i-1] = pd.TT[i-1] - pd.EEt[i-1]
        pd.SScm[i-1] = sh.SSm_c*(1.0 - sh.b * (pd.r[i-1]/sh.Ra)**(-sh.k))
        pd.SSo[i-1] = pd.SScm[i-1] - sh.T*pd.TT[i-1]
        if pd.SSo[i-1] <= sh.ssgp*sh.SSm_c:
            pd.SSo[i-1] = sh.ssgp*sh.SSm_c
            if rp==0: rp=pd.r[i-1]
        pd.SSt[i-1]= - pd.SSo[i-1] + sh.C[1]*pd.SSr[i-1]+sh.C[2]*pd.SSr[i-1]**2;
        pd.drRo[i-1] = exp(pd.EEr[i-1])
        pd.drU[i-1]   = pd.drRo[i-1] - 1
        pd.drSSr[i-1] = ( (pd.SSt[i-1] - pd.SSr[i-1])/pd.ro[i-1] + sh.gamma * sin(omega) ) * pd.drRo[i-1]
    return pd, rk, rp


def pFDM2_ln_spher_t(sh, ed, Rz, N, omega):
    """
    запредельная зона: нелинейная функция разупрочнения, лог. деформации. Сфер.\цилиндр. полость. T зависит от радиуса
    :param sh: Shaft
    :param ed: elastic Details (arrays)
    :param Rz: radius of plastic region
    :param N: число точек в запредельной зоне
    :param omega: угол [Rad]
    :return: ed with plastic data
    """
    # Nz = calc_Nz(sh, Rz, N)
    # pd = Detail([Rz]*N,[0]*N,[0]*N,[0]*N,[0]*N, [0]*N,[0] * N, [0]*N,[0]*N, [0] * N, [0]*N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    pd = new_NDS_detail(N)
    dr = (Rz-sh.Ra)/(N-1) if N>1 else 0
    C = sh.C1

    # K = 2  # 2- spher;  1 -cyl
    # alpha = 0.75

    # условие непрерывности
    pd.U[N-1] = ed.U[0]
    pd.SSr[N-1] = ed.SSr[0]
    pd.SSt[N-1] = ed.SSt[0]
    pd.EEr[N-1] = ed.EEr[0]

    pd.drRo[N-1]  = exp(pd.EEr[N-1])
    pd.drU[N-1]   = pd.drRo[N-1] - 1
    pd.ro[N-1]    = pd.U[N-1] + Rz
    pd.r[N-1]     = sh.Ra + dr * (N-1)
    pd.drSSr[N-1] = ( sh.K * (pd.SSt[N-1] + pd.SSr[N-1])/pd.ro[N-1] + sh.gamma * sin(omega) ) * pd.drRo[N-1]

    EEtz = ed.EEt[0]
    thetta_l = sh.theta_l
    thetta_z = ed.EEr[0]+ed.EEt[0]

    C1s = C[1] + 2 * C[2] * pd.SSr[N-1] * (1 - sh.alpha)
    C2s = C[2] * sh.alpha



    rk, rp = 0, 0
    for i in range(N-1,0,-1):

        pd.r[i-1] = sh.Ra + dr*(i-1)
        pd.U[i-1]   = pd.U[i]   - dr*pd.drU[i]
        pd.SSr[i-1] = pd.SSr[i] - dr*pd.drSSr[i]
        pd.ro[i-1] = pd.r[i-1] + pd.U[i-1]
        pd.EEt[i-1] = log(pd.ro[i-1]/pd.r[i-1])  # log = ln
        pd.TT[i-1]  = (1 - C1s - 2 * C2s * pd.SSr[i-1])*sh.K*(pd.EEt[i-1] - EEtz)
        if (pd.TT[i-1] >= thetta_l - 1e-10):
            pd.TT[i-1] = thetta_l
            if rk==0: rk=pd.r[i-1]
        pd.EEr[i-1] = pd.TT[i-1] - sh.K * pd.EEt[i-1]
        pd.SScm[i-1] = sh.SSm_c*(1.0 - sh.b * (pd.r[i-1]/sh.Ra)**(-sh.k))

        T = (pd.SScm[i-1] - sh.ssgp*sh.SSm_c) / thetta_l

        pd.SSo[i-1] = pd.SScm[i-1] - T * pd.TT[i-1]

        if pd.SSo[i-1] <= sh.ssgp*sh.SSm_c:
            pd.SSo[i-1] = sh.ssgp*sh.SSm_c
            if rp==0: rp=pd.r[i-1]
        pd.SSt [i-1]= - pd.SSo[i-1] + C[1]*pd.SSr[i-1]+C[2]*pd.SSr[i-1]**2
        pd.drRo[i-1] = exp(pd.EEr[i-1])
        pd.drU [i-1]   = pd.drRo[i-1] - 1
        pd.drSSr[i-1] = ( sh.K*(pd.SSt[i-1] - pd.SSr[i-1])/pd.ro[i-1] + sh.gamma * sin(omega) ) * pd.drRo[i-1]
    return pd, rk, rp


def pFDM2_ln_spher(sh, ed, Rz, N, omega):
    """
запредельная зона: нелинейная функция разупрочнения, логарифмические деформации. Сферическая\цилиндрическая полость
    :param sh: Shaft
    :param ed: elastic Details (arrays)
    :param Rz: radius of plastic region
    :param N: число точек в запредельной зоне
    :param omega: угол [Rad]
    :return: ed with plastic data
    """
    # Nz = calc_Nz(sh, Rz, N)
    # pd = Detail([Rz]*N,[0]*N,[0]*N,[0]*N,[0]*N, [0]*N,[0] * N, [0]*N,[0]*N, [0] * N, [0]*N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    pd = new_NDS_detail(N)
    dr = (Rz-sh.Ra)/(N-1) if N>1 else 0
    C = sh.C1

    # K = 2  # 2- spher;  1 -cyl
    # alpha = 0.75

    # условие непрерывности
    pd.U[N-1] = ed.U[0]
    pd.SSr[N-1] = ed.SSr[0]
    pd.SSt[N-1] = ed.SSt[0]
    pd.EEr[N-1] = ed.EEr[0]

    pd.drRo[N-1]  = exp(pd.EEr[N-1])
    pd.drU[N-1]   = pd.drRo[N-1] - 1
    pd.ro[N-1]    = pd.U[N-1] + Rz
    pd.r[N-1]     = sh.Ra + dr * (N-1)
    pd.drSSr[N-1] = ( sh.K * (pd.SSt[N-1] + pd.SSr[N-1])/pd.ro[N-1] + sh.gamma * sin(omega) ) * pd.drRo[N-1]

    EEtz = ed.EEt[0]
    thetta_l = sh.theta_l
    thetta_z = ed.EEr[0]+ed.EEt[0]

    C1s = C[1] + 2 * C[2] * pd.SSr[N-1] * (1 - sh.alpha)
    C2s = C[2] * sh.alpha

    rk, rp = 0, 0
    for i in range(N-1,0,-1):

        pd.r[i-1] = sh.Ra + dr*(i-1)
        pd.U[i-1]   = pd.U[i]   - dr*pd.drU[i]
        pd.SSr[i-1] = pd.SSr[i] - dr*pd.drSSr[i]
        pd.ro[i-1] = pd.r[i-1] + pd.U[i-1]
        pd.EEt[i-1] = log(pd.ro[i-1]/pd.r[i-1])  # log = ln
        pd.TT[i-1]  = (1 - C1s - 2 * C2s * pd.SSr[i-1])*sh.K*(pd.EEt[i-1] - EEtz)
        if (pd.TT[i-1] >= thetta_l - 1e-10):
            pd.TT[i-1] = thetta_l
            if rk==0: rk=pd.r[i-1]
        pd.EEr[i-1] = pd.TT[i-1] - sh.K * pd.EEt[i-1]
        pd.SScm[i-1] = sh.SSm_c*(1.0 - sh.b * (pd.r[i-1]/sh.Ra)**(-sh.k))
        pd.SSo[i-1] = pd.SScm[i-1] - sh.T*pd.TT[i-1]
        if pd.SSo[i-1] <= sh.ssgp*sh.SSm_c:
            pd.SSo[i-1] = sh.ssgp*sh.SSm_c
            if rp==0: rp=pd.r[i-1]
        pd.SSt [i-1]= - pd.SSo[i-1] + C[1]*pd.SSr[i-1]+C[2]*pd.SSr[i-1]**2
        pd.drRo[i-1] = exp(pd.EEr[i-1])
        pd.drU [i-1]   = pd.drRo[i-1] - 1
        pd.drSSr[i-1] = ( sh.K*(pd.SSt[i-1] - pd.SSr[i-1])/pd.ro[i-1] + sh.gamma * sin(omega) ) * pd.drRo[i-1]
    return pd, rk, rp


def pFDM2_ln_spher_a(sh, ed, Rz, N, omega):
    """
Запредельная зона: нелинейная функция разупрочнения, логарифмические деформации. Сферическая\цилиндрическая полость. С учётом deltaT*
    :param sh: Shaft
    :param D: elastic Details (arrays)
    :param Rz: radius of plastic region
    :param N: число точек в запредельной зоне
    :param omega: угол [Rad]
    :return: D with plastic data
    """
    # Nz = calc_Nz(sh, Rz, N)
    # pd = Detail([Rz]*N,[0]*N,[0]*N,[0]*N,[0]*N, [0]*N,[0] * N, [0]*N,[0]*N, [0] * N, [0]*N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    pd = new_NDS_detail(N)
    dr = (Rz-sh.Ra)/(N-1) if N>1 else 0
    C = sh.C1

    # K = 2  # 2- spher;  1 -cyl
    # alpha = 0.75

    # условие непрерывности
    pd.U[N-1] = ed.U[0]
    pd.SSr[N-1] = ed.SSr[0]
    pd.SSt[N-1] = ed.SSt[0]
    pd.EEr[N-1] = ed.EEr[0]

    pd.drRo[N-1]  = exp(pd.EEr[N-1])
    pd.drU[N-1]   = pd.drRo[N-1] - 1
    pd.ro[N-1]    = pd.U[N-1] + Rz
    pd.r[N-1]     = sh.Ra + dr * (N-1)
    pd.drSSr[N-1] = ( sh.K * (pd.SSt[N-1] + pd.SSr[N-1])/pd.ro[N-1] + sh.gamma * sin(omega) ) * pd.drRo[N-1]

    pd.SScm[-1] = sh.SSm_c * (1.0 - sh.b * (Rz / sh.Ra) ** (-sh.k))
    pd.SSo[-1] = pd.SScm[-1]
    pd.TT[-1] = ed.EEr[0]+ed.EEt[0]

    EEtz = ed.EEt[0]
    thetta_l = sh.theta_l
    TTz = ed.EEr[0]+ed.EEt[0]

    C1s = C[1] + 2 * C[2] * pd.SSr[N-1] * (1 - sh.alpha)
    C2s = C[2] * sh.alpha

    beta = ( ed.SSt[0] + sh.SSm_c ) / ed.SSr[0]
    sinfi = (beta - 1) / (beta + 1)
    # Em = 0.75 * (0.0168 + 0.733 * ko + 0.119 * ko ** 2) * Elab
    tm = (2.5 + sh.ku) *  sh.Em * (0.5 - sh.mu) / (0.5 - 0.2) * sinfi / sin(pi/4)  # L
    T =  tm / (beta - 1)

    rk, rp = 0, 0
    for i in range(N-1,0,-1):

        pd.r[i-1] = sh.Ra + dr*(i-1)
        pd.U[i-1]   = pd.U[i]   - dr*pd.drU[i]
        pd.SSr[i-1] = pd.SSr[i] - dr*pd.drSSr[i]
        pd.ro[i-1] = pd.r[i-1] + pd.U[i-1]
        pd.EEt[i-1] = log(pd.ro[i-1]/pd.r[i-1])  # log = ln
        pd.TT[i-1]  = TTz + (1 - C1s - 2 * C2s * pd.SSr[i-1])*sh.K*(pd.EEt[i-1] - EEtz)
        if (pd.TT[i-1] >= thetta_l - 1e-10):
            pd.TT[i-1] = thetta_l
            if rk==0: rk=pd.r[i-1]
        pd.EEr[i-1] = pd.TT[i-1] - sh.K * pd.EEt[i-1]
        pd.SScm[i-1] = sh.SSm_c*(1.0 - sh.b * (pd.r[i-1]/sh.Ra)**(-sh.k))
        pd.SSo[i-1] = pd.SScm[i-1] - sh.T* (pd.TT[i-1] - TTz)
        if pd.SSo[i-1] <= sh.ssgp*sh.SSm_c:
            pd.SSo[i-1] = sh.ssgp*sh.SSm_c
            if rp==0: rp=pd.r[i-1]
        pd.SSt [i-1]= - pd.SSo[i-1] + C[1]*pd.SSr[i-1]+C[2]*pd.SSr[i-1]**2
        pd.drRo[i-1] = exp(pd.EEr[i-1])
        pd.drU [i-1]   = pd.drRo[i-1] - 1
        pd.drSSr[i-1] = ( sh.K*(pd.SSt[i-1] - pd.SSr[i-1])/pd.ro[i-1] + sh.gamma * sin(omega) ) * pd.drRo[i-1]

    return pd, rk, rp



def pFDM21_ln_spher_a(sh, ed, Rz, N, omega):
    """
Запредельная зона: нелинейная функция разупрочнения, логарифмические деформации. Сферическая\цилиндрическая полость. С учётом deltaT*
Исправлено условие непрерывности
    :param sh: Shaft
    :param D: elastic Details (arrays)
    :param Rz: radius of plastic region
    :param N: число точек в запредельной зоне
    :param omega: угол [Rad]
    :return: D with plastic data
    """
    # Nz = calc_Nz(sh, Rz, N)
    # pd = Detail([Rz]*N,[0]*N,[0]*N,[0]*N,[0]*N, [0]*N,[0] * N, [0]*N,[0]*N, [0] * N, [0]*N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    pd = new_NDS_detail(N)
    dr = (Rz-sh.Ra)/(N-1) if N>1 else 0
    C = sh.C1

    # K = 2  # 2- spher;  1 -cyl
    # alpha = 0.75

    # условие непрерывности
    pd.U[N-1] = ed.U[0]
    pd.SSr[N-1] = ed.SSr[0]
    pd.SSt[N-1] = ed.SSt[0]
    pd.EEt[N - 1] = ed.EEt[0]
    pd.EEr[N - 1] = - pd.EEt[N - 1]    # дополнение для статьи ГИАБ

    pd.drRo[N-1]  = exp(pd.EEr[N-1])
    pd.drU[N-1]   = pd.drRo[N-1] - 1
    pd.ro[N-1]    = pd.U[N-1] + Rz
    pd.r[N-1]     = sh.Ra + dr * (N-1)
    pd.drSSr[N-1] = ( sh.K * (pd.SSt[N-1] + pd.SSr[N-1])/pd.ro[N-1] + sh.gamma * sin(omega) ) * pd.drRo[N-1]

    pd.SScm[-1] = sh.SSm_c * (1.0 - sh.b * (Rz / sh.Ra) ** (-sh.k))
    pd.SSo[-1] = pd.SScm[-1]
    pd.TT[-1] = 0  # ed.EEr[0]+ed.EEt[0]   

    EEtz = ed.EEt[0]
    thetta_l = sh.theta_l
    TTz = 0  # ed.EEr[0]+ed.EEt[0]  # как в статье для ГИАБ 2020. tt*=0

    # C1s = C[1] + 2 * C[2] * pd.SSr[N-1] * (1 - sh.alpha)
    # C2s = C[2] * sh.alpha

    C1s = C[1]
    C2s = C[2]

    # beta = ( ed.SSt[0] + sh.SSm_c ) / ed.SSr[0]
    # sinfi = (beta - 1) / (beta + 1)
    # Em = 0.75 * (0.0168 + 0.733 * ko + 0.119 * ko ** 2) * Elab
    # tm = (2.5 + sh.ku) *  sh.Em * (0.5 - sh.mu) / (0.5 - 0.2) * sinfi / sin(pi/4)  # L
    # T =  tm / (beta - 1)

    rk, rp = 0, 0
    for i in range(N-1,0,-1):
        pd.r[i-1] = sh.Ra + dr*(i-1)
        pd.U[i-1]   = pd.U[i]   - dr*pd.drU[i]
        pd.SSr[i-1] = pd.SSr[i] - dr*pd.drSSr[i]
        pd.ro[i-1] = pd.r[i-1] + pd.U[i-1]
        pd.EEt[i-1] = np.log(pd.ro[i-1]/pd.r[i-1])  # log = ln
        pd.TT[i-1]  = TTz + (1 - C1s - 2 * C2s * pd.SSr[i-1])*sh.K*(pd.EEt[i-1] - EEtz)
        if (pd.TT[i-1] >= thetta_l - 1e-10):
            pd.TT[i-1] = thetta_l
            if rk==0: rk=pd.r[i-1]
        pd.EEr[i-1] = pd.TT[i-1] - sh.K * pd.EEt[i-1]
        pd.SScm[i-1] = sh.SSm_c*(1.0 - sh.b * (pd.r[i-1]/sh.Ra)**(-sh.k))
        pd.SSo[i-1] = pd.SScm[i-1] - sh.T* (pd.TT[i-1] - TTz)
        if pd.SSo[i-1] <= sh.ssgp*sh.SSm_c:
            pd.SSo[i-1] = sh.ssgp*sh.SSm_c
            if rp==0: rp=pd.r[i-1]
        pd.SSt [i-1]= - pd.SSo[i-1] + C[1]*pd.SSr[i-1]+C[2]*pd.SSr[i-1]**2
        pd.drRo[i-1] = np.exp(pd.EEr[i-1])
        pd.drU [i-1]   = pd.drRo[i-1] - 1
        pd.drSSr[i-1] = ( sh.K*(pd.SSt[i-1] - pd.SSr[i-1])/pd.ro[i-1] + sh.gamma * sin(omega) ) * pd.drRo[i-1]

    return pd, rk, rp



def pFDM21_ln_spher_a_beta(sh, ed, Rz, N, omega):
    """
Запредельная зона: нелинейная функция разупрочнения, логарифмические деформации. Сферическая\цилиндрическая полость. С учётом deltaT*
Исправлено условие непрерывности. Вычисляет beta из системы уравнений (линейный случай)
    :param sh: Shaft
    :param D: elastic Details (arrays)
    :param Rz: radius of plastic region
    :param N: число точек в запредельной зоне
    :param omega: угол [Rad]
    :return: D with plastic data
    """
    # Nz = calc_Nz(sh, Rz, N)
    # pd = Detail([Rz]*N,[0]*N,[0]*N,[0]*N,[0]*N, [0]*N,[0] * N, [0]*N,[0]*N, [0] * N, [0]*N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    pd = new_NDS_detail(N)
    dr = (Rz-sh.Ra)/(N-1) if N>1 else 0
    C = sh.C1

    # K = 2  # 2- spher;  1 -cyl
    # alpha = 0.75

    # условие непрерывности
    pd.U[N-1] = ed.U[0]
    pd.SSr[N-1] = ed.SSr[0]
    pd.SSt[N-1] = ed.SSt[0]
    pd.EEt[N - 1] = ed.EEt[0]
    pd.EEr[N - 1] = - pd.EEt[N - 1]    # дополнение для статьи ГИАБ

    pd.drRo[N-1]  = exp(pd.EEr[N-1])
    pd.drU[N-1]   = pd.drRo[N-1] - 1
    pd.ro[N-1]    = pd.U[N-1] + Rz
    pd.r[N-1]     = sh.Ra + dr * (N-1)
    pd.drSSr[N-1] = ( sh.K * (pd.SSt[N-1] + pd.SSr[N-1])/pd.ro[N-1] + sh.gamma * sin(omega) ) * pd.drRo[N-1]

    pd.SScm[-1] = sh.SSm_c * (1.0 - sh.b * (Rz / sh.Ra) ** (-sh.k))
    pd.SSo[-1] = pd.SScm[-1]
    pd.TT[-1] = 0  # ed.EEr[0]+ed.EEt[0]

    EEtz = ed.EEt[0]
    thetta_l = sh.theta_l
    TTz = 0  # ed.EEr[0]+ed.EEt[0]  # как в статье для ГИАБ 2020. tt*=0


    beta = ( ed.SSt[0] + sh.SSm_c ) / ed.SSr[0]
    sinfi = (beta - 1) / (beta + 1)
    # Em = 0.75 * (0.0168 + 0.733 * ko + 0.119 * ko ** 2) * Elab
    tm = (2.5 + sh.ku) *  sh.Em * (0.5 - sh.mu) / (0.5 - 0.2) * sinfi / sin(pi/4)  # L
    T =  tm / (beta - 1)

    C1s = C[1] + 2 * C[2] * pd.SSr[N - 1] * (1 - sh.alpha)
    C1s = beta
    C2s = C[2] * sh.alpha
    C2s = 0

    rk, rp = 0, 0
    for i in range(N-1,0,-1):
        pd.r[i-1] = sh.Ra + dr*(i-1)
        pd.U[i-1]   = pd.U[i]   - dr*pd.drU[i]
        pd.SSr[i-1] = pd.SSr[i] - dr*pd.drSSr[i]
        pd.ro[i-1] = pd.r[i-1] + pd.U[i-1]
        pd.EEt[i-1] = log(pd.ro[i-1]/pd.r[i-1])  # log = ln
        pd.TT[i-1]  = TTz + (1 - C1s - 2 * C2s * pd.SSr[i-1])*sh.K*(pd.EEt[i-1] - EEtz)
        if (pd.TT[i-1] >= thetta_l - 1e-10):
            pd.TT[i-1] = thetta_l
            if rk==0: rk=pd.r[i-1]
        pd.EEr[i-1] = pd.TT[i-1] - sh.K * pd.EEt[i-1]
        pd.SScm[i-1] = sh.SSm_c*(1.0 - sh.b * (pd.r[i-1]/sh.Ra)**(-sh.k))
        pd.SSo[i-1] = pd.SScm[i-1] - sh.T* (pd.TT[i-1] - TTz)
        if pd.SSo[i-1] <= sh.ssgp*sh.SSm_c:
            pd.SSo[i-1] = sh.ssgp*sh.SSm_c
            if rp==0: rp=pd.r[i-1]
        pd.SSt [i-1]= - pd.SSo[i-1] + C[1]*pd.SSr[i-1]+C[2]*pd.SSr[i-1]**2
        pd.drRo[i-1] = exp(pd.EEr[i-1])
        pd.drU [i-1]   = pd.drRo[i-1] - 1
        pd.drSSr[i-1] = ( sh.K*(pd.SSt[i-1] - pd.SSr[i-1])/pd.ro[i-1] + sh.gamma * sin(omega) ) * pd.drRo[i-1]

    return pd, rk, rp



def pFDM2(sh, ed, Rz, N, omega):
    """
    запредельная зона: нелинейная функция разупрочнения, деформации. Коши, Сферическая\цилиндрическая полость. С учётом deltaT*
    Дополняет массивы D данными из запредельной зоны,
    :param sh: Shaft
    :param D: elastic Details (arrays)
    :param Rz: radius of plastic region
    :param N: число точек в запредельной зоне
    :param omega: угол [Rad]
    :return: D with plastic data
    """
    pd = new_NDS_detail(N)

    dr = (Rz-sh.Ra)/(N-1) if N>1 else 0
    C = sh.C1

    # условие непрерывности
    pd.U[N-1] = ed.U[0]
    pd.SSr[N-1] = ed.SSr[0]
    pd.SSt[N-1] = ed.SSt[0]
    pd.EEr[N-1] = ed.EEr[0]

    pd.drRo[N-1]  = exp(pd.EEr[N-1])
    pd.drU[N-1]   = pd.drRo[N-1] - 1
    pd.ro[N-1]    = pd.U[N-1] + Rz
    pd.r[N-1] = sh.Ra + dr * (N-1)
    pd.drSSr[N-1] = ( (pd.SSt[N-1] + pd.SSr[N-1])/pd.ro[N-1] + sh.gamma * sin(omega) ) * pd.drRo[N-1]

    pd.SScm[-1] = sh.SSm_c * (1.0 - sh.b * (Rz / sh.Ra) ** (-sh.k))
    pd.SSo[-1] = pd.SScm[-1]
    pd.TT[-1] = ed.EEr[0] + ed.EEt[0]

    EEtz = ed.EEt[0]
    thetta_l = sh.theta_l
    thetta_z = ed.EEr[0]+ed.EEt[0]

    rk, rp = 0, 0
    for i in range(N-1,0,-1):
        pd.r[i-1] = sh.Ra + dr*(i-1)
        pd.U[i-1]   = pd.U[i]   - dr*pd.drU[i]
        pd.SSr[i-1] = pd.SSr[i] - dr*pd.drSSr[i]
        # pd.ro[i-1] = pd.r[i-1] + pd.U[i-1]
        # pd.EEt[i-1] = log(pd.ro[i-1]/pd.r[i-1])  # log = ln
        pd.EEt[i - 1] = pd.U[i-1]/pd.r[i-1]
        pd.TT[i-1]  = (1 - C[1] - 2 * C[2] * pd.SSr[i-1])*(pd.EEt[i-1] - EEtz)
        if (pd.TT[i-1] >= thetta_l - 1e-10):
            if rk==0:  rk=pd.r[i-1]
            pd.TT[i - 1] = thetta_l
        pd.EEr[i-1] = pd.TT[i-1] - pd.EEt[i-1]
        pd.SScm[i-1] = sh.SSm_c*(1.0 - sh.b * (pd.r[i-1]/sh.Ra)**(-sh.k))
        pd.SSo[i-1] = pd.SScm[i-1] - sh.T*(pd.TT[i-1])
        if pd.SSo[i-1] <= sh.ssgp*sh.SSm_c:
            pd.SSo[i-1] = sh.ssgp*sh.SSm_c;
            if rp==0: rp=pd.r[i-1]
        pd.SSt[i-1]= - pd.SSo[i-1] + sh.C[1]*pd.SSr[i-1]+sh.C[2]*pd.SSr[i-1]**2;
        # pd.drRo[i-1] = exp(pd.EEr[i-1])
        pd.drU[i-1]   = pd.EEr[i-1]
        pd.drSSr[i-1] = (pd.SSt[i-1] - pd.SSr[i-1])/pd.r[i-1] + sh.gamma * sin(omega)
    return pd, rk, rp


def pFDM2_flow(sh, ed, Rz, N, omega):
    """Пластическое течение
    Дополняет массивы D данными из запредельной зоны,
    :param sh: Shaft
    :param D: elastic Details (arrays)
    :param Rz: radius of plastic region
    :param N: число точек в запредельной зоне
    :param omega: угол [Rad]
    :return: D with plastic data
    """
    # Nz = calc_Nz(sh, Rz, N)

    # pd = Detail([Rz]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0] * N, [0]*N,[0]*N, [0] * N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)
    # pd = new_NDS_detail(N)

    pd = Detail([Rz]*N,[0]*N,[0]*N,[0] * N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0] * N, [0]*N,[0]*N, [0] * N, [0] * N, [0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,[0]*N,1,1)

    dr = (Rz-sh.Ra)/(N-1) if N>1 else 0
    C = sh.C1

    # условие непрерывности
    pd.U[N-1] = ed.U[0]
    pd.SSr[N-1] = ed.SSr[0]
    pd.SSt[N-1] = ed.SSt[0]
    pd.EEr[N-1] = ed.EEr[0]

    pd.drRo[N-1]  = exp(pd.EEr[N-1])
    pd.drU[N-1]   = pd.drRo[N-1] - 1
    pd.ro[N-1]    = pd.U[N-1] + Rz
    pd.drSSr[N-1] = ( (pd.SSt[N-1] + pd.SSr[N-1])/pd.ro[N-1] + sh.gamma * sin(omega) ) * pd.drRo[N-1]

    EEtz = ed.EEt[0]
    thetta_l = 0.12
    thetta_z = ed.EEr[0]+ed.EEt[0]

    rk, rp = 0, 0
    for i in range(N-1,0,-1):
        pd.r[i] = sh.Ra + dr*i
        pd.r[i-1] = sh.Ra + dr*(i-1)
        pd.U[i-1]   = pd.U[i]   - dr*pd.drU[i]
        pd.SSr[i-1] = pd.SSr[i] - dr*pd.drSSr[i]
        # pd.ro[i-1] = pd.r[i-1] + pd.U[i-1]
        # pd.EEt[i-1] = log(pd.ro[i-1]/pd.r[i-1])  # log = ln
        pd.EEt[i - 1] = pd.U[i-1]/pd.r[i-1]
        pd.TT[i-1]  = (1 - C[1] - 2 * C[2] * pd.SSr[i-1])*(pd.EEt[i-1] - EEtz)
        if (pd.TT[i] >= thetta_l - 1e-10):
            if rk==0:
                # sh.rk=pd.r[i-1]
                rk=pd.r[i-1]
        pd.EEr[i-1] = pd.TT[i-1] - pd.EEt[i-1]
        pd.SScm[i] = sh.SSm_c*(1.0 - sh.b * (pd.r[i]/sh.Ra)**(-sh.k))
        # pd.SSo[i-1] = sh.SSm_c - sh.T*(pd.TT[i-1])
        pd.SSo[i-1] = pd.SScm[i] - sh.T*(pd.TT[i-1])
        if pd.SSo[i-1] <= sh.ssgp*sh.SSm_c:
            pd.SSo[i-1] = sh.ssgp*sh.SSm_c;
            if rp==0:
                # sh.rp = pd.r[i-1]
                rp=pd.r[i-1]
        pd.SSt[i-1]= - pd.SSo[i-1] + sh.C[1]*pd.SSr[i-1]+sh.C[2]*pd.SSr[i-1]**2;
        # pd.drRo[i-1] = exp(pd.EEr[i-1])
        pd.drU[i-1]   = pd.EEr[i-1]
        pd.drSSr[i-1] = (pd.SSt[i-1] - pd.SSr[i-1])/pd.r[i-1] + sh.gamma * sin(omega)
    return pd, rk, rp

# Вычисляет упругую и запредельную зону T
def el_pl_worker(sh,N,omega, NLP_calc, Tasks, result_queue):
     while Tasks.qsize():
         Rz = Tasks.get()
         ed, res, pd, rp,rk = el_pl(sh, Rz, N, omega, NLP_calc)
         result = Rz, ed, res, pd, rp,rk
         result_queue.put(result)
         if pd.SSr[0]>0: break
     # print("Done")
     return

# Вычисления для упругой и запредельной области.
# Решение для запредельной зоны - Т. Функция дилат. сост. линейная.
# Функция прочности здадаёться параметром NLP_func.
def el_pl(sh, Rz, N, Omega=pi/2, NLP_func = func_2d_lin):
    """
    :param sh:
    :param Rz: [mm]
    :param N:
    :return:
    """
    Nz = calc_Nz(sh, Rz, N)
    Ne = N - Nz
    Xrz = calc_at_rz(sh, Rz)
    x0 = [Xrz[3], Xrz[1]]
    res = minimize(NLP_func, x0, args=(sh, Rz, Ne, eFDM))
    ed = eFDM(res.x, sh, Rz, Ne)
    dr_p = (Rz-sh.Ra)/(Nz-1) if Nz>1 else 0
    pd = Detail([sh.Ra + i*dr_p for i in range(Nz)],[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz, [0]*Nz, [0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,
                [0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,1,1)
    pd.U[-1] = ed.U[0]
    pd.SSr[-1] = ed.SSr[0]
    pd.SSt[-1] = ed.SSt[0]
    pd.SScm[-1] = sh.SSm_c*(1.0-sh.b*(pd.r[0]/sh.Ra)**(-sh.n))
    # p_q, pd.SSr[-1], pd.SSt[-1], pd.U[-1] = calc_at_rz(sh, Rz)
    rk, rp = sh.Ra, sh.Ra  # радиусы предельно разрыхленной и разрушенных зон соответственно
    flagrk, flagrp = False, False
    for i in range(Nz-1,-1,-1):
        pd.SScm[i] = sh.SSm_c*(1.0-sh.b*(pd.r[i]/sh.Ra)**(-sh.n))
        pd.drSSr[i], pd.TT[i], pd.SSo[i], pd.SSt[i], pd.drU[i] = \
        pl_at_r(sh, pd.r[i],pd.SSr[i],pd.U[i], pd.SScm[i], uz = ed.U[0], Rz = Rz, Omega=Omega, rp=flagrp, rk=flagrk)
        if pd.TT[i] > sh.theta_l and flagrk==False:
            flagrk=True;  rk=pd.r[i]
            pd.drSSr[i], pd.TT[i], pd.SSo[i], pd.SSt[i], pd.drU[i] = \
            pl_at_r(sh, pd.r[i],pd.SSr[i],pd.U[i], pd.SScm[i], uz = ed.U[0], Rz = Rz, Omega=Omega, rp=flagrp, rk=flagrk)
        if pd.SSo[i] <= sh.ssgp*sh.SSm_c and flagrp==False:
            flagrp=True; rp=pd.r[i]
            pd.drSSr[i], pd.TT[i], pd.SSo[i], pd.SSt[i], pd.drU[i] = \
            pl_at_r(sh, pd.r[i],pd.SSr[i],pd.U[i], pd.SScm[i], uz = ed.U[0], Rz = Rz, Omega=Omega, rp=flagrp, rk=flagrk)
        if i>0:
            pd.U[i-1]   = pd.U[i]   - dr_p*pd.drU[i]
            pd.SSr[i-1] = pd.SSr[i] - dr_p*pd.drSSr[i]
        pd.EEr[i] = pd.drU[i]
        pd.EEt[i] = pd.U[i]/pd.r[i]
    return ed, res, pd, rp,rk


def el_pl2(sh, Rz, N, omega =pi / 2, NLP_func = func_2d_quad, el_system = eFDM, pl_system = pFDM2):
    """
    :param sh:
    :param Rz:
    :param N:
    :param omega:
    :param NLP_func:
    :param el_system:
    :param pl_system:
    :return: Elastic, Plastic,
    """
    Nz = calc_Nz(sh, Rz, N)
    Ne = N - Nz
    Xrz = calc_at_rz(sh, Rz)
    x0 = [Xrz[3], Xrz[1]]
    res = minimize(NLP_func, x0, args=(sh, Rz, Ne, el_system))
    ed = el_system(res.x, sh, Rz, Ne)
    pd,rk,rp = pl_system(sh, ed, Rz, Nz, omega = omega)
    return ed, res, pd, rk, rp







def el_pl_ln(sh, Rz, N, Omega=pi/2, NLP_func=func_2d_quad):
    Xrz = calc_at_rz(sh, sh.Ra)
    x0 = [Xrz[3], Xrz[1]]

    Nz = calc_Nz(sh, Rz, N)
    Ne = N - Nz

    res_ln = minimize(NLP_func, x0, args=(sh, Rz, Ne, eFDM_ln))
    ed_ln =  eFDM_ln(res_ln.x, sh, Rz, Ne)
    pd_ln,rk,rp = pFDM2_ln(sh, ed_ln, Rz, Nz, omega=Omega)
    return ed_ln, res_ln, pd_ln, rk, rp


def pl_flow(sh, Rz, Nz, dr, ed, _pd, omega=pi/2):
    """
    :param sh:
    :param Rz: [mm]
    :param N:
    :return:
    """
    thetta_l = sh.theta_l

    # pd = Detail([sh.Ra + i*dr for i in range(Nz)],[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz, [0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,  [0]*Nz, [0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,
    #             [0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,1,1)
    #
    # pd = new_NDS_detail(N)

    pd = Detail([sh.Ra + i*dr for i in range(Nz)],[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz, [0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,  [0]*Nz, [0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,
                [0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,[0]*Nz,1,1)

    pd.U[-1] = ed.U[0]
    pd.drU[-1] = ed.drU[0]
    pd.SSr[-1] = ed.SSr[0]
    pd.SSt[-1] = ed.SSt[0]
    pd.EEt[-1] = ed.EEt[0]
    pd.SScm[-1] = sh.SSm_c*(1.0-sh.b*(pd.r[0]/sh.Ra)**(-sh.n))
    # p_q, pd.SSr[-1], pd.SSt[-1], pd.U[-1] = calc_at_rz(sh, Rz)
    rk, rp = sh.Ra, sh.Ra  # радиусы предельно разрыхленной и разрушенных зон соответственно
    flagrk, flagrp = False, False
    rk, rp = 0, 0
    for i in range(Nz-1, 0, -1):
        pd.r[i] = sh.Ra + dr*i
        pd.drSSr[i] = (pd.SSt[i] - pd.SSr[i]) / pd.r[i]
        pd.U[i-1]   = pd.U[i]   - dr*pd.drU[i]
        pd.SSr[i-1] = pd.SSr[i] - dr*pd.drSSr[i]
        pd.EEt[i-1] =  pd.U[i-1] / pd.r[i-1]
        # pd.TT[i-1] = - (1 - sh.C[1] - 2*sh.C[2] * _pd.SSr[i-1]) * ( (pd.EEt[i-1] - pd.EEt[-1]) - (_pd.EEt[i-1] - _pd.EEt[-1]) ) + _pd.TT[i-1]
        pd.TT[i-1] = - (1 - sh.C[1] - 2*sh.C[2] * _pd.SSr[i-1]) * (pd.EEt[i-1] - pd.EEt[-1])
        if (pd.TT[i-1]>thetta_l):
            pd.TT[i-1] = thetta_l;
            if rk == 0:
                rk=pd.r[i-1]
        pd.drU[i-1] = pd.TT[i-1] - pd.EEt[i-1]
        pd.EEr[i-1] = pd.drU[i-1]
        pd.SSo[i-1] = sh.SSm_c - sh.T*pd.TT[i-1]
        if pd.SSo[i-1] < sh.ssgp*sh.SSm_c:
            pd.SSo[i-1] =  sh.ssgp*sh.SSm_c
            if rp == 0:
                rp=pd.r[i-1]
        pd.SSt[i-1]= - pd.SSo[i-1] + sh.C[1]*pd.SSr[i-1]+sh.C[2]*pd.SSr[i-1]**2;
    return pd, rk, rp


# find first intersection of the curves. return next index after (at) intersection. return 0, if no intersection
# curve 1 and curve 2 mush be same long and corresponded indexes
def findPxP(Pp, POb, U):
    x = Pp[0]>POb[0]
    i = 0; gotit = 0;
    for p, pob in zip(Pp, POb):
        if (p>pob) != x: gotit=1; break;
        else: i+=1
    if gotit:
        # _y1, y1, _y2, y2 = Pp[i-1], Pp[i], POb[i-1], POb[i]
        # _x1, x1, _x2, x2 = U[i-1], U[i], U[i-1], U[i]
        # x=-((_x1*_y2-_x2*_y1)*(x2-x1)-(x1*y2-x2*y1)*(_x2-_x1))/((_y1-_y2)*(x2-x1)-(y1-y2)*(_x2-_x1));
        # y=-((y1-y2)*x-(x1*y2-x2*y1))/(x2-x1)
        return (U[i] + U[i-1])/2,(Pp[i-1] + Pp[i])/2
    else: return -1,1


def calc_beta_sinfi(sh, Rz, Ne, el_sys, NLP_func):
    """
    Вычисление beta и phi для линейного условия прочности из ssr* и sst*. Для этого считает упугую зону
    :param ssc:
    :param s:
    :param C:
    :return:
    """
    ed,res = el(sh, Rz, Ne, el_sys, NLP_func)
    ssrz = ed.SSr[0]
    sstz = ed.SSt[0]
    sscm = sh.SSm_c

    beta = (sstz+sscm)/ssrz
    sinfi = (beta-1)/(beta+1)
    return beta, sinfi






# /// calculate P_min, P_max and intresection P_ob with P curves
# /// return 1 - ok
# int  getPminimax(Core& c){
# unsigned ii = find_intersection(c.aval.pob, c.aval.p, c.aval.n);
# if (ii-1<ii){
#     interoint(ii-1,c.aval.p[ii-1], ii, c.aval.p[ii], ii-1,c.aval.pob[ii-1], ii, c.aval.pob[ii], c.aval.pp_i,c.aval.pp_val);
#     double px = c.aval.pp_val;
#     OneDGrid::N_Type ix = ii;
#     c.Pmin = px;
#
#     double pob_0, pob_uw;
#     OneDGrid::N_Type i_0=-1, i_uw=-1;
#
# // где p=0?
# //if (c.aval.p[c.aval.n-1]>0) return -1;    // нужно увеличить rz_max
# for (OneDGrid::N_Type i = c.aval.n-1; i>=0; i--)
#   if (c.aval.p[i]<=0) {i_0=i; pob_0 = c.aval.pob[i_0]; break;}
#
# double shor_u = c.shaft.shit.uw;
# // if there is UW and Pob intersection
# if (-c.aval.ua[c.aval.n-1] >= shor_u && -c.aval.ua[0] <= shor_u && c.aval.n>1){
# OneDGrid::N_Type i=c.aval.n-1;
# for (;-c.aval.ua[i] < shor_u || -c.aval.ua[i-1] > shor_u;) i--;
# pob_uw = (c.aval.pob[i] + c.aval.pob[i-1])/2;
# i_uw=i;}
# else i_uw=-1; // no Uw and Pob intersection
#
# if (i_uw==-1)
#   if (i_0==-1) return 0;
#   else c.Pmax = pob_0;
# else
#    if (i_uw<ix)
#     if (i_0==-1) return 0;
#     else c.Pmax = pob_0;
#    else
#    if (i_uw>i_0) c.Pmax = pob_uw;
#    else          c.Pmax = pob_0;
# return 1;
#
# } else return 0;
# }

def nds_full_plain(_nds1, _nds2, tt, sh):
    """ Полные напряжения для плоской упругой задачи"""

    ndsr = []

    nds1 =list(_nds1)                               # данные из   осесимметричной задачи ( по функции pFDM..)
    nds2 = _nds2                                    # данные из НЕосесимметричной задачи ( по функции eFDM_plain_2)

    f_cos = lambda x, y: x + y * cos(2 * tt)
    f_sin = lambda x, y: y * sin(2 * tt)
    f_SS1 = lambda ssr, sst, tau: (ssr + sst) / 2 + 0.5 * ((((ssr - sst) ** 2) + (4*(tau ** 2)))**0.5)
    f_SS3 = lambda ssr, sst, tau: (ssr + sst) / 2 - 0.5 * ((((ssr - sst) ** 2) + (4*(tau ** 2)))**0.5)
    f_SSeq = lambda ss1, ss3: ss1 * sh.beta - ss3
    f_SScm_r = lambda r: SScm_r(sh.SSm_c,sh.b,sh.k,r/sh.Ra)
    f_SS_sub = lambda sseq, sscm: sseq -sscm

    for n1, n2 in zip(nds1, nds2):
        SSr = list(map(f_cos, n1.SSr, n2.SSr))
        SSt = list(map(f_cos, n1.SSt, n2.SSt))
        U = list(map(f_cos, n1.U, n2.U))
        Tau = list(map(f_sin, n1.Tau, n2.Tau))
        V = list(map(f_sin, n1.V, n2.V))
        SS1 = list(map(f_SS1, SSr, SSt, Tau))
        SS3 = list(map(f_SS3, SSr, SSt, Tau))
        SSeq = list(map(f_SSeq, SS1, SS3))
        SScm = list(map(f_SScm_r,n1.r))
        SS_sub = list(map(f_SS_sub,SSeq, SScm))

        ndsr += [n1._replace(SSr=SSr,
                             SSt=SSt,
                             U=U, V=V,
                             Tau=Tau,
                             SS1=SS1,
                             SS3=SS3,
                             SSeq=SSeq,
                             SScm =SScm,
                             SSsub = SS_sub
                             )]
    return ndsr





def calc_I(sh):
    """ для неосиметричной задачи???"""
    global eps
    u = U_I(sh.S, sh.Em_p, sh.mu_p, sh.Ra)
    ssr = -sh.S
    sst = -sh.S
    eet_k = u/sh.Ra
    eet_h = - sh.S / sh.Em_p * (1 - sh.mu_p)
    print (("        ,mm  =%5.3f"+print_mask)%(sh.mu,u,-sh.S, -sh.S, eet_k, eet_h, '+' if compare(eet_k, eet_h, eps) else '-', 0, sh.Em))
    return u,ssr,sst,eet_k,eet_h,compare(eet_k, eet_h, eps)


def calc_I_II(sh, tri):
    """ для неосиметричной задачи???"""
    # global eps
    note = Note(sh)
    u,ssr,sst, eet_k, eet_h,b,result = tri
    S = sh.S
    ssr-=S
    sst-=S
    uI = U_I(sh.S, sh.Em_p, sh.mu_p, sh.Ra)
    u = u + uI
    eet_k = u/sh.Ra
    E = sh.Em_p if sh.a==0 else result.Er[0]
    eet_h = EET(sh.Em_p if sh.a==0 else sh.Em_ra,sst,ssr,S=0,mu=sh.mu_p)
    print(note + print_mask%(u,  ssr,  sst, eet_k, eet_h, '+' if compare(eet_k, eet_h, eps) else '-', result.Er[0], E))
    return u,ssr,sst,eet_k,eet_h,compare(eet_k, eet_h, eps)


def calc_prog(sh,N, fdm, func):
    """ для неосиметричной задачи???"""
    global eps
    note = Note(sh)
    u0,ssr0 = 0, 0 if func==funcIV_1D else sh.S
    res = minimize(func, [u0, ssr0], args=(sh, N, fdm))
    if res.status != 0:
        print("Optimization faild")
    u0 = res.x[0]
    f = func([u0,ssr0],sh, N, fdm)
    result = fdm([u0,ssr0],sh, N)
    eet_k = u0/sh.Ra
    E = sh.Em_p if sh.a==0 else result.Er[0]
    S = 0 if fdm!=eFDM and func!=funcIV_1D else sh.S
    eet_h = EET(E, result.SSt[0],ssr0,S,mu=sh.mu_p)
    print (note + (print_mask+", F= %f")%(u0,ssr0, result.SSt[0],eet_k, eet_h, '+' if compare(eet_k, eet_h, eps) else '-', result.Er[0], E, f))
    return u0,ssr0,result.SSt[0],eet_k,eet_h,compare(eet_k, eet_h, eps), result