import numpy as np

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

from report import *
import pandas
import seaborn
import geom_aux


LineStyles = ['-', '--']


#d - Detail
def draw(d):
    pass



def NDS_plots(sh, Rz: float, DF: pandas.DataFrame, Cases: list, shaft_name: str, show=1, save=1):
    """
    Строит графики для НДС
    :param sh:
    :param Rz:
    :param df:
    :param Cases:
    :return:
    """

    fig = plt.figure()

    # leg = plt.legend(fontsize=10)

    sh0 = Cases[0][0]

    prepare_plot(x_lim=[0, 7], xlabel="r, [$r_a$]", ylabel="Напряжение [МПа]",
                 plot_title="радиальные и тангенциальные напряжения $\sigma_r, \sigma_{\\theta}$",
                 window_title="'Напряжения. %s'%shaft_name", vline=sh0.S, rz=Rz / sh0.Ra)
    i = 0
    yscale("log")
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    grid(b=True, which='minor', linestyle='--')
    for *_, task, label in Cases:
        data = DF[DF['task'] == task]
        plt.plot(data['r[r_a]'], np.abs(data['SSr[MPa]']), LineStyles[i % len(LineStyles)],
                 label="радиальные напряжения. " + label, linewidth=1 if i%2==1 else 2)
        plt.plot(data['r[r_a]'], np.abs(data['SSt[MPa]']), LineStyles[i % len(LineStyles)],
                 label="тангенциальные напряжения. " + label, linewidth=1 if i%2==1 else 2)
        i += 1
    leg = plt.legend(loc='best', fontsize=10)
    if save: savefig('ssr,sst.%s.png' % shaft_name)
    if show: plt.show()

    fig = plt.figure()
    fig.canvas.set_window_title('Перемещения. %s' % shaft_name)
    plt.title("радиальные перемещения", fontsize=16)
    plt.xlim([0, 7])
    i = 0
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    for *_, task, label in Cases:
        data = DF[DF['task'] == task]
        if i%2 == 0:
            plt.plot(data['r[r_a]'], np.abs(data['u[mm]']), LineStyles[i % len(LineStyles)], label=label, linewidth=2)
        else:
            plt.plot(data['r[r_a]'], np.abs(data['u[mm]']), LineStyles[i % len(LineStyles)], label=label, linewidth=2,
                     dashes=(5,5))
        i = i + 1
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]");
    plt.ylabel("Перемещения [мм]")
    plt.grid(True)
    plt.legend(loc='best')
    if save: savefig('u.%s.png' % shaft_name, dpi=DPI)
    if show:
        plt.show()
    #
    plt.title("Относительное объёмное разрыхление $\Delta\Theta$", fontsize=16)
    maxrz = 0
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    for case in Cases:
        sh, N, Rz, omega, el_sys, pl_sys, func, task, label = case
        if Rz > maxrz:
            maxrz = Rz

    plt.xlim([0.75, maxrz / sh.Ra + 0.5])
    i=0
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    for *_, task, label in Cases:
        data = DF[DF['task'] == task]
        # plt.xlim([0.75, RZ[-1] / sh.Ra + 0.5])
        plt.plot(data['r[r_a]'], data['TT'], LineStyles[i % len(LineStyles)], label=label)
        i = i + 1
    plt.axhline(0.12, ls="dashed", color="gray", linewidth=1)
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]");
    plt.ylabel("$\Delta\Theta$")
    plt.grid(True);
    plt.legend(loc='best')
    if save: savefig('TT.%s.svg' % shaft_name, dpi=DPI)
    if show > 1: plt.show()

    plt.title("Относительная объёмноная деформация $\Delta\\varepsilon_v$", fontsize=16)
    maxrz = 0
    for case in Cases:
        sh, N, Rz, omega, el_sys, pl_sys, func, task, label = case
        if Rz > maxrz:
            maxrz = Rz
    plt.xlim([0.75, maxrz / sh.Ra + 0.5])
    i = 0
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    for *_, task, label in Cases:
        data = DF[DF['task'] == task]
        plt.plot(data['r[r_a]'], np.exp(data['TT'])-1, LineStyles[i % len(LineStyles)], label=label, linewidth=2,
                 dashes=((5, 7) if i % 2 == 1 else (5, 0)) )
        i += 1
    theta_l = Cases[0][0].theta_l
    plt.text(0.85, np.exp(theta_l) - 1, "$\Delta\\varepsilon_{v,пр}$")
    # plt.text(Rz, -0.1, "$r*$")
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.axhline(np.exp(theta_l) - 1, ls="dashed", color="gray", linewidth=1)

    plt.xlabel("r, [$r_a$]")
    plt.ylabel("$\Delta\\varepsilon_v$")
    plt.grid(True)
    plt.legend(loc='best')
    if save: savefig(f'Ev.{shaft_name}.svg', dpi=DPI)
    if show > 1: plt.show()

    plt.title("Остаточная прочность $\sigma_{ост}$", fontsize=16)
    plt.xlim([0.75, 4])
    i = 0
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    for *_, task, label in Cases:
        data = DF[DF['task'] == task]
        plt.plot(data['r[r_a]'], data['SSo[MPa]'], LineStyles[i % len(LineStyles)], label=label, linewidth=2,
                 dashes=((5, 7) if i % 2 == 1 else (5, 0)) )
        i = i + 1
    plt.axhline(sh.ssgp * sh.SSm_c, ls="dashed", color="gray", linewidth=1)
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]");
    plt.ylabel("$\sigma_o, [MPa]$")
    plt.grid(True)
    plt.legend(loc='best')
    if save: savefig('SSo.%s.svg' % shaft_name, dpi=DPI)
    if show > 1: plt.show()

    # plt.title("Модуль упругости $E(r)$", fontsize=16)
    # plt.xlim([0.75, 7])
    # plt.plot(r_ln, df[(df.task == 'лог. С') & (df.Rz == RZ[-1])]['Er[MPa]'], '--', label="лог. С. сфера")
    # plt.plot(r_koshi, df[(df.task == 'коши. С') & (df.Rz == RZ[-1])]['Er[MPa]'], label="коши. С. сфера")
    # plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    # plt.xlabel("r, [$r_a$]");
    # plt.ylabel("$E(r), [MPa]$")
    # plt.grid(True);
    # plt.legend(loc='best')
    # if save: savefig('Er.%s.png' % shaft_name, dpi=DPI)
    # if show > 1: plt.show()


    # тепловая карта для ssr
    for *_, task, label in Cases:
        data = DF[DF['task'] == task]
        if show:
            polar_heatmap_r(data['SSr[MPa]'].values, r_from = data['r[r_a]'].values[0], 
                                                r_to   = data['r[r_a]'].values[-1],
                                                title=task + ". $\sigma_r$, [MPa]")


def RP_plots(sh: Shaft, DF: pandas.DataFrame, Cases: list, shaft_name: str, p_lining: float = None, Ux:list=[], show=1, save=1):
    """
    Показывает и созраняет графики для Гороного давления и нескольких конфигураций расчёта (Cases)

    :param shaft_name:
    :param sh: основная конфигурация выработки
    :param DF:
    :param Cases: элемент списка [sh, (1 * sh.Ra, 3 * sh.Ra), Omega, el_pl2, func_2d_quad, eFDM_sph, pFDM2_ln_spher_a,
    name='RP.koshi', title='деформ. Коши' ]
    :param p_lining: отпор крепи (кПа). рисуется на графике горного давления
    :param Ux: список из абсцисс вертикальных линий - границ допустимых перемещений [(task1, label1, u1, u2), (task1, label1, u1, u2)]
                этот список возвращает функция отчёта report_compared_RP
    :param show:
    :param save:
    :return:
    """

    task0 = Cases[0][-2]
    df = DF[DF['task'] == task0]

    # ux, px = intersection_P_Pob(df['P[kPa]'], df['Pob[kPa]'], df['U_a[mm]'])

    fig = plt.figure()
    fig.canvas.set_window_title('Горное давление. %s' % shaft_name)

    i = 0
    for *_, task, label in Cases:
        data = DF[DF['task'] == task]
        plt.plot(-data['U_a[mm]'], data['P[kPa]'],   label="p. "+label,
                 ls=LineStyles[i % len(LineStyles)] )
        plt.plot(-data['U_a[mm]'], data['Pob[kPa]'], label="давление в случае обрушения пород $P_{об}$. "+label,
                 ls=LineStyles[i % len(LineStyles)], linewidth=LWmain)
        i += 1

    # plt.axhline(px, color = 'black', ls='dashed', linewidth=LWaux)
    u1_p_l, u2_p_l = None, None
    if p_lining:
        plt.axhline(p_lining, color='black', ls='dashed', linewidth=LWaux)

        if Ux:
            u1_p_l, u2_p_l = Ux[0][2], Ux[0][3]  # абсцисса пересенчия отпора крепи с давлением и давлением при обрушении
        # df = DF[DF['task'] == Cases[0][-2]]  # -2 - это task
        # P, Pob, U = df["P[kPa]"], df["Pob[kPa]"], df['U_a[mm]']



    plt.xlabel("Перемещение контура выработки, $u_a$ [мм]")
    plt.ylabel("Давление [КПа]")
    if u1_p_l and u2_p_l:
        plt.axvline(u1_p_l, color = 'black', ls='dashed', linewidth=LWaux)
        plt.axvline(u2_p_l, color = 'black', ls='dashed', linewidth=LWaux)
    plt.grid(True)
    plt.legend(loc='best', fontsize=12)
    plt.title("Горное давление $P$ и давление обрушения $P_{об}$", fontsize=16)
    if save: savefig(f'P,Pob.({shaft_name}).png', dpi=DPI)
    if show: plt.show()

    fig = plt.figure()
    fig.canvas.set_window_title(f'Радиус запрежельной зоны. {shaft_name}')
    i = 0
    for *_, task, label in Cases:
        data = DF[DF['task'] == task]
        plt.plot(-data['U_a[mm]'], data['Rz']/sh.Ra, label=label, ls=LineStyles[i % len(LineStyles)])
        i += 1
    plt.grid(True)
    plt.legend(loc='best', fontsize=12)
    plt.xlabel("Перемещение контура выработки, $u_a$ [мм]")
    plt.ylabel("радиус запредельной зоны, $r*/r_a$")
    if save: savefig(f'ua-rz({shaft_name}).png', dpi=DPI)
    if show: plt.show()



    # plt.plot(Ua, RK, label="Радиус, на котором массив предельно разрыхлен $r_k$", linewidth=2)
    # plt.plot(Ua, RP, label="Радиус, на котором массив предельно разрушен $r_p$", linewidth=2)
    # plt.plot(Ua, Rz_, label="Радиус запредельной зонны $r^*$", linewidth=2)
    # # plt.xticks(arange(int(Rb/sh.Ra+1)))
    # plt.xlabel("Перемещение контура выработки, $u_a$ [мм]")
    # plt.ylabel("Расстояние от центра выработки [ед. Ra]")
    # plt.grid(True)
    # plt.legend(loc='best', fontsize=12)
    # plt.title("Радиус пред. разрыхления и радиус пред. разрушения", fontsize=16)
    # if save: savefig('Rp,Rk.(%s).png' % shaft_name, dpi=DPI)
    # if show > 1: plt.show()
    #
    # fig = plt.figure()
    # fig.canvas.set_window_title('Радиальные и касательные напряжения на r*. %s' % shaft_name)
    # plt.plot(Ua, SSrz, label=" $\sigma_r^*$", linewidth=2)
    # plt.plot(Ua, SStz, label="$\sigma_\\theta^*$", linewidth=2)
    # # plt.xticks(arange(int(Rb/sh.Ra+1)))
    # plt.xlabel("Перемещение контура выработки, $u_a$ [мм]")
    # plt.ylabel("Напряжения [MPa]")
    # plt.grid(True)
    # plt.legend(loc='best', fontsize=12)
    # plt.title("Радиальные и касательные напряжения на r*", fontsize=16)
    # if save: savefig('SSrz,SStz.(%s).png' % shaft_name, dpi=DPI)
    # if show > 1: plt.show()



# ???

def draw_f(sh, N, func, fdm):
    dx, dy = 1.25, 1.25
    # generate 2 2d grids for the x & y bounds
    y, x = np.mgrid[slice(-30, 30 + dy, dy), slice(-200, 160 + dx, dx)]
    z = func([x, y], sh, N, fdm)
    z = z[:-1, :-1]
    z_min, z_max = -np.abs(z).max(), np.abs(z).max()

    plt.clf()
    plt.pcolor(x, y, z, cmap=cm.gray, vmin=z_min, vmax=z_max)
    plt.title('F(u0,ssr0)')
    # set the limits of the plot to the limits of the data
    plt.axis([x.min(), x.max(), y.min(), y.max()])
    plt.colorbar()
    plt.show()



def polar_heatmap_r(values: np.ndarray, r_from=1, r_to=3, cmap='jet', title='', fontsize=16):
    """ Строит тепловую карту в полярных координатах
    :param: values -- набор значений вдоль радиуса
    :param: r_from -- от какого радиуса отсчитываются данные, в ед. r_a
    :param: r_to   -- до какого радиуса отсчитываются данные, в ед. r_a
    :param: cmap   -- набор цвтов для раскраски (https://matplotlib.org/stable/tutorials/colors/colormaps.html)
    """
    MAX_RAD_NODES = 100
    print(values.shape)

    fig = plt.figure(figsize=(15, 15))
    ax = Axes3D(fig)

    nth = 1  # шаг с которым из данных выдёргиваем точки
    n = len(values)  # число точек на радиусе
    if n > MAX_RAD_NODES:
        nth = n // 100
        n = values[::nth].shape[0]

    m = 365  # число точек на окружности
    rad = np.linspace(r_from, r_to, n + 1)
    azm = np.linspace(0, 2 * np.pi, m)
    r, th = np.meshgrid(rad, azm)

    # тут радиусы -- это второй индекс
    z = np.ones(shape=(m, n))
    for i in range(m):
        z[i, :] = values[::nth]  # индексация: theta, r

    plt.title(title, fontsize=fontsize)
    plt.subplot(projection="polar")
    plt.pcolormesh(th, r, z, cmap=cmap, shading='auto')
    plt.plot(azm, r, ls='none')
    plt.colorbar()
    plt.grid()
    plt.show()



def polar_heatmap(values: np.ndarray, r_from=1, r_to=3, cmap='jet', title='', fontsize=16):
    """ Строит осесимметричную тепловую карту в полярных координатах
    :param: values -- матрица из значений вдоль радиуса (строка), число строк = число углов
    :param: r_from -- от какого радиуса отсчитываются данные, в ед. r_a
    :param: r_to   -- до какого радиуса отсчитываются данные, в ед. r_a
    :param: cmap   -- набор цвтов для раскраски (https://matplotlib.org/stable/tutorials/colors/colormaps.html)
    """
    MAX_RAD_NODES = 100
    MAX_TH_NODES = 365


    fig = plt.figure( figsize=(20, 20) )
    ax = Axes3D(fig)

    nth, mth = 1, 1           # шаг с которым из данных выдёргиваем точки
    m,n = values.shape           # число точек на радиусе
    
    # если значений слишком много ...
    if (n > MAX_RAD_NODES):
        nth = n // MAX_RAD_NODES 
        n = values[0, ::nth].shape[0]

    # если значений слишком много ...
    if (m > MAX_RAD_NODES):
        mth = m // MAX_TH_NODES 
        m = values[::mth, 0].shape[0]
   
    rad = np.linspace(r_from, r_to, n+1)
    azm = np.linspace(0, 2 * np.pi, m+1)
    r, th = np.meshgrid(rad, azm)
        
    z = values
    
    plt.title(title, fontsize=fontsize)
    plt.subplot(projection="polar")
    plt.pcolormesh(th, r, z, cmap=cmap, shading='auto')

    plt.plot(azm, r, ls='none')
    plt.colorbar()
    plt.grid()
    plt.show()


# юзать функции рисования тепловой карты напряжений так:
# nds_data0 = nds11[0]
# draw.polar_heatmap_r( np.array(nds_data0.SSr) )
