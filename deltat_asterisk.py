# сферическая и цилиндрическая задача упругости (упругая зона и задача)
# для конференции на каф. СМиМ осень 2018


# НЕТ:
# 2019: для статьи Решение нелинейной осесимметричной задачи горного давления при наличии запредельных зон
# с использованием логарифмических деформаций.


import pandas  # для DataFrame
from geom import *
from geom_aux import *
from calc import *
from report import *
from sst_ttm import *
from draw import *


# Эта функция потом должна стать основой для более общей. Надо бы тут те классы заюзать
# noinspection PyPep8Naming
def compare_general(Cases, show=1, save=1):
    """ Сравнивает несколько задач (cases) упругости
    улучшенная по сравнению с compare_general. Юзать именно эту
    :param Cases: [ (sh, N, Rz,  Omega, el_sys, pl_sys, func, name, label), (,,,,,,)]  func - целевая функция
    name - краткое обозначение задачи. далее используется в DataFrame
    label  используется дял подписи графиков
    :param show:
    :param save:
    :return:
    """
    global prog_dir, shaft_name
    print("""
       Вычисление ... 
       """)

    # DataFrame отлично подходит для обработки данных
    # task - краткое обозначение решаемой задачи
    DF = pandas.DataFrame(
        columns=['task', 'Rz', 'r[r_a]', 'r[mm]', 'Er[MPa]', 'SScm[MPa]', 'u[mm]', 'SSr[MPa]', 'SSt[MPa]',
                 'EEr', 'EEt', 'TT', 'SSo[MPa]'])

    # task - условное обозначений решаемой задачи (например цилиндр или сфера);
    # здесь (уже нет) используется Г - геометр. нелинейность; ГФ - геом. и физ. нелинейность

    print("Решаемые задачи: ")
    for case in Cases:
        el_sys, pl_sys = case[4:6]
        print(get_func_info(el_sys))
        print(get_func_info(pl_sys))
    print()

    for case in Cases:
        sh, N, Rz, omega, el_sys, pl_sys, func, task, label = case
        print(f"{task:10s} ", end="", flush=True)

        df,rk,rp, _ = el_pl_df(sh, Rz, N, omega, func, el_sys, pl_sys)  # вычисления
        df['task'] = [task] * len(df)

        DF = DF.append(df, sort=False)
        print("Готово")

    print()

    sh = Cases[0][0]
    Rz = Cases[0][2]

    print_compared_NDS(sh, Rz, DF)
    NDS_plots(sh, Rz, DF, Cases, shaft_name, show, save)

    os.chdir(result_dir)

    file = f"{shaft_name}.cvs"
    DF.to_csv("DF." + file, sep='\t', float_format='%.5f')
    # DF.to_html("DF." + file1+".html", classes=["table-bordered", "t    able-striped", "table-hover"])
    os.chdir(prog_dir)
    print(f"Данные сохранены в   {result_dir}")
    print(f"Файл: {file}")


# noinspection PyPep8Naming
def compare_RP_general(Cases:list, p_lining:float, show=1, save=1):
    """ Сравнивает несколько задач (cases) определения ГД

    :param Cases: [sh, (1 * sh.Ra, 3 * sh.Ra), Omega, el_pl2, func_2d_quad, eFDM_sph, pFDM2_ln_spher_a, name='RP.koshi', title='деформ. Коши' ]
    name - краткое обозначение задачи. далее используется в DataFrame
    label  используется дял подписи графиков
    :param p_lining: отпор крепи (кПа). рисуется на графике горного давления
    :param show:
    :param save:
    :return:
    """
    global prog_dir, shaft_name
    print("""
       Решенеие задачи Горного давления... 
       """)

    # DataFrame отлично подходит для обработки данных
    # task - краткое обозначение решаемой задачи

    DF_RP = pandas.DataFrame(columns=['task'] + RP_COLUMNS)

    # task - условное обозначений решаемой задачи (например цилиндр или сфера);
    # здесь (уже нет) используется Г - геометр. нелинейность; ГФ - геом. и физ. нелинейность

    print("Решаемые задачи: ")
    for case in Cases:
        el_sys, pl_sys = case[4:6]
        print(get_func_info(el_sys))
        print(get_func_info(pl_sys))
    print()
    for case in Cases:
        sh, Rz_Rz, N, Nz, omega, el_pl, el_sys, pl_sys, func, task, label = case

        DF_NDS = pandas.DataFrame(columns= NDS_COLUMNS)

        print(f"Вычисление для {label}")
        drz = (Rz_Rz[1] - Rz_Rz[0]) / Nz
        RZ = [Rz_Rz[0] + drz * i for i in range(Nz)]
        print("""Диапазон изменения Rz: %1.2f - %1.2f; шаг drz: %1.4f ra (%0.2f мм); число точек: %i""" %
              (RZ[0] / sh.Ra, RZ[-1] / sh.Ra, drz / sh.Ra, drz, Nz))
        DD = []  # details [ed + pd]

        RP, RK = [], []
        Pob = []

        i = 0
        d = 20  # печатать прогресс после прохождения каждой 1/d части вычислений

        rp_, rk, Rp_, Rk_ = 0, 0, 0, 0  # с каких значений начинаються rp, rk. Ед. Ra
        print("Выполнение %: ", end="")
        x = None
        for Rz in RZ:
            df_nds, rk, rp, x = el_pl_df(sh, Rz, N, omega, func, el_sys, pl_sys, x)  # вычисления
            df_nds['task'] = [task] * len(df_nds)
            df_nds['Rz'] = [Rz] * len(df_nds)

            DF_NDS = DF_NDS.append(df_nds, sort=False)

            if rp != sh.Ra and Rp_ == 0:  Rp_ = rp
            if rk != sh.Ra and Rk_ == 0:  Rk_ = rk
            RP += [rp / sh.Ra]
            RK += [rk / sh.Ra]
            Pob += [(sh.kd * sh.gamma * (Rz - sh.Ra)) / sh.ksr * 1000]
            i += 1

            if i % (len(RZ) / d) == 0:
                print(f"{i /len(RZ) * 100:.0f}|", end="", flush=True)
            if df_nds['SSr[MPa]'][0] < -10 or i == len(RZ):
                break

        df = dfNDS_2_dfRP(DF_NDS)
        df['Pob[kPa]'] = Pob
        df['task'] = [task]*len(df)
        df['rk'] = RK
        df['rp'] = RP
        DF_RP = DF_RP.append(df, sort=False)

        print()

    pandas.set_option('display.expand_frame_repr', False)
    print(DF_RP)

    os.chdir(result_dir)
    report_compared_RP(sh, DF_RP, Cases)
    RP_plots(sh, DF_RP, Cases, shaft_name,  p_lining, show, save)


    # DF.to_csv(data_file + ".csv")
    # print_result_RP(rp_data[0], sh, extra_points=[(Rp_ / sh.Ra, "rp"), (Rk_ / sh.Ra, "rk")])
    # show_plots = 1
    # save_result_RP(rp_data, sh, result_dir, data_filename=data_file + ".csv", show=show_plots)
    os.chdir(prog_dir)

#


def RP_ex(sh, N, NNz, Rz_Rz, Omega, elpl=el_pl2, NLP_func=func_2d_quad, el_system=eFDM, pl_system=pFDM2):
    """
    Построение кривых горного давления. Настраивается. Использовать его?
    :param sh:
    :param N: Число точек для шага по R*
    :param NNz: Число точек для шага по R*
    :param Rz_Rz:
    :param Omega:
    :return:
    """
    global result_dir, data_file, shaft_name, show_plots
    print("""Задача определения горного давления.
    Линейное условие прочности.
    Линейная функция дилатансионных состояний.""")
    drz = (Rz_Rz[1] - Rz_Rz[0]) / NNz
    RZ = [Rz_Rz[0] + drz * i for i in range(NNz)]
    print("""Диапазон изменения Rz: %1.2f - %1.2f; шаг drz: %1.4f ra (%0.2f мм); число точек: %i""" %
          (RZ[0] / sh.Ra, RZ[-1] / sh.Ra, drz / sh.Ra, drz, NNz))
    omega = Omega[0]
    DD = []  # details [ed + pd]
    RP, RK = [], []
    P, Pob, Ua, SSt_a, EEra, EEta, SSoa = [], [], [], [], [], [], []
    Uz, SSrz, SStz, EErz, EEtz = [], [], [], [], []
    rp, rk = 0, 0
    i = 0
    print("Вычисление...")
    RPdata = []
    Rp_, Rk_ = 0, 0  # с каких значений начинаються rp, rk. Ед. Ra
    ssr_positive = False
    for Rz in RZ:
        Nz = calc_Nz(sh, Rz, N)
        ed, res, pd, rp, rk = elpl(sh, Rz, N, omega, NLP_func, el_system, pl_system)
        D = pd_plus_ed(pd, ed)
        if rp != sh.Ra and Rp_ == 0:  Rp_ = rp
        if rk != sh.Ra and Rk_ == 0:  Rk_ = rk
        RP += [rp / sh.Ra]
        RK += [rk / sh.Ra]
        Uz += [D.U[Nz]]
        SSrz += [D.SSr[Nz]]
        SStz += [D.SSt[Nz]]
        EErz += [D.EEr[Nz]]
        EEtz += [D.EEt[Nz]]
        P += [-D.SSr[0] * 1000]
        SSt_a += [D.SSt[0]]
        Ua += [-D.U[0]]
        Pob += [(sh.kd * sh.gamma * (Rz - sh.Ra)) / sh.ksr * 1000]
        i += 1
        if D.SSr[0] > 0:
            print(" Радиальные напряжения на контуре выработки положительны")
            break
        if i % (NNz / 20) == 0:
            print(f"{i /NNz * 100:.0f}|", end="", flush=True)
        if P[-1] < -10: break
    print("\nПредельно разрушенная зона образуеться:  r* = %1.2f; u = %3.2f" % (Rp_ / sh.Ra, get_by(RZ, Ua, Rp_)))
    print("Предельно разрыхленная зона образуеться: r* = %1.2f; u = %3.2f" % (Rk_ / sh.Ra, get_by(RZ, Ua, Rk_)))
    Ux, PxPob = findPxP(P, Pob, Ua)
    print("Кривые горного давления и давления в случае обрушения пород пересекаються:\n %5.2f КПа; %2.2f мм" % (
    PxPob * 1000, Ux))
    print()
    RPdata += [(RZ, RP, RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega)]
    print_result_RP(RPdata[0], sh, extra_points=[(Rp_ / sh.Ra, "rp"), (Rk_ / sh.Ra, "rk")])
    show_plots = 1
    save_result_RP(RPdata, sh, result_dir, data_filename=data_file + ".csv", show=show_plots)


def main_rock_pressure():
    """
    Определение горного давления. Для статьи 2019.
    :return:
    """
    global data_file, result_dir, shaft_name
    result_dir = 'result/sph_paper2019'
    print("""\n
    Данные сохраняються в каталог %s\n""" % result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.RP.' + shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)
    shaft.sh = shaft.sh._replace(alpha=0.5, K=1)  # 2 - сфера; 1 - цилиндр.
    # alpha = 1 (старое решение)

    C = [-5.964, 6.773, -1.425]  # мар 2019. данные обновлены
    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # С1, С2 - наборы констант

    sh = shaft.sh  # для краткости.
    print(shaft.to_text())  # параметры
    Omega = [0]
    N, Nz = 3000, 100  # число точек на радиусе, число шагов по Rz
    if not sh:
        print("Error opening %s" % filename)
        sys.exit()
    # print_annotation(filename,sh,N)
    show_plots = False
    P_lining = 110  # kPa - отпор крепи. рисуется на графиках
    Cases = [
        [sh, (1.1 * sh.Ra, 3 * sh.Ra), N, Nz, Omega, el_pl2, eFDM_sph, pFDM2_ln_spher_a, func_2d_quad,  'RP.log',
         'лог. деформ.'],
        [sh, (1.1 * sh.Ra, 3 * sh.Ra), N, Nz, Omega, el_pl2, eFDM_sph, pFDM21_ln_spher_a, func_2d_quad, 'RP.log2',
         'лог. деформ.2'],
        [sh, (1.1 * sh.Ra, 3 * sh.Ra), N, Nz, Omega, el_pl2, eFDM,     pFDM2, func_2d_quad, 'RP.koshi',
         'деформ. Коши'],
    ]
    compare_RP_general(Cases, P_lining)


def main_elpl_theta():
    """
    Для статьи 2019.
    Определение НДС. Что даёт ограничение объёмной деформации?
    сферическая\цилиндрическая полость. Учёт TTz
    :return:
    """
    # TODO: кажется тут нужно заюзать те классы для графиков НДС массива в статье

    global data_file, result_dir, shaft_name
    result_dir = 'result/sph_paper2019'
    print("""\nКак влияет ограничение объёмной деформации?
    Сравнение деформаций Коши и логарифмических деформаций на примере:
    1. задачи с запредельной зоной
    Данные сохраняються в каталог %s\n""" % result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.' + shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)
    shaft.sh = shaft.sh._replace(alpha=0.5, K=1)

    C = [-5.964, 6.773, -1.425]  # мар 2019. данные обновлены
    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # 2 - сфера; 1 - цилиндр.  alpha = 1 (старое решение)

    sh = shaft.sh  # для краткости.
    sh_t = sh._replace(theta_l=0.12)
    sh_t2 = sh._replace(theta_l=1.6)

    print(shaft.to_text())  # параметры
    Omega = [0]
    N, Nz = 3000, 3000  # число точек на радиусе, число шагов по Rz
    if not sh: print("Error opening %s" % filename); sys.exit()
    # print_annotation(filename,sh,N)
    print("\n   1. НДС массива\n")
    show_plots = False

    RZ = 1.5 * sh.Ra  # из графика горно давления - за точкой пересечения кривых P и Pоб
    Cases = [ #(sh_t2, N, RZ, Omega[0], eFDM_sph, pFDM2_ln_spher_a, func_2d_quad, 'log-t2', 'лог. деформ - t07'),
             (sh_t,  N, RZ, Omega[0], eFDM_sph, pFDM21_ln_spher_a,  func_2d_quad, 'log-t',      'лог. деформ'),
             (sh_t,  N, RZ, Omega[0], eFDM,     pFDM2,              func_2d_quad, 'koshi-t',    'деформ. Коши'),
        (sh_t2, N, RZ, Omega[0], eFDM, pFDM2, func_2d_quad, 'koshi-t2', 'деформ. Коши; $\Delta\Theta$ не ограничено'),
             ]
    compare_general(Cases, show=2)


def main_elpl():
    """
    Для статьи 2019.
    Определение НДС. Сравнение нескольких случаев.
    сферическая\цилиндрическая полость. Учёт TTz
    :return:
    """
    # TODO: кажется тут нужно заюзать те классы для графиков НДС массива в статье

    global data_file, result_dir, shaft_name, show_plots
    result_dir = 'result/sph_paper2019'
    print("""\nСравнение деформаций Коши и логарифмических деформаций на примере:
    1. задачи с запредельной зоной
    Данные сохраняються в каталог %s\n""" % result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.' + shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)
    shaft.sh = shaft.sh._replace(alpha=0.5, K=1)

    C = [-5.964, 6.773, -1.425]  # мар 2019. данные обновлены
    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # 2 - сфера; 1 - цилиндр.  alpha = 1 (старое решение)
    # для проверки
    # shaft.sh = shaft.sh._replace(theta_l=2.75)

    sh = shaft.sh  # для краткости.

    print(shaft.to_text())  # параметры
    Omega = [0]
    N, Nz = 3000, 3000  # число точек на радиусе, число шагов по Rz
    if not sh: print("Error opening %s" % filename); sys.exit()
    # print_annotation(filename,sh,N)
    print("\n   1. НДС массива\n")
    show_plots = False

    RZ = 1.5 * sh.Ra  # из графика горно давления - за точкой пересечения кривых P и Pоб
    Cases = [(sh, N, RZ, Omega[0], eFDM_sph, pFDM2_ln_spher_a, func_2d_quad, 'log', 'лог. деформ'),
             (sh, N, RZ, Omega[0], eFDM_sph, pFDM21_ln_spher_a, func_2d_quad, 'log2', 'лог. деформ2'),
             # (sh_oldc, N, 1.8 * sh.Ra, Omega[0], eFDM_sph, pFDM2_ln_spher_a,  func_2d_quad, 'old_c', 'log. стар. С'),
             (sh, N, RZ, Omega[0], eFDM, pFDM2, func_2d_quad, 'koshi', 'деформ. Коши'),
             ]
    compare_general(Cases, show=2)



# сравнение для упругой зоны: лог днформации, деформации коши, Баклашов. mu = 0.5. Цилиндр
# для таблиц в статье: упругая зона
def main_el():
    global data_file, result_dir, shaft_name, show_plots
    result_dir = 'result/sph_paper2019'
    print(f"""\nСравнение деформаций Коши и логарифмических деформаций на примере:
1. задачи с запредельной зоной
2. задачай определения горного давления
Данные сохраняються в каталог {result_dir}\n""")
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.' + shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)

    # C=[sh.SSm_c, sh.beta, 0]
    C = [5.96, 6.649, -0.216]  # Квершлаг 1, гор 750
    # C1 - c
    # C2 - c'
    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # С1, С2 - наборы констант
    # 2 - сфера; 1 - цилиндр.
    # alpha = 1 (старое решение)
    shaft.sh = shaft.sh._replace(alpha=0.5, K=1)

    # старая формула для Em
    # sh2 = shaft.sh._replace(Em=shaft.sh.Elab * 0.75 * (1. + shaft.sh.ko) / 2.)
    # sh2 = shaft.sh._replace(kp=shaft.sh.kp)  # это просто способ копировать
    sh2 = shaft.sh

    sh2 = sh2._replace(Em_p=sh2.Em / (1.0 - 0.5 ** 5))
    sh2 = sh2._replace(mu=0.5, mu_p=0.5 / (1.0 - 0.5))

    shaft.sh = shaft.sh._replace(Em_p=shaft.sh.Em / (1.0 - 0.5 ** 2))
    shaft.sh = shaft.sh._replace(mu=0.5, mu_p=0.5 / (1.0 - 0.5))

    # shaft.sh = shaft.sh._replace(S = 24)

    sh = shaft.sh  # для краткости.

    print(shaft.to_text())  # параметры
    Omega = [0]
    N, Nz = 1000, 500  # число точек на радиусе, число шагов по Rz
    if not sh:
        print("Error opening %s" % filename)
        sys.exit()
    # print_annotation(filename,sh,N)
    print("\n   1. НДС массива\n")
    show_plots = False

    print("Упругая задача")
    # логиримические, коши, баклашов. Для таблицы в статье
    # упругая задача
    Cases = [(sh2, N, 1 * sh.Ra, Omega[0], eFDM, None, func_1d, 'koshi', 'деформации Коши'),
             (sh2, N, 1 * sh.Ra, Omega[0], calc_bak2, None, None, 'bakl', 'решение Баклашов'),
             (sh2, N, 1 * sh.Ra, Omega[0], eFDM_ln, None, func_1d, 'log', 'логарифмические деформации'),
             ]
    compare_general(Cases)

    print("Упругая зона")
    # логиримические, коши, баклашов. Для таблицы в статье
    # упругая зона
    # Cases =
    #    [ (sh2, N, 1.8*sh.Ra, Omega[0],    eFDM,          None, func_2d_quad,   'koshi', 'деформации Коши'),
    #      (sh2, N, 1.8*sh.Ra, Omega[0],    calc_bak2,     None, None,           'bakl',  'решение Баклашов'),
    #      (sh2, N, 1.8*sh.Ra, Omega[0],    eFDM_ln,       None, func_2d_quad,   'log',   'логарифмические деформации'),
    #           ]
    # compare_general( Cases )
