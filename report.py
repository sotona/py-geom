from pickletools import uint2

from geom import *
from geom_aux import *


"""
Кажется модуль нафиг не нежен
"""

class Reporter:
    name = ''               # имя отчёта
    result_dir = 'result'   # начальное значение. далее вычисляется абсолютный путь
    shaft_name = ''
    show_plots = True
    DPI = 600

    delimiter = " "
    fmt = "%3.4f"

    LWmain = 2  # толщина основных линий
    LWaux = 0.5  # толщина дополнительных линий

    header_nds = "r[r_a] r[mm] u[mm] SSr[MPa] SSt[MPa] Tau SS1[MPa] SS3[MPa] SSeq[MPa] SSSub[MPa] EEr EEt TT Er[MPa] SScm[MPa] SSo[MPa]"


    def __init__(self, data_file, shaft_name, calculator, report_name="", DPI=600):
        self.name = report_name
        self.result_dir = os.path.abspath(self.result_dir)
        if report_name != "" :
            self.result_dir = os.path.join(self.result_dir, self.name)

        if not os.path.exists(self.result_dir): os.mkdir(self.result_dir)
        self.data_file = data_file
        self.shaft_name = shaft_name
        # self.show_plots = show_plots
        self.DPI = 600
        # TODO:  записывать информацию о процесе вычиления
        if not os.path.exists(self.result_dir):
            os.makedirs(self.result_dir)


    def make_annotation(self, shaft, calc_param):
        """
        Преобразует в текст параметры выработки и параметры расчёта
        :param shaft:
        :param calc_param:
        :return:
        """
        return shaft.to_text() + "\n" + calc_param.to_text()


    def compare_nds(self, shaft, Rz, nds1, nds2, Nz=None):
        """
        Cравнение НДС. На выходе - таблицы в консоли.
        :param sh:
        :param Rz:
        :param nds1: НДС 1
        :param nds2: НДС 2
        :param Nz:
        :return:
        """
        sh = shaft.sh
        verbose = False
        if Nz is None:
            Nz = calc_Nz(sh, Rz, len(nds1.r))
        I = [0, calc_Nz(sh, Rz, len(nds1.r))] + [calc_Nz(sh, sh.Ra * k / 100, len(nds1.r)) for k in
                                                 [105, 110, 120, 130, 150, 200, 300]]  # номера точек
        I = list(set(I));
        I.sort();
        for i in I:
            print("r%s = %1.2f Ra, i = %i" % (("*" if i == Nz  else " "), nds1.r[i] / sh.Ra, i))
            print("U   : %8.4f | %8.4f | %8.4f  [mm]" % (nds1.U[i], nds2.U[i], abs(nds1.U[i] - nds2.U[i])))
            print("SSr : %8.4f | %8.4f | %8.4f  [MPa]" % (nds1.SSr[i], nds2.SSr[i], abs(nds1.SSr[i] - nds2.SSr[i])))
            print("SSt : %8.4f | %8.4f | %8.4f  [MPa]" % (nds1.SSt[i], nds2.SSt[i], abs(nds1.SSt[i] - nds2.SSt[i])))

        if verbose:
            du = abs(nds1.U[0] - nds2.U[0])
            dssr = abs(nds1.SSr[0] - nds2.SSr[0])
            dsst = abs(nds1.SSt[0] - nds2.SSt[0])
            if du > 20: print(
                "Перемещения на контуре выработки различаються на %f2 мм, это %3f %" % (du, du / nds1.U[0] * 100))
            # if dssr > 20: print("Радиальные напряжения на контуре выработки различаються на %f2 МПа, это %3f % или %2.2f" % (dssr, du/d1.SSr[0]*100))
            # if dsst > 20: print("Перемещения на контуре выработки различаються на %f2 мм, это %3f %" % (du, du/d1.U[0]*100))


    def save_nds(self, shaft, calc_param, Nds):
        """

        :param shaft:
        :param calc_param:
        :param name:
        :return: Список имён файлов
        """
        sh = shaft.sh
        Rb = calc_param.Rb
        N = calc_param.N

        data_file = "NDS. " + dt.now().strftime("%Y.%h%d %H.%M.%S. ")+ shaft.name

        curdir = os.getcwd()
        if os.path.abspath(curdir) != os.path.abspath(self.result_dir):
            os.chdir(self.result_dir)

        ra_ = 0.9  # начало оси абсцисс
        Nb = calc_Nz(sh, Rb, N)  # последняя точка на графиках после которой нет необходимости строить кривые
        R = [ [r / sh.Ra for r in D.r[:-1]] for D in Nds ]

        params = self.make_annotation(shaft, calc_param)

        DTfiles = []
        for r, d, Rz in zip(R, Nds, calc_param.RZ):
            if len(data_file) > 40: data_file = data_file[:39] + "..."
            fn = data_file + ".Rz=%1.2f" % (Rz / sh.Ra) + ".csv"
            DTfiles += [fn]
            data = numpy.asarray([r, d.r[:-1], d.U[:-1], d.SSr[:-1], d.SSt[:-1], d.Tau[:-1], d.SS1[:-1], d.SS3[:-1], d.SSeq[:-1], d.SSsub[:-1], d.EEr[:-1], d.EEt[:-1], d.TT[:-1],
                                  d.Er[:-1], d.SScm[:-1], d.SSo[:-1], ])
            numpy.savetxt(fn, numpy.transpose(data), delimiter=self.delimiter, fmt=self.fmt, header= self.header_nds)

            f = open(fn,'ta')
            f.write("\n")
            for line in params.split("\n"):
                f.write("# " + line + "\n")

        os.chdir(curdir)
        return DTfiles


    def make_report_nds(self, shaft, calc_praram, Nds, save=True, show=True, Note=[]):
        """
        :param shaft:
        :param calc_praram:
        :param Nds:
        :return:
        """
        sh = shaft.sh
        Rb = calc_praram.Rb
        N = calc_praram.N
        RZ = calc_praram.RZ

        fn_pref = dt.now().strftime("%Y.%h%d-%H.%M.%S=")
        R = []
        # Rb = Rb * sh.Ra
        ra_ = 0.9  # начало оси абсцисс
        Nb = calc_Nz(sh, Rb, N)  # последняя точка на графиках после которой нет необходимости строить кривые
        R += [ [r / sh.Ra for r in D.r[:-1]] for D in Nds ]

        if save: self.save_nds(shaft, calc_praram, Nds)

        curdir = os.getcwd()
        if os.path.abspath(curdir) != os.path.abspath(self.result_dir):
            os.chdir(self.result_dir)


        fig = plt.figure()
        fig.canvas.set_window_title('Напряжения. %s' % shaft.name)
        for i in range(len(Nds)):
            note = Note[i] if len(Note)>i else ""
            label_ssr = label = "%sрадиальные напряжения" % note
            label_sst = label = "%sтангенциальные напряжения" % note
            label_tau = label = "%sкасательные напряжения" % note
            if len(Nds) > 1:  # если для одной величины есть несколько графиков - подпишем каждый, чтобы отличать
                label_ssr += ". $r^* = $%0.2f $r_a$" % (RZ[i] / sh.Ra)
                label_sst += ". $r^* = $%0.2f $r_a$" % (RZ[i] / sh.Ra)
                label_tau += ". $r^* = $%0.2f $r_a$" % (RZ[i] / sh.Ra)
            plt.plot(R[i][:Nb], list(map(lambda x: x, Nds[i].SSr[:-1][:Nb])), "-" if i % 2 == 0 else "--",
                     label=label_ssr, linewidth=2)
            plt.plot(R[i][:Nb], list(map(lambda x: x, Nds[i].SSt[:-1][:Nb])), "-" if i % 2 == 0 else "--",
                     label=label_sst, linewidth=2)
            plt.plot(R[i][:Nb], list(map(lambda x: x, Nds[i].Tau[:-1][:Nb])), "-" if i % 2 == 0 else "--",
                     label=label_tau, linewidth=2)

        plt.xticks(list(arange(1, int(Rb / sh.Ra + 1))) + [RZ[0] / sh.Ra, ra_])
        xt = plt.xticks()[1][1].set_visible(False)  # TODO: удалять подписи, которые мешают подписывать r*
        xt = plt.xticks()[1][-1].set_visible(False)  # удалим подпись для ra_
        plt.yticks(list(plt.yticks()[0]) + [sh.S])
        plt.xlabel("r, [$r_a$]")
        plt.ylabel("Напряжение [МПа]")
        plt.axhline(sh.S, ls="dashed", color="red", linewidth=self.LWaux)
        plt.text(ra_, sh.S, 'S')
        for Rz in RZ: plt.axvline(Rz / sh.Ra, ls='dashed', color='black', linewidth=self.LWaux)
        plt.grid(True)
        plt.legend(loc='best', fontsize=12)
        plt.title("радиальные и тангенциальные напряжения $\sigma_r, \sigma_{\\theta}$", fontsize=16)
        if save: savefig(fn_pref + 'ssr,sst.png', dpi=DPI)
        if show: plt.show()

        fig = plt.figure()
        fig.canvas.set_window_title('Перемещения. %s' % shaft.name)

        for i in range(len(Nds)):
            note = Note[i] if len(Note) > i else ""
            plt.plot(R[i][:Nb], list(map(lambda x: -x, Nds[i].U[:-1][:Nb])), "-" if i % 2 == 0 else "--",
                     linewidth=self.LWmain,
                     label="%s$r^*$ =%0.2f" % (note, RZ[i] / sh.Ra))
        for Rz in RZ: plt.axvline(Rz / sh.Ra, ls='dashed', color='black', linewidth=self.LWaux)
        plt.xlabel("r, [$r_a$]")
        if len(Nds) > 1: plt.legend(loc='best', fontsize=12)
        plt.ylabel("Перемещения [мм]")
        plt.grid(True)

        plt.xticks(list(arange(1, int(Rb / sh.Ra + 1))) + [RZ[0] / sh.Ra, ra_])
        xt = plt.xticks()[1][1].set_visible(False)  # TODO: удалять подписи, которые мешают подписывать r*
        xt = plt.xticks()[1][-1].set_visible(False)  # удалим подпись для ra_
        plt.title("радиальные перемещения", fontsize=16)
        if save: savefig(fn_pref + 'u.png', dpi=DPI)
        if show: plt.show()

        os.chdir(curdir)
        if save: print("Графики и данные сохранены в каталог: %s" % (self.result_dir))
        return ""


    def print_result_RP(self, RPdata, sh, extra_points):
        """
        :param RPdata:
        :param sh:
        :param plt_dir:
        :param data_filename:
        :param extra_points: Дополнительные точки на R* значения для которых нужно напечатать с пометкой. [ (R в ед. Ra, текст. метка) ...]
        :return:
        """
        RZ, RP, RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega = RPdata[0]
        print("=== Значения велечин на контуре выработки и соответствующих им r*")
        print("r*[ra]   u[мм]  Pob [MPa]  P[MPa] ([S units])  SSt[MPa] ([S units])")
        N = len(Ua)
        # extra = [round((r-1)/(RZ[-1]-RZ[0])*sh.Ra*N) for r,m in extra_points]
        I = [0, ceil(N / 20), ceil(N / 10), ceil(N / 3), ceil(N / 2)]  # +extra
        I = list(set(I))
        I = list(map(lambda x: int(x), I));
        I.sort();
        I += [-1]
        for i in I:
            # mark = ",".join(j for j in filter(lambda x: y[1] if x[0]==RZ[i]/sh.Ra else None, extra_points))
            print(("%1.2f   %8.4f  %8.4f  %8.4f (%7.4f)    %8.4f (%7.4f)") % (RZ[i] / sh.Ra, Ua[i],
                                                                              Pob[i] / 1000, P[i] / 1000,
                                                                              P[i] / 1000 / sh.S, SSt_a[i],
                                                                              SSt_a[i] / sh.S))
        print()


    def save_RP(self, RPdata, sh, show=0, save=True):
        curdir = os.getcwd()
        if os.path.abspath(curdir) != os.path.abspath(self.result_dir):
            os.chdir(self.result_dir)

        shaft_name = sh.name
        RZ, RP, RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega = RPdata[0]

        data_file = "RP. " + dt.now().strftime("%Y.%h%d %H.%M.%S. ") + sh.name

        Rz_ = list(map(lambda x: x / sh.Ra, RZ[:len(Ua)]))
        header = "U_a[mm] Rz[Ra u.] Rk[Ra u.] Rp[Ra u.] P[KPa] P_ob[KPa] Uz[mm] SSrz[MPa] SStz[MPA] EErz EEtz"
        data = numpy.asarray([Ua, Rz_, RK, RP, P, Pob, Uz, SSrz, SStz, EErz, EEtz])
        numpy.savetxt(data_file, numpy.transpose(data), delimiter=" ", fmt="%3.4f", header=header)
        print("Графики и данные сохранены в каталог: %s" % (self.result_dir))
        print("Файл: " + data_file)

        fig = plt.figure()
        fig.canvas.set_window_title('Горное давление. %s' % shaft_name)
        for data in RPdata:
            Ua, P, Pob, omega = data[5], data[3], data[4], data[-1]
            plt.plot(Ua, P, label="Давление на контуре выработки $P$" + (
            (". Omega = %3.0f°" % (omega / pi * 180)) if len(RPdata) > 1 else ""), linewidth=2)
        RZ, RP, RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega = RPdata[0]
        plt.plot(Ua, Pob, label="Давление в случае обрушения пород $P_{об}$", linewidth=self.LWmain)
        # plt.xticks(arange(int(Rb/sh.Ra+1)))
        plt.xlabel("Перемещение контура выработки, $u_a$ [мм]")
        plt.ylabel("Давление [КПа]")
        # plt.axvline(sh.uw, ls='dashed', color = 'black', label="uw", linewidth = LWaux)
        plt.grid(True)
        plt.legend(loc='best', fontsize=12)
        plt.title("Горное давление $P$ и давление обрушения $P_{об}$", fontsize=16)
        if save: savefig('P,Pob.(%s).png' % shaft_name, dpi=self.DPI)
        if show: plt.show()

        fig = plt.figure()
        fig.canvas.set_window_title('Радиус пред. разрыхления и радиус пред. разрушения. %s' % shaft_name)
        plt.plot(Ua, RK, label="Радиус, на котором массив предельно разрыхлен $r_k$", linewidth=2)
        plt.plot(Ua, RP, label="Радиус, на котором массив предельно разрушен $r_p$", linewidth=2)
        plt.plot(Ua, Rz_, label="Радиус запредельной зонны $r^*$", linewidth=2)
        # plt.xticks(arange(int(Rb/sh.Ra+1)))
        plt.xlabel("Перемещение контура выработки, $u_a$ [мм]")
        plt.ylabel("Расстояние от центра выработки [ед. Ra]")
        plt.grid(True)
        plt.legend(loc='best', fontsize=12)
        plt.title("Радиус пред. разрыхления и радиус пред. разрушения", fontsize=16)
        if save: savefig('Rp,Rk.(%s).png' % shaft_name, dpi=self.DPI)
        if show > 1: plt.show()

        fig = plt.figure()
        fig.canvas.set_window_title('Радиальные и касательные напряжения на r*. %s' % shaft_name)
        plt.plot(Ua, SSrz, label=" $\sigma_r^*$", linewidth=2)
        plt.plot(Ua, SStz, label="$\sigma_\\theta^*$", linewidth=2)
        # plt.xticks(arange(int(Rb/sh.Ra+1)))
        plt.xlabel("Перемещение контура выработки, $u_a$ [мм]")
        plt.ylabel("Напряжения [MPa]")
        plt.grid(True)
        plt.legend(loc='best', fontsize=12)
        plt.title("Радиальные и касательные напряжения на r*", fontsize=16)
        if save: savefig('SSrz,SStz.(%s).png' % shaft_name, dpi=self.DPI)
        if show > 1: plt.show()
        os.chdir(prog_dir)

    def save_RP_plain(self, RPdata, sh, show=0, save=True):
        curdir = os.getcwd()
        if os.path.abspath(curdir) != os.path.abspath(self.result_dir):
            os.chdir(self.result_dir)

        shaft_name = sh.name
        TT,SSr,SSt,Tau,U = RPdata.TT,RPdata.SSr,RPdata.SSt,RPdata.Tau,RPdata.U

        data_file = "RP. " + dt.now().strftime("%Y.%h%d %H.%M.%S. ") + sh.name

        header = "TT[gr] SSr[MPa] SSt[MPA] Tau[MPa]"
        data = numpy.asarray([TT,SSr,SSt,Tau])
        numpy.savetxt(data_file, numpy.transpose(data), delimiter=" ", fmt="%3.4f", header=header)
        print("Графики и данные сохранены в каталог: %s" % (self.result_dir))
        print("Файл: " + data_file)

        fig = plt.figure()
        fig.canvas.set_window_title('Радиальные и касательные напряжения на контуре. %s' % shaft_name)
        plt.plot(TT, SSr, label=" $\sigma_r$", linewidth=2)
        plt.plot(TT, SSt, label="$\sigma_\\theta$", linewidth=2)
        plt.plot(TT, Tau, label=" $\sigma_\\tau$", linewidth=2)
        plt.plot(TT, U, label=" $u$", linewidth=2)
        # plt.xticks(arange(int(Rb/sh.Ra+1)))
        plt.xlabel("Угол, $tt$ [гр]")
        plt.ylabel("Напряжения [MPa]")
        plt.grid(True)
        plt.legend(loc='best', fontsize=12)
        plt.title("Радиальные и касательные напряжения на r*", fontsize=16)
        if save: savefig('SSrz,SStz.(%s).png' % shaft_name, dpi=self.DPI)

        if show > 1: plt.show()

        fig = plt.figure()
        fig.canvas.set_window_title('Напряжения в зависимости от перемещений. %s' % shaft_name)
        plt.plot(U, SSr, label=" $\sigma_r$", linewidth=2)
        plt.plot(U, SSt, label="$\sigma_\\theta$", linewidth=2)
        plt.plot(U, Tau, label=" $\sigma_\\tau$", linewidth=2)
        # plt.xticks(arange(int(Rb/sh.Ra+1)))
        plt.xlabel("Угол, $tt$ [гр]")
        plt.ylabel("Напряжения [MPa]")
        plt.grid(True)
        plt.legend(loc='best', fontsize=12)
        plt.title("Радиальные и касательные напряжения на r*", fontsize=16)
        if save: savefig('SSrz,SStz.(%s).png' % shaft_name, dpi=self.DPI)

        if show > 1: plt.show()
        os.chdir(prog_dir)

    def make_report_rp(self, shaft, calc_praram, rp, save=True, show=True, Note=[]):
        self.print_result_RP(rp,shaft.sh,[])
        self.save_RP(rp, shaft.sh, show = 1)


# подготовка графика для f(r)
def prepare_plot(x_lim = None, xlabel = "", ylabel = "", plot_title = "", window_title = "", rz = 1, vline = None,
                 filename = ""):
    """vline - значения, для изображения вертикальной линии"""
    plt.title(plot_title, fontsize=16)
    # fig.canvas.set_window_title(window_title)
    # leg = plt.legend(fontsize=10)
    if x_lim:
        plt.xlim(x_lim)
    plt.xlabel(xlabel); plt.ylabel(ylabel)
    if vline:
        plt.axhline(vline,ls ="dashed",color="gray", linewidth=0.5)
    if rz:
        plt.axvline(rz, ls ="dashed",color="gray", linewidth=0.5)
    plt.grid(True); plt.legend(loc='best')


# выводится больше велечин чем в compare_d
def compare_d_ex(sh, Rz, d1, d2, Nz = None):
    """
    Cравнение НДС. На выходе - таблицы в консоли.
    :param sh:
    :param Rz:
    :param d1: НДС 1
    :param d2: НДС 2
    :param Nz:
    :return:
    """
    verbose = False
    if Nz is None:
        Nz = calc_Nz(sh,Rz,len(d1.r))
    I = [0, calc_Nz(sh,Rz,len(d1.r))] + [calc_Nz(sh, sh.Ra*k/100, len(d1.r)) for k in [105, 110, 120, 130, 150, 200, 300]]  # номера точек
    I = list(set(I));  I.sort();
    for i in I:
        print("r%s = %1.2f Ra, i = %i" % (("*" if i==Nz  else " "), d1.r[i]/sh.Ra, i))
        print ("U   : %8.4f | %8.4f | %8.4f  [mm]" % (d1.U[i], d2.U[i], abs(d1.U[i] - d2.U[i])) )
        print ("SSr : %8.4f | %8.4f | %8.4f  [MPa]" % (d1.SSr[i], d2.SSr[i], abs(d1.SSr[i] - d2.SSr[i])) )
        print ("SSt : %8.4f | %8.4f | %8.4f  [MPa]" % (d1.SSt[i], d2.SSt[i], abs(d1.SSt[i] - d2.SSt[i])) )
        print ("EEt : %8.4f | %8.4f | %8.4f  []" % (d1.EEt[i], d2.EEt[i], abs(d1.EEt[i] - d2.EEt[i])))
        print ("EEr : %8.4f | %8.4f | %8.4f  []" % (d1.EEr[i], d2.EEr[i], abs(d1.EEr[i] - d2.EEr[i])))
        print ("TT  : %8.4f | %8.4f | %8.4f  []" % (d1.TT[i], d2.TT[i], abs(d1.TT[i] - d2.TT[i])))
        print ("SSo : %8.4f | %8.4f | %8.4f  [MPa]" % (d1.SSo[i], d2.TT[i], abs(d1.TT[i] - d2.TT[i])))

    if verbose:
        du = abs(d1.U[0] - d2.U[0])
        dssr = abs(d1.SSr[0] - d2.SSr[0])
        dsst = abs(d1.SSt[0] - d2.SSt[0])
        if du > 20: print("Перемещения на контуре выработки различаються на %f2 мм, это %3f %" % (du, du/d1.U[0]*100))
        # if dssr > 20: print("Радиальные напряжения на контуре выработки различаються на %f2 МПа, это %3f % или %2.2f" % (dssr, du/d1.SSr[0]*100))
        # if dsst > 20: print("Перемещения на контуре выработки различаються на %f2 мм, это %3f %" % (du, du/d1.U[0]*100))




def make_compare_line(df, tasks, col, i, format = "%10.4f |", rel_diff = False):
    """
    :param df:
    :param tasks:
    :param col: имя столбца
    :param i: номер строки в DataFrame
    :param format:
    :param rel_diff: показывать ли относительную разность?
    :return:
    """
    n = len(tasks)
    s = [ f"{col:8s}:" + ((format * n) % tuple( df[df['task']==t][ col ].iloc[i] for t in tasks) ) ]

    c2n = int( n * (n - 1) / 2 )  # число сочетаний из n по 2
    for l in range(c2n):
        for j in range(l+1,n):
            val1 = df[ df['task'] == tasks[l] ][col].iloc[i]
            val2 = df[ df['task'] == tasks[j] ][col].iloc[i]
            difff = abs(val1 - val2)

            if rel_diff:
                diff_str = format[:-2] % difff
                try:
                    rel_difff = difff/ round(val1, 5) * 100
                    diff_str += f"({rel_difff:6.1f}%)"
                except ZeroDivisionError:
                    diff_str += "(   -   )"
            else:
                diff_str = format % difff
            s += [ diff_str ]
    return "".join(s)


# сравнивается разные случаии НДС хрянящиеся в DataFrame
def print_compared_NDS(sh, Rz, df, Nz = None):
    """
    Cравнение НДС. На выходе - таблицы в консоли.
    обыно вызывается внутри функций типа compare_general
    :param sh:
    :param Rz:
    :param df: DataFrame со всеми значениями описывающими НДС и столбцом категириальных данных (df['task']),
    по которым можно отличить один случай НДС от дргуго. df создаётся внутри compare_general
    :param Nz:
    :return:
    """

    tasks = df['task'].unique()
    l = len(tasks)
    # предположим что наборов НДС в датфрейме как минимум два
    d1 = df[df.task == tasks[0]]
    if (len(tasks)>1):
        d2 = df[df.task == tasks[1]]
    else:
        d2 = d1

    r = d1['r[r_a]'].values
    verbose = False
    if Nz is None:
        Nz = calc_Nz(sh,Rz,len(r))
    I = [0, calc_Nz(sh,Rz,len(r))] + [calc_Nz(sh, sh.Ra*k/100, len(r)) for k in [105, 110, 120, 130, 150, 200, 300]]  # номера точек
    I = list(set(I));  I.sort();

    header = ["         ", "|".join([f"{t:^11s}" for t in tasks])]
    c2n = int(l * (l - 1) / 2)  # число сочетаний из n по 2
    for i in range(c2n):
        for j in range(i + 1, l):
            header += [f'{f"|{tasks[i]}-{tasks[j]}":^12s}']
    print("".join(header))
    rel_diff = True

    f = open( datetime.datetime.now().strftime("%Y.%m.%d - %H:%M") + ".txt",'w')

    for i in I:
        print("r%s = %1.2f Ra, i = %i" % (("*" if i==Nz  else " "), r[i], i))
        print(make_compare_line(df, tasks, col='SSr[MPa]', i=i, rel_diff=rel_diff))
        print(make_compare_line(df, tasks, col='SSt[MPa]', i=i, rel_diff=rel_diff))
        print(make_compare_line(df, tasks, col='u[mm]', i=i, format = "%10.3f |", rel_diff=rel_diff))
        print(make_compare_line(df, tasks, col='EEt', i=i, rel_diff=rel_diff))
        print(make_compare_line(df, tasks, col='EEr', i=i, rel_diff=rel_diff))
        print(make_compare_line(df, tasks, col='TT', i=i, rel_diff=rel_diff))
        print(make_compare_line(df, tasks, col='SSo[MPa]', i=i, rel_diff=rel_diff))

        f.write("r%s = %1.2f Ra, i = %i" % (("*" if i == Nz else " "), r[i], i))
        f.write(make_compare_line(df, tasks, col='SSr[MPa]', i=i, rel_diff=rel_diff))
        f.write(make_compare_line(df, tasks, col='SSt[MPa]', i=i, rel_diff=rel_diff))
        f.write(make_compare_line(df, tasks, col='u[mm]', i=i, format="%10.3f |", rel_diff=rel_diff))
        f.write(make_compare_line(df, tasks, col='EEt', i=i, rel_diff=rel_diff))
        f.write(make_compare_line(df, tasks, col='EEr', i=i, rel_diff=rel_diff))
        f.write(make_compare_line(df, tasks, col='TT', i=i, rel_diff=rel_diff))
        f.write(make_compare_line(df, tasks, col='SSo[MPa]', i=i, rel_diff=rel_diff))

    print(f"Файл с этим данными сохранён в {os.getcwd()+f.name}")
    f.close()


def report_compared_RP(sh: Shaft, DF:pandas.DataFrame, Cases:list, p_lining:float = None)-> list:
    """
    Cравнение ГД. На выходе - таблицы в консоли.
    обыно вызывается внутри функций типа compare_general
    :param DF:
    :param p_lining: отпор крепи
    :param Cases: набор конфигураций расчётов (предаётся так же в функцию compare_RP_general)
    :param sh: конфугурация выработки ( одна из возможны)
    :param df: DataFrame со всеми значениями описывающими ГД (RP_COLUMNS) и столбцом категириальных данных (df['task']),
    по которым можно отличить один случай НДС от дргуго. df создаётся внутри compare_RP_general
    :return: список из особых значений: пересечение отпора крепи с кривыми даления
    """

    tasks = DF['task'].unique()
    l = len(tasks)
    # предположим что наборов данных о ГД в датафрейме как минимум два
    # d1 = df[df.task == tasks[0]]
    # if (len(tasks)>1):
    #     d2 = df[df.task == tasks[1]]
    # else:
    #     d2 = d1


    # DataFrame для сравнения
    d = pandas.DataFrame(np.NaN, columns = ['task', 'ux[mm]', 'Px[kPa]', 'P10[kPa]'],
                         index = [i for i in range(len(tasks))])

    i = 0
    Ux = []  # перемещение при которо кривая давления пересекает линию отпора крепи
    for *_, task, label in Cases:
        df = DF[ DF['task']==task ]
        # ux, px = intersection_P_Pob(df['P[kPa]'], df['Pob[kPa]'], df['U_a[mm]'])
        P = df['P[kPa]']
        Pob = df['Pob[kPa]']
        U =  df['U_a[mm]']

        if p_lining:  # если задан отпор крепи
            P_df, Pob_df = np.inf, np.inf
            u1_p_l, u2_p_l = None, None
            for i in range(len(P)):
                if abs(P[i] - p_lining) < P_df:
                    P_df = abs(P[i] - p_lining)
                    u1_p_l = abs(U[i])

                if abs(Pob[i] - p_lining) < Pob_df:
                    Pob_df = abs(Pob[i] - p_lining)
                    u2_p_l = abs(U[i])

            # вертикальные линии для перемещений (пересечение отпора крепи и кривых давления)
            P_ACCUR = 500  # точность пересечения считается достаточной если меньше 5 кПа
            # print(u1_p_l, u2_p_l)
            if abs(u1_p_l) > abs(u2_p_l) or P_df > P_ACCUR or Pob_df > P_ACCUR:
                    u1_p_l = None
                    u2_p_l = None
                    # plt.axvline(-u1_p_l, color='black', ls='dashed', linewidth=LWaux)
                    # print(f"u1 = {u1_p_l}")
                    # plt.axvline(-u2_p_l, color='black', ls='dashed', linewidth=LWaux)
                    # print(f"u2 = {u2_p_l}")



            Ux += [(task, label, u1_p_l, u2_p_l)]

    # print(d)
    if p_lining:
        print()
        print(f"Области допустимых перемещений для отпора крепи {p_lining:.0f} кПа")
        for ux in Ux:
            task, label, u1, u2 = ux
            print(f"{task:10s}" + (f"{u1:7.1f} мм  - {u2:7.1f} мм" if u1 and u2 else "        нет"))
    return Ux


    # r = d1['r[r_a]'].values
    # verbose = False
    # if Nz is None:
    #     Nz = calc_Nz(sh,Rz,len(r))
    # I = [0, calc_Nz(sh,Rz,len(r))] + [calc_Nz(sh, sh.Ra*k/100, len(r)) for k in [105, 110, 120, 130, 150, 200, 300]]  # номера точек
    # I = list(set(I));  I.sort();
    #
    # header = ["         ", "|".join([f"{t:^11s}" for t in tasks])]
    # c2n = int(l * (l - 1) / 2)  # число сочетаний из n по 2
    # for i in range(c2n):
        # for j in range(i + 1, l):
    #         header += [f'{f"|{tasks[i]}-{tasks[j]}":^12s}']
    # print("".join(header))
    # rel_diff = True
    # for i in I:
    #     print("r%s = %1.2f Ra, i = %i" % (("*" if i==Nz  else " "), r[i], i))
    #     print(make_compare_line(df, tasks, col='SSr[MPa]', i=i, rel_diff=rel_diff))
    #     print(make_compare_line(df, tasks, col='SSt[MPa]', i=i, rel_diff=rel_diff))
    #     print(make_compare_line(df, tasks, col='u[mm]', i=i, format = "%10.3f |", rel_diff=rel_diff))
    #     print(make_compare_line(df, tasks, col='EEt', i=i, rel_diff=rel_diff))
    #     print(make_compare_line(df, tasks, col='EEr', i=i, rel_diff=rel_diff))
    #     print(make_compare_line(df, tasks, col='TT', i=i, rel_diff=rel_diff))
    #     print(make_compare_line(df, tasks, col='SSo[MPa]', i=i, rel_diff=rel_diff))


def print_RPcases_info(Cases):
    """
    Печатает на экран краткую информация о каждом решении
    :param Cases:  то же самое, что и для функции compare_RP_general
    """
    for case in Cases:
        sh, Rz_Rz, N, Nz, omega, el_pl, el_sys, pl_sys, func, task, label = case
        print(f"{task:15s}  {el_sys.__name__:25s}  {pl_sys.__name__:25s}   {func.__name__:25s}")