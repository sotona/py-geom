# для статьи по сферической полости. конференция СМИМ?

import seaborn
import pandas  # для DataFrame
import numpy
import matplotlib.pyplot as plt



from geom import *
from geom_aux import *
from calc import *
from report import *
from sst_ttm import *

# сравнение сферических деформаций с ...
def compare_spher_cyl(sh, N, RZ, omega=pi / 2, show=1, save=1):
    """
    Сравнить логарифмические и линейные деформациии.
    Сохраняет данные в csv и в картинки
    :param sh:
    :param N:
    :param RZ:
    :param omega:
    :param show:
    :param save:
    """
    global show_plots, prog_dir, shaft_name
    print("""
    Сравнение ... 
    """)

    # DataFrame отлично подходит для обработки данных
    DF = pandas.DataFrame(
        columns=['task', 'Rz', 'r[r_a]', 'r[mm]', 'Er[MPa]', 'SScm[MPa]', 'u[mm]', 'SSr[MPa]', 'SSt[MPa]',
                 'EEr', 'EEt', 'TT', 'SSo[MPa]'])

    # task - условное обозначений решаемой задачи (например цилиндр или сфера);
    # здесь используется Г - геометр. нелинейность; ГФ - геом. и физ. нелинейность

    for Rz in RZ:
        df = pandas.DataFrame( columns=DF.columns )
        Xrz = calc_at_rz(sh, sh.Ra)
        x0 = [Xrz[3], Xrz[1]]

        Nz = calc_Nz(sh, Rz, N)
        Ne = N - Nz

        # TODO: Нужно замутить цилиндр без логарифмов. с физической нелинейностью, без геометрической. константы C должны быть заданы
        # res_g = minimize(func_2d_quad, x0, args=(sh, Rz, Ne, eFDM_sph))
        # # sh._replace(C=C, C1=C, C2=C)
        # ed_g = eFDM_sph(res_g.x, sh, Rz, Ne)
        # pd_f, rk, rp = pFDM2_ln_spher(sh, ed_g, Rz, Nz, omega)


        # с геометрической и физической нелинейностью. константы C должны быть заданы
        res_gf = minimize(func_2d_quad, x0, args=(sh, Rz, Ne, eFDM_sph))
        # sh._replace(C=C, C1=C, C2=C)
        ed_gf = eFDM_sph(res_gf.x, sh, Rz, Ne)
        pd_gf, rk, rp = pFDM2_ln_spher(sh, ed_gf, Rz, Nz, omega)


        #  с геометрической нелинейностью, без физической
        res_g = minimize(func_2d_quad, x0, args=(sh, Rz, Ne, eFDM_sph))
        ed_g = eFDM_sph(res_g.x, sh, Rz, Ne)
        # хз. как формула получилась, но результат  то же. что и по формуле ниже
        # beta = sh.C2[1] + sh.C2[2] * ed_ln.SSr[0]
        beta = (ed_g.SSt[0] + ed_g.SScm[0]) / ed_g.SSr[0]
        C = sh.C
        C[1] = beta
        C[2] = 0
        sh._replace(C=C, C1=C, C2=C)
        ed_g = eFDM_sph(res_g.x, sh, Rz, Ne)
        pd_g, rk, rp = pFDM2_ln_spher(sh, ed_g, Rz, Nz, omega)

        d_gf = pd_plus_ed(pd_gf, ed_gf)
        d_g = pd_plus_ed(pd_g, ed_g)

        print("Rz = %0.2f Ra (%0.0f мм)" % (Rz/sh.Ra, Rz))
        print("Omega = %3.0f °" % (omega * 180 / pi))

        # print and show result
        print("\nРассматриваются: Ф - физическая нелинейность,\nГ - геометрическая нелинейность,\nГФ - геометрическая и физическая ")
        print ("\nУравнения: ФГ  | Г       | delta")

        # print ("Целев. функция: %e | %e | %e" % (res1.fun, res_ln.fun, abs(res1.fun-res_ln.fun)) )
        print(f"Целев. функция: {res_gf.fun:.2e} | {res_g.fun:.2e} | {abs(res_gf.fun - res_g.fun):.2e}")
        compare_d_ex(sh,Rz,d_gf,d_g)

        ssr_gf, ssr_g = d_gf.SSr[:-1], d_g.SSr[:-1]
        sst_gf, sst_g = d_gf.SSt[:-1], d_g.SSt[:-1]
        u_gf, u_g = d_gf.U[:-1], d_g.U[:-1]
        r_gf, r_g = [r/sh.Ra for r in d_gf.r[:-1]], [r/sh.Ra for r in d_g.r[:-1]]

        r_gf, r_g = [r / sh.Ra for r in d_gf.r[:-1]], [r / sh.Ra for r in d_g.r[:-1]]

        check_result(sh, d_gf)
        check_result(sh, d_g)

        'u[mm]', 'SSr[MPa]', 'SSt[MPa]',
        'EEr', 'EEt', 'TT', 'SSo [MPa]'

        n = len(d_gf.r[:-1])
        df['task'] = ['ГФ'] * n + ['Г'] * n
        df['Rz']        = [Rz] * 2*n
        df['r[r_a]']    = list(np.array(d_gf.r[:-1])/sh.Ra) + list(np.array(d_g.r[:-1])/sh.Ra)
        df['r[mm]']     = d_gf.r[:-1] + d_g.r[:-1]
        df['Er[MPa]']   = d_gf.Er[:-1] + d_g.Er[:-1]
        df['SScm[MPa]'] = d_gf.SScm[:-1] + d_g.SScm[:-1]
        df['u[mm]']     = u_gf + u_g
        df['SSr[MPa]']  = ssr_gf + ssr_g
        df['SSt[MPa]']  = sst_gf + sst_g
        df['EEr']       = d_gf.EEr[:-1] + d_g.EEr[:-1]
        df['EEt']       = d_gf.EEt[:-1] + d_g.EEt[:-1]
        df['TT']        = d_gf.TT[:-1]  + d_g.TT[:-1]
        df['SSo[MPa]']  = d_gf.SSo[:-1] + d_g.SSo[:-1]

        DF = DF.append(df)

    # compare_rz(sh, df)

    os.chdir(result_dir)
    fig = plt.figure()


    prepare_plot(x_lim=[0,7], xlabel= "r, [$r_a$]", ylabel="Напряжение [МПа]",
                 plot_title="радиальные и тангенциальные напряжения $\sigma_r, \sigma_{\\theta}$",
                 window_title="'Напряжения. %s'%shaft_name", vline=-sh.S, rz=Rz/sh.Ra)
    plt.plot(r_gf,ssr_gf,'--', label="радиальные напряжения. ГФ. сфера")
    plt.plot(r_gf,sst_gf,'--',label="тангенциальные напряжения. ГФ. сфера")
    plt.plot(r_g, ssr_g,label="радиальные напряжения. Г. сфера")
    plt.plot(r_g, sst_g,label="тангенциальные напряжения. Г. сфера")
    leg = plt.legend(loc = 'best', fontsize=10)
    if show: plt.show()
    if save: savefig('ssr,sst.%s.png'%shaft_name)

    fig = plt.figure()
    fig.canvas.set_window_title('Перемещения. %s'%shaft_name)

    plt.title("радиальные перемещения", fontsize=16)
    plt.xlim([0,7])
    plt.plot(r_gf, u_gf, '--', label = "ГФ. сфера")
    plt.plot(r_g,  u_g,        label = "Г. сфера")
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]"); plt.ylabel("Перемещения [мм]")
    plt.grid(True); plt.legend(loc='best')
    if save: savefig('u.%s.png'%shaft_name, dpi=DPI)
    if show:
        plt.show()


    plt.title("Относительное объёмное разрыхление $\Delta\Theta$", fontsize=16)
    plt.xlim([0.75, RZ[-1] / sh.Ra + 0.5])
    plt.plot(r_gf, df[ (df.task=='ГФ') & (df.Rz == RZ[-1]) ]['TT'], '--', label="ГФ. сфера")
    plt.plot(r_g,  df[ (df.task=='Г' ) & (df.Rz == RZ[-1]) ]['TT'],        label="Г. сфера")
    plt.axhline(0.12, ls="dashed", color="gray", linewidth=1)
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]");
    plt.ylabel("$\Delta\Theta$")
    plt.grid(True);
    plt.legend(loc='best')
    if save: savefig('TT.%s.png' % shaft_name, dpi=DPI)
    if show > 1: plt.show()


    plt.title("Остаточная прочность $\sigma_o$", fontsize=16)
    plt.xlim([0.75, RZ[-1] / sh.Ra + 0.5])
    plt.plot(r_gf, df[ (df.task=='ГФ') & (df.Rz == RZ[-1]) ]['SSo[MPa]'], '--', label="ГФ. сфера")
    # plt.plot(r_g,  DF[ (DF.task=='Г' ) & (DF.Rz == RZ[-1]) ]['SSo[MPa]'],       label="Г. сфера")
    plt.axhline(sh.ssgp*sh.SSm_c, ls="dashed", color="gray", linewidth=1)
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]");
    plt.ylabel("$\sigma_o, [MPa]$")
    plt.grid(True);
    plt.legend(loc='best')
    if save: savefig('SSo.%s.png' % shaft_name, dpi=DPI)
    if show > 1: plt.show()


    plt.title("Модуль упругости $E(r)$", fontsize=16)
    plt.xlim([0.75, 7])
    plt.plot(r_gf, df[ (df.task=='ГФ') & (df.Rz == RZ[-1]) ]['Er[MPa]'], '--', label="ГФ. сфера")
    plt.plot(r_g,  df[ (df.task=='Г' ) & (df.Rz == RZ[-1]) ]['Er[MPa]'],       label="Г. сфера")
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]");
    plt.ylabel("$E(r), [MPa]$")
    plt.grid(True);
    plt.legend(loc='best')
    if save: savefig('Er.%s.png' % shaft_name, dpi=DPI)
    if show > 1: plt.show()


    file = f"{shaft_name}.cvs"
    DF.to_csv("DF." + file, sep='\t', float_format = '%.5f')
    # DF.to_html("DF." + file1+".html", classes=["table-bordered", "t    able-striped", "table-hover"])
    os.chdir(prog_dir)
    print(f"Данные сохранены в   {result_dir}")
    print(f"Файл: {file}")


# сферические деформации
# кажется тут нужно заюзать те классы
def main_spher():
    global data_file, result_dir, shaft_name
    result_dir = 'result/sph_vs_cyl'
    print("""\nСравнение деформаций Коши и логарифмических деформаций на примере:
1. задачи с запредельной зоной
2. задачай определения горного давления
Данные сохраняються в каталог %s\n"""%result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.'+shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)

    # C=[sh.SSm_c, sh.beta, 0]
    C = [5.96, 6.649, -0.216]  # Квершлаг 1, гор 750
    # C1 - c
    # C2 - c'
    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # С1, С2 - наборы констант
    # 2 - сфера; 1 - цилиндр.
    # alpha = 1 (старое решение)
    shaft.sh = shaft.sh._replace(alpha = 0.5, K = 2)

    sh = shaft.sh  # для краткости.

    print( shaft.to_text() )  # параметры
    # sh = sh._replace(C=C, C1=C, C2=C)
    Omega = [0]
    N, Nz = 3000 , 300  # число точек на радиусе, число шагов по Rz
    if not sh: print("Eror opening %s"%filename); sys.exit()
    # print_annotation(filename,sh,N)
    print("\n   1. НДС массива\n")
    # RZ = [1*sh.Ra, 1.1*sh.Ra, 1.2*sh.Ra, 1.3*sh.Ra,  1.4*sh.Ra, 1.5*sh.Ra, 1.8*sh.Ra, 2.5*sh.Ra, 3*sh.Ra, 4*sh.Ra]
    RZ = [1*sh.Ra, 1.1*sh.Ra, 1.2*sh.Ra, 1.3*sh.Ra,  1.4*sh.Ra]
    # RZ = [4.0*sh.Ra]
    # RZ = [ 1.4*sh.Ra ]
    show_plots = False
    compare_spher_cyl(sh, N, RZ, Omega[0], show = 0)
    # print("\n2.   Горное давление при логарифмических деформациях.\n")
    # data_file_ = data_file
    # data_file = data_file_ + ".ln"

