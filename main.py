"""
Это главный файл.
Тут нужно только вызывать функции, котрые всё считают и задавать самые общие  параметры.

Для каждой новой статьи создавать свой файл со свойе функцией main, тут её только вызывать
"""
from sys import exit
from datetime import datetime as dt

from scipy.optimize import minimize
import numpy

from matplotlib.contour import QuadContourSet
import matplotlib.pyplot as plt

from pip._vendor.html5lib.html5parser import impliedTagToken            # ???
from heapq import nlargest                                              # ???


# модули программы
from geom import *
from geom_aux import *
from calc import *
from report import *
from sst_ttm import *
import draw

# файлы для статей
import giab2019
import spher



ResultDir = 'result'


class Data:
    DD = None

    def __init__(self, dd):
        """
        :param dd: list of Details
        """
        self.DD = dd





def main_el(sh, N, RZ, el_system, NLP_func):
    """
    Решение упругой задачи.
    :param sh: 
    :param N: 
    :param RZ: 
    :param el_system: 
    :param NLP_func: 
    :return: 
    """
    global show_plots, save_data
    D, res = el(sh, RZ[0], N, eFDM, NLP_func)
    rp, rk = 0,0
    print_Rzproblem(sh,res, D, RZ[0], rp, rk)
    check_result(sh, D)
    save_results_many_d([D],[""]*len(RZ),sh, RZ, N, (result_dir,data_file,shaft_name,DPI), Rb=7, show=show_plots, save=save_data)


def main_el_pl_general(sh, N, RZ, omega = pi/2, el_system = eFDM, pl_system = pFDM2, NLP_func = func_2d_quad):
    """
    Рещение задачи: упругая + запредельная зоны
    :rtype: object
    :param Rz: [mm]
    :return:
    """
    global save_data, show_plots
    print("Решение задчаи с запредельной зоной"+
    "Упругая зона: \n   " + ("деформации Коши" if el_system == eFDM else "логарифмичесие деформации") + "\n " +
          ("Параболическое условие прочности." if NLP_func == func_2d_quad else "Линейное условие прочности."))
    print("Данные получены для угла в {0} градусов ".format(omega/pi*180) + ("(кровля выработки)" if omega==pi/2 else "") )
    DD = []  # details [ed + pd]
    RP,RK = [],[]
    rp,rk = 0,0
    for Rz in RZ:
        # ed, res, pd, rp, rk = el_pl_calc(sh, Rz, N, Omega=pi/2, NLP_func=NLP_func)
        Nz = calc_Nz(sh, Rz, N)
        Ne = N - Nz
        Xrz = calc_at_rz(sh, sh.Ra)  # вычислить начальные значения для целевой функции
        x0 = [Xrz[3], Xrz[1]]
        res = minimize(NLP_func, x0, args=(sh, Rz, Ne, el_system))
        ed = el_system(res.x, sh, Rz, Ne)
        pd, rk, rp = pl_system(sh, ed, Rz, Nz, omega=omega)


def print_annotation(filename, sh, N):
    print("Выработка: %s"%filename)
    print("Естественное давление (с учётом коэф. перегрузки), S: %2.4f МПа"%sh.S)
    print("Ширина выработки, ra: %4.2f мм"%sh.Ra)
    print("Коэффициент Пуассона: %0.2f, %2.4f (для плоской деформации)"%(sh.mu, sh.mu_p))
    print("Радиус rb:  %1.2f ra"%(sh.Rb/sh.Ra))
    print("Число точек на радиусе: %i"%N)


def print_result(sh, res, d):
    """
    :param sh: Shaft
    :param res: result of minimize function from scipy.optimize
    :param d: Detail
    """
    print("Значение целевой функции: %e"%res.fun)
    print("r[ra]   u[мм]    SSr[MPa]    SSt[MPa]")
    print("%1.2f    %1.4f   %1.4f   %1.4f"%(d.r[0]/sh.Ra, d.U[0], d.SSr[0], d.SSt[0]))
    i=N//20
    print("%1.2f    %1.4f   %1.4f   %1.4f"%(d.r[i]/sh.Ra, d.U[i], d.SSr[i], d.SSt[i]))
    i=i*3
    print("%1.2f    %1.4f   %1.4f   %1.4f"%(d.r[i]/sh.Ra, d.U[i], d.SSr[i], d.SSt[i]))
    print()


def save_results(d, sh, plt_dir, data_filename, show=False):
    """
    :param show:
    :param d: Detail
    :param sh: Shaft
    :param plt_dir: dir for plots saving
    :param show: show plots?
    :return:
    """
    ssr = d.SSr[:-1]
    sst = d.SSt[:-1]
    u = d.U[:-1]
    r = [r/sh.Ra for r in d.r[:-1]]

    fig = plt.figure()
    fig.canvas.set_window_title('Напряжения. %s'%shaft_name)

    plt.plot(r,ssr,"b",label="радиальные напряжения")
    plt.plot(r,sst,"g",label="тангенциальные напряжения")
    plt.xlabel("r, [$r_a$]")
    plt.ylabel("Напряжение [МПа]")
    plt.axhline(-sh.S,ls ="dashed",color="red")
    plt.grid()
    plt.legend(loc='best')
    plt.title("радиальные и тангенциальные напряжения $\sigma_r, \sigma_{\\theta}$",fontsize=16)
    savefig(plt_dir+'/ssr,sst.png')
    if show: plt.show()

    fig = plt.figure()
    fig.canvas.set_window_title('Перемещения. %s'%shaft_name)

    plt.plot(r,u)
    plt.xlabel("r, [$r_a$]")
    plt.ylabel("Перемещения [мм]")
    plt.grid()
    plt.title("радиальные перемещения",fontsize=16)
    savefig(plt_dir+'/u.png')
    if show: plt.show()

    fig = plt.figure()
    fig.canvas.set_window_title('. %s'%shaft_name)

    os.chdir(prog_dir)
    print("Графики и данные сохранены в каталог: %s"%(result_dir))


def compare_RP_main(sh, N, NNz, Rz_Rz, RP_data1, RP_data2):
    pass


# для статьи в ФТПРПИ
def main_ln_koshi_compare():
    global data_file, result_dir, shaft_name
    result_dir = 'result/koshi.vs.log'
    print("""\nСравнение деформаций Коши и логарифмических деформаций на примере:
1. задачи с запредельной зоной
2. задачай определения горного давления
Данные сохраняються в каталог %s\n"""%result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.'+shaft_name

    shaft = ShaftParams(filename)
    sh = shaft.load_params(nobur=False)

    # sh = load_params(filename, nobur=True)


    # C=[sh.SSm_c, sh.beta, 0]
    C = [5.96, 6.649, -0.216]  # Квершлаг 1, гор 750
    sh = sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # С1, С2 - наборы констант
    # sh = sh._replace(C=C, C1=C, C2=C)
    Omega = [0]
    N, Nz = 3000 , 300  # число точек на радиусе, число шагов по Rz
    if not sh: print("Eror opening %s"%filename); sys.exit()
    # print_annotation(filename,sh,N)
    print("\n   1. НДС массива\n")
    compare_deform(sh, N, [1.9*sh.Ra], Omega[0])
    print("\n2.   Горное давление при логарифмических деформациях.\n")
    data_file_ = data_file
    data_file = data_file_ + ".ln"
    main_RP_ex(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=Omega, elpl=el_pl2, NLP_func=func_2d_quad,
               el_system=eFDM_ln, pl_system=pFDM2_ln)
    print("\n3.   Горное давление при деформациях Коши.\n")
    data_file = data_file_ + ".koshi"
    main_RP_ex(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=Omega, elpl=el_pl2, NLP_func=func_2d_quad,
               el_system=eFDM, pl_system=pFDM2)
    # main_RPll(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=Omega) # Rz-Rz диапазон изменения Rz в здаче определения горного давления


# для пластического течения
def main_flow_vs():
    global data_file, result_dir, shaft_name
    result_dir = 'result/koshi.vs.log'
    print("""\nЗадача с пластическим течением:
Данные сохраняються в каталог %s\n"""%result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.'+shaft_name
    sh = load_params(filename, nobur=False)
    C=[sh.SSm_c, sh.beta, 0]
    # C = [5.96, 6.649, -0.216]  # Квершлаг 1, гор 750
    sh = sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # С1, С2 - наборы констант
    # sh = sh._replace(C=C, C1=C, C2=C)
    Omega = [0]
    N, Nz = 300 , 300  # число точек на радиусе, число шагов по Rz
    if not sh: print("Eror opening %s"%filename); sys.exit()
    print_annotation(filename,sh,N)
    print("\n   Горное давление. Лин задача.\n")
    data_file_ = data_file
    data_file = data_file_ + ".ln"
    main_RP_ex(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=Omega, elpl=el_pl2, NLP_func=func_2d_quad,
               el_system=eFDM, pl_system=pFDM2)
    main_RP_flow(sh, N, Omega)
    print("\nГорное давление. Платическое течение.\n")
    data_file = data_file_ + ".koshi"
    # main_RP_ex(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=Omega, elpl=el_pl2, NLP_func=func_2d_quad,
    #            el_system=eFDM, pl_system=pFDM2)
    # main_RPll(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=Omega) # Rz-Rz диапазон изменения Rz в здаче определения горного давления


def main_RP_flow(sh, N, Omega):
    """
    Пластическое течение
    Построение кривых горного давления
    :param sh:
    :param N: Число точек для шага по R*
    :param NNz: Число точек для шага по R*
    :param Rz_Rz:
    :param Omega:
    :return:
    """
    global result_dir, data_file, shaft_name, show_plots
    print("""Задача определения горного давления.
    Нелинейное условие прочности.
    нелинейная функция дилатансионных состояний.
    Пластическое течение.""")

    MaxUa = 1000  # максимальное значение перемещения при котором завершаются вычисления. мм

    dr = (sh.Rb - sh.Ra)/N  # шаг вдоль r и r*
    # RZ = [Rz_Rz[0] + drz*i for i in range(NNz)]
    # print("""Диапазон изменения Rz: %1.2f - %1.2f; шаг drz: %1.4f ra (%0.2f мм); число точек: %i""" %
    #       (RZ[0]/sh.Ra, RZ[-1]/sh.Ra, drz/sh.Ra, drz, NNz) )
    omega = pi/2
    DD = []  # details [ed + pd]
    RP,RK = [],[]
    P, Pob, Ua, SSt_a, EEra, EEta, SSoa = [],[],[],[],[],[],[]
    Uz, SSrz, SStz, EErz, EEtz = [],[],[],[],[]
    rp,rk = 0,0
    i = 0
    print("Вычисление...")
    RPdata = []
    Rp_, Rk_ = 0,0  # с каких значений начинаються rp, rk. Ед. Ra
    ssr_positive = False

    Rz = sh.Ra

    # Rz = sh.Ra + dr # чтобы пропустить область неединственного решения
    print ("Начальное значение r* = {:7.2f} ед. Ra".format(Rz/sh.Ra))

    RZ = []
    ed, res = el(sh, sh.Ra, N, eFDM, func_2d_quad)  # Ra - начало,  N - число точек в упр. зоне
    pd = ed
    _pd = Detail([sh.Ra],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],1,1)
    _pd.SSr[0] = ed.SSr[0]
    _pd.EEt[0] = ed.EEt[0]
    _pd.TT[0] = ed.TT[0]
    print("r * = ", end = "")
    while (True):
        Rz = Rz + dr
        RZ += [Rz]

        Nz = calc_Nz(sh,Rz,N)  # число точек в запредельной области (считаю точку на r*)
        Ne = N - Nz
        ed, res = el(sh, Rz, Ne, eFDM, func_2d_quad)
        # pd, rk, rp = pl_flow(sh, Rz, Nz, dr, ed, _pd, omega) # <- передавать запредельную зону для предыдущего r*
        pd, rk, rp = pFDM2_flow(sh, ed, Rz, Nz, omega) # <- передавать запредельную зону для предыдущего r*

        D = pd_plus_ed(pd, ed)
        if rp != sh.Ra and Rp_ == 0:  Rp_ = rp
        if rk != sh.Ra and Rk_ == 0:  Rk_ = rk
        RP += [rp/sh.Ra]; RK += [rk/sh.Ra]
        Uz += [D.U[Nz]]; SSrz += [D.SSr[Nz]]; SStz += [D.SSt[Nz]]; EErz += [D.EEr[Nz]]; EEtz += [D.EEt[Nz]];
        P += [-D.SSr[0]*1000]; SSt_a += [D.SSt[0]]
        Ua += [-D.U[0]]
        Pob += [(sh.kd * sh.gamma * (Rz-sh.Ra))/sh.ksr * 1000]
        i+=1
        _pd = pd
        if D.SSr[0] > 0:
            print(" Радиальные напряжения на контуре выработки положительны")
            break
        if Ua[-1] > MaxUa:
            print (" Перемещения контура выработки достигли предельного значения: {0:7.2f} > {1:7.2f}".format(Ua[-1],MaxUa) )
            break
        if Rz % sh.Ra <= dr: print("%i|" % (Rz/sh.Ra), end="", flush=True)

        if P[-1] < -10 or Nz + 1 == N: break
    print("\nПредельно разрушенная зона образуеться:  r* = %1.2f; u = %3.2f" % (Rp_/sh.Ra, get_by(RZ, Ua, Rp_)))
    print("Предельно разрыхленная зона образуеться: r* = %1.2f; u = %3.2f" % (Rk_/sh.Ra, get_by(RZ, Ua, Rk_)))
    Ux, PxPob = findPxP(P,Pob,Ua)
    print("Кривые горного давления и давления в случае обрушения пород пересекаються:\n %5.2f КПа; %2.2f мм" % (PxPob*1000, Ux))
    print()
    RPdata += [(RZ,RP,RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega)]
    print_result_RP(RPdata[0], sh, extra_points=[(Rp_/sh.Ra, "rp"), (Rk_/sh.Ra,"rk")])
    save_result_RP (RPdata, sh, result_dir, data_filename=data_file + ".csv", show=show_plots)


def iterate_shafts():
    """
    выполнить действие для разных выработок
    """
    global data_file, result_dir, shaft_name, show_plots
    show_plots = 0
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)
    result_dir = 'result/all'
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    N, Nz = 350, 30  # число точек на радиусе, число шагов по Rz

    i = 0
    param_dir = "../asp/bin/params/"
    for filename in os.listdir(param_dir):
        if not filename.endswith(".ini"): continue
        filename = param_dir+filename
        shaft_name = filename.split('/')[-1]
        data_file = 'data.'+shaft_name
        sh = load_params(filename, nobur=False)
        if not sh: print("Eror opening %s"%filename); continue
        print_annotation(filename,sh,N)
        C=[sh.SSm_c, sh.beta, 0]; sh = sh._replace(C=C, C1=C, C2=C)
        compare_deform(sh, N, [1.5*sh.Ra])
        # main_el_pl(sh,N, [sh.Ra*1.3])
        i+=1
    print("Сделаны вычисления для %i выработок"%i)


def lab():
    """
    Для экспериментов
    :return: 
    """
    global data_file, result_dir, shaft_name

    # rc('font', **{'family': 'serif'})
    # rc('text', usetex=True)
    # rc('text.latex', unicode=True)
    # rc('text.latex', preamble=r"\usepackage[utf8]{inputenc}")
    # rc('text.latex', preamble=r"\usepackage[russian]{babel}")

    if not os.path.exists(result_dir):
        os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.'+shaft_name
    sh = load_params(filename, nobur=True)
    # sh = sh._replace(S=20)
    N, Nz = 3000, 30  # число точек на радиусе, число шагов по Rz
    if not sh: print("Eror opening %s"%filename); sys.exit()
    print_annotation(filename,sh,N)
    dr = (sh.Rb - sh.Ra)/N  # шаг вдоль r и r*
    RZ=[1.*sh.Ra, 1.05*sh.Ra, 1.8*sh.Ra, 2*sh.Ra, 2.7*sh.Ra]
    Omega = [0, pi/2, 3/2*pi]

    #2,737, #4,936 3-й гор
    # C=[-sh.SSm_c, sh.beta, 0]
    # C=[-sh.SSm_c, 6.24, 0]
    # C = [-sh.SSm_c, 2.737, 0.16]  # для дилатансии
    # C2 = [-sh.SSm_c, 4.936, 0.16]  # для прочности
    # C1=[5.9662, 5.2354, -0.1207]; C2 = [5.9676, 6.6070, -0.2006]
    # C=C1
    C = [5.96, 6.649, -0.216] # Квершлаг 1, гор 750
    # sh = sh._replace(C1=C1, C2 = C2,  ssgp=0.01)
    sh = sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # С1, С2 - наборы констант

    # main_el(sh,N, [sh.Ra*1.0], el_system = eFDM, NLP_func= func_1d)
    # compare_prog_bak(sh, N)
    # compare_bak_bvr_no(sh, N)

    # main_el_pl_general(sh, N, [1.05*sh.Ra], omega=0, el_system=eFDM, pl_system=pFDM2, NLP_func=func_2d_quad)

    # compare_rb(sh, N, [1.6*sh.Ra])
    # compare_strenght(sh, N, [1.8*sh.Ra])
    # for Rz in RZ:
    #     print("=============================== Rz = %1.1f" % (Rz/sh.Ra))
    #     compare_rb(sh, N, Rz)


    C = [5.96, 6.41, -0.16]  # Квершлаг 1, гор 750
    sh = sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # С1, С2 - наборы констант
    # main_RP(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=Omega)
    # main_RP_ex(sh, N, N, [sh.Ra, sh.Rb], Omega=[pi/2])
    N, Nz = 1000, 1000  # число точек на радиусе, число шагов по Rz
    # main_RP_flow(sh, N, Omega=Omega)
    # main_RP_ex(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=Omega, elpl=el_pl2, NLP_func=func_2d_quad,
    #         el_system=eFDM, pl_system=pFDM2)
    # main_RP_ex(sh, N, Nz, Rz_Rz = (sh.Ra, sh.Rb), Omega=Omega, elpl=el_pl2, NLP_func=func_2d_quad)

    # main_RPll(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=[pi/2]) # Rz-Rz диапазон изменения Rz в здаче определения горного давления

    # filename = "../asp/lbin/750_e.ini"
    # print("Вычисление коэффииентов полиномов описывающих прочность и дилатансию")
    # test_pasp13()
    # SSC = [2.98, (sh.S+sh.SSm_c/2)/2 ,sh.S]
    # cc1, cc2, e1, e2 = calc_coeffs3(sh, SSC)
    # print(cc1, cc2)



def main():
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]


    shaft = ShaftParams(filename)
    if not shaft.load_params(nobur=True):
        print("Eror opening %s" % shaft.filename)
        sys.exit()

    # C = [5.96, 6.649, -0.216]  # Квершлаг 1, гор 750
    C = [5.96, 6.649, 0]
    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # С1, С2 - наборы констант

    calc_params_nds = CalcParamsNDS(shaft.sh.Ra, shaft.sh.Rb, RZ = [1.9 * shaft.sh.Ra], N=500)

    # print_annotation(shaft, calc_params_nds)

    print("\n1.   НДС масиива для набора значений r*.\n")
    calc = Calculator(el_sys = eFDM_ln, pl_sys = pFDM2_ln, nlp_func = func_2d_quad)

    reporter = Reporter(shaft.data_file, shaft.name, calc, report_name="Test")
    print( reporter.make_annotation(shaft, calc_params_nds) )

    nds1, *other = calc.calc_nds(shaft, calc_params_nds)

    files = reporter.make_report_nds(shaft, calc_params_nds, nds1, "rp", Note=[], show=True)
    print(files)

    # print("\n2.   Горное давление\n")
    # calc_params_rp = CalcParamsRP(r1 = shaft.sh.Ra, r2 = shaft.sh.Ra*5, nds_param=calc_params_nds, dr = calc_params_nds.dr)
    # RP = calc.calc_rp(shaft, calc_params_rp)
    # reporter.make_report_rp(shaft, calc_params_rp, RP, "name", show=False)

    # print("\n3.   Горное давление. Пластическое течение\n")
    # RPflow = calc.calc_rp_flow(shaft, calc_params_rp)
    # reporter.make_report_rp(shaft, calc_params_rp, RPflow, "flow")
    # data_file_ = data_file
    # data_file = data_file_ + ".ln"
    # main_RP_ex(mysh, N, Nz, Rz_Rz = (1*mysh.Ra, 3*mysh.Ra), Omega=Omega, elpl=el_pl2, NLP_func=func_2d_quad,
    #            el_system=eFDM_ln, pl_system=pFDM2_ln)



def main_plain_2():
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]


    shaft = ShaftParams(filename)
    if not shaft.load_params(nobur=True):
        print("Eror opening %s" % shaft.filename)
        sys.exit()

    # C = [5.96, 6.649, -0.216]  # Квершлаг 1, гор 750
    C = [5.96, 6.649, 0]

    S=shaft.sh.S
    lam = shaft.sh.lam

    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01, S=S,lam=lam)  # С1, С2 - наборы констант

    calc_params_nds = CalcParamsNDS(shaft.sh.Ra, shaft.sh.Rb, RZ = [1 * shaft.sh.Ra], N=500)


    # print_annotation(shaft, calc_params_nds)

    print("\n1.   НДС масиива для набора значений r*.\n")
    calc = Calculator(el_sys = eFDM_plain_2, pl_sys = None, nlp_func = func_plain_2)

    reporter = Reporter(shaft.data_file, shaft.name, calc, report_name="Test")
    print( reporter.make_annotation(shaft, calc_params_nds) )
    nds1, *other = calc.calc_nds(shaft, calc_params_nds)

    files = reporter.make_report_nds(shaft, calc_params_nds, nds1, "rp", Note=[], show=True)
    print(files)

    # print("\n2.   Горное давление\n")
    # calc_params_rp = CalcParamsRP(r1 = shaft.sh.Ra, r2 = shaft.sh.Ra*5, nds_param=calc_params_nds, dr = calc_params_nds.dr)
    # RP = calc.calc_rp(shaft, calc_params_rp)
    # reporter.make_report_rp(shaft, calc_params_rp, RP, "name", show=False)
    #
    # print("\n3.   Горное давление. Пластическое течение\n")
    # RPflow = calc.calc_rp_flow(shaft, calc_params_rp)
    # reporter.make_report_rp(shaft, calc_params_rp, RPflow, "flow")
    # data_file_ = data_file
    # data_file = data_file_ + ".ln"
    # main_RP_ex(mysh, N, Nz, Rz_Rz = (1*mysh.Ra, 3*mysh.Ra), Omega=Omega, elpl=el_pl2, NLP_func=func_2d_quad,
    #            el_system=eFDM_ln, pl_system=pFDM2_ln)


def main_plain_1():
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]

    shaft = ShaftParams(filename)
    if not shaft.load_params(nobur=True):
        print("Eror opening %s" % shaft.filename)
        sys.exit()

    # C = [5.96, 6.649, -0.216]  # Квершлаг 1, гор 750
    C = [5.96, 6.649, 0]

    S_p=shaft.sh.S_p
    lam = shaft.sh.lam

    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01, S_p=S_p, lam=lam)  # С1, С2 - наборы констант

    calc_params_nds = CalcParamsNDS(shaft.sh.Ra, shaft.sh.Rb, RZ=[1 * shaft.sh.Ra], N=500)

    # print_annotation(shaft, calc_params_nds)

    print("\n1.   НДС масиива для набора значений r*.\n")
    calc = Calculator(el_sys=eFDM, pl_sys=None, nlp_func=func_1d)


    reporter = Reporter(shaft.data_file, shaft.name, calc, report_name="Test")
    print(reporter.make_annotation(shaft, calc_params_nds))
    nds1, *other = calc.calc_nds(shaft, calc_params_nds)

    files = reporter.make_report_nds(shaft, calc_params_nds, nds1, "rp", Note=[], show=True)
    print(files)

    # print("\n2.   Горное давление\n")
    # calc_params_rp = CalcParamsRP(r1 = shaft.sh.Ra, r2 = shaft.sh.Ra*5, nds_param=calc_params_nds, dr = calc_params_nds.dr)
    # RP = calc.calc_rp(shaft, calc_params_rp)
    # reporter.make_report_rp(shaft, calc_params_rp, RP, "name", show=False)
    #
    # print("\n3.   Горное давление. Пластическое течение\n")
    # RPflow = calc.calc_rp_flow(shaft, calc_params_rp)
    # reporter.make_report_rp(shaft, calc_params_rp, RPflow, "flow")
    # data_file_ = data_file
    # data_file = data_file_ + ".ln"
    # main_RP_ex(mysh, N, Nz, Rz_Rz = (1*mysh.Ra, 3*mysh.Ra), Omega=Omega, elpl=el_pl2, NLP_func=func_2d_quad,
    #            el_system=eFDM_ln, pl_system=pFDM2_ln)


def main_plain():
    filename = "params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]

    shaft = ShaftParams(filename)
    if not shaft.load_params(nobur=False):
        print("Eror opening %s" % shaft.filename)
        sys.exit()

    # C = [5.96, 6.649, -0.216]  # Квершлаг 1, гор 750
    C = [5.96, 6.649, 0]

    lam = 0.8

    S = 10

    tt = 45 * pi/180                  # Theta

    S_p=S*(1+lam)/2

    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01, S=S_p, lam=lam, mu=0.26, a=0.94,n=3.01, Em_p=1579)

    #shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01, S=S_p, lam=lam, mu=0.26, a=0,n=0)

    calc_params_nds = CalcParamsNDS(shaft.sh.Ra, shaft.sh.Rb, RZ=[1 * shaft.sh.Ra], N=380)

    # print_annotation(shaft, calc_params_nds)

    print("\n1.   НДС масиива для набора значений r*.\n")
    # объекты для расчётов вдоль радиуса
    calc_1 = Calculator(el_sys=eFDM,         pl_sys=None, nlp_func=func_1d)             # компоненты не зависящие от угла
    calc_2 = Calculator(el_sys=eFDM_plain_2, pl_sys=None, nlp_func=func_plain_2)        # компоненты    зависящие от угла (уго учтётся потом)

    reporter = Reporter(shaft.data_file, shaft.name, calc_2, report_name="Test")

    print(reporter.make_annotation(shaft, calc_params_nds))
    nds11, *other = calc_1.calc_nds(shaft, calc_params_nds)                             # вычисления вдоль радиуса

    #files = reporter.make_report_nds(shaft, calc_params_nds, nds11, "rp", Note=[], show=True)
    #print(files)

    shaft.sh = shaft.sh._replace(S=S)
    #print(reporter.make_annotation(shaft, calc_params_nds))


    nds12, *other = calc_2.calc_nds(shaft, calc_params_nds)

    #files = reporter.make_report_nds(shaft, calc_params_nds, nds12, "rp", Note=[], show=True)
    #print(files)

    nds1=nds_full_plain(nds11,nds12,tt,shaft.sh)                                        # вычисление с учётом угла

    files = reporter.make_report_nds(shaft, calc_params_nds, nds1, "rp", Note=[], show=True)
    print(files)


prog_dir = os.getcwd()
save_data = True



if __name__ == "__main__":

    # giab2019.main_rock_pressure_lab()
    # giab2019.main_rock_pressure()
    # giab2019.main_rock_pressure_lab_ll()
    # giab2019.main_elpl_lin()
    # giab2019.main_elpl_nottl()
    # giab2019.main_elpl()
    # main_plain()


    # spher.main_rock_pressure()
    spher.main_elpl()

    # main()

     # неосесимметричная задача
    # main_plain()               
    # main_plain_1()
    # main_plain_2()

    # iterate_shafts()
    # main_ln_koshi_compare()
    # main_flow_vs()
    # spher_paper_e.main_spher()

    # deltat_asterisk.main_el_compare()
    # deltat_asterisk.main_elpl_theta()  # что будет если не ограничивать Theta?
    # deltat_asterisk.main_elpl()
    # deltat_asterisk.main_rock_pressure()

    # calc_coefs_test()
    # pass



# plt_dir = 'plots'
# filename = "../asp/lbin/750_e.ini"
# shaft_name = filename.split('/')[-1]
# data_file = 'data.'+shaft_name+".csv"
# show_plots = False # Показывать графики во время работы программы?
# sh = load_params(filename)
# N = 1000
# if not sh: print("Eror opening %s"%filename); sys.exit()

# print_annotation(filename, sh, N)

# x0 = [0,0]
# res = minimize(funcIV_1D, x0, args=(sh, N, eFDM))
# d = eFDM(res.x, sh, N)

# save_results(d,sh,plt_dir, data_file, show=False)

# print_result(sh,res,d)
# print("Графики сохранены в каталог %s, данные в файл %s"%(plt_dir, data_file))

# draw_f(sh,N,func_2d_lin,eFDM)