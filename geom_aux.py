"""
Здесь функции и классы не занимающиеся непосредственно вычислениями
Но могут вызывать функции для вычислений
"""

__author__ = 'sotona'
from multiprocessing import Queue
import configparser
from math import sin, pi
from subprocess import Popen, PIPE
import re
from datetime import datetime as dt
from sys import exit
import os
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import numpy

import pandas

import operator

from geom import *
# from sst_ttm import *


from geom import Shaft, bur, ERA

Pasp13i = "../other/13i/pasp13i"

# толщина линий на графиках
LWmain = 1  # основная
LWaux = 0.5 # дополнительная

DPI=600
prog_dir = os.getcwd()

# названия столбцов для DataFrame, в который записывается НДС
# см. функцию el_pl_df
NDS_COLUMNS = ['Rz', 'r[r_a]', 'r[mm]', 'Er[MPa]', 'SScm[MPa]', 'u[mm]', 'SSr[MPa]', 'SSt[MPa]', 'EEr', 'EEt', 'TT',
               'SSo[MPa]']

# названия столбцов для DataFrame, в который записывается Горное давление и т.п
# см. функцию
RP_COLUMNS = ['Rz', 'U_a[mm]', 'P[kPa]','Pob[kPa]', 'SSt_a[MPa]', 'EEr_a', 'EEt_a', 'TT_a', 'U_z[mm]', 'SSr_z[MPa]', 'SSt_z[MPa]',
              'EEr_z', 'EEt_z', 'TT_z', 'rk', 'rp']


# Выработка
class ShaftParams:
    """Параметры выработка + некотррые коэффициенты
    """
    sh = None           # namedtuple Shaft со всеми параметарми выработки
    filename = ""
    name = None         
    data_file = None

    def __init__(self, filename):
        self.filename = filename
        self.name = self.filename.split('/')[-1]
        self.data_file = 'data.' + self.name


    def load_params(self, nobur=False, verbose=0):
        """
        загружает данные из файла, вычисляет некоторые параметры на соснове этих данных
        - параметры буровзрывных работ

        """
        if verbose:
            print("Загрука данных....  ", end = "")
        config = configparser.ConfigParser( allow_no_value=True )
        # allow_no_value=True -- теперь можно писать комменты в файле: с новой строки, после ;
        if len(config.read(self.filename)) == 0:
            raise Exeption("Cant open file: "+self.filename)
        try:
            S = float(config["general_params"]["S"])
            S_h = float(config["general_params"]["S_h"])        # S_h -- гориз. давление;
            kp = float(config["plastic"]["kp"])
            Elab = float(config["general_params"]["Elab"])
            ko = float(config["general_params"]["ko"])  # коэффициент структурного ослабления
            ke = float(config["plastic"]["ke"])  # коэффициент длительной прочности
            ssl_c = float(config["general_params"]["ssl_c"])
            ff0 = float(config["plastic"]["ff0"]) * pi / 180
            ff1 = float(config["plastic"]["ff1"]) * pi / 180
            mm = float(config["general_params"]["mu"])
            br = float(config["plastic"]["Br"])
            # ra = float(config["general_params"]["ra"])
            gg = float(config["plastic"]["gg"]) * 9.81e-6
            omg = float(config["plastic"]["thetta"])
            ttk = float(config["plastic"]["ttk"])
            ku = float(config["plastic"]["ku"])
            ksr = float(config["plastic"]["km"])
            kd = float(config["plastic"]["kd"])
            uw = float(config["plastic"]["uw"]) * 10
            ssgp = float(
                config["plastic"]["ssgp"]) / 100  # предельная остаточная прочность [0..1], безразмерная величина
            ssl_t = float(config["general_params"]["ssl_t"])
        except KeyError as e:
            print("======= Не все параметры корректно загружены: " + str(e) + ". " + self.filename)
            if not 'ssl_t' in locals():
                ssl_t = 10

        try:       # площадь поперечного сечния в черне                      ширина выработки
            Sblack = config["general_params"]["Sblack"]
            width = config["general_params"]["width"]
            # ra = ((float(config["general_params"]["Sblack"])/pi)**0.5 + float(config["general_params"]["width"])/2 )/2
            ra = float(width)/2
        except KeyError as e:
            print("======= Не все параметры корректно загружены: " + str(e))
            ra = float(config["general_params"]["ra"])

        print("Вычисление параметров")
        # rb = float(config["general_params"]["rb"]) * ra
        rb = 20 * ra
        n, a, k, b = bur(br, ssl_c, ra)
        if nobur:
            n, a, k, b = 0, 0, 0, 0
        # Em = Elab * 0.75 * (1. + ko) / 2.
        Em = 0.75 * ( 0.0168 + 0.733*ko + 0.119*ko**2)*Elab

        ssm_c = ssl_c * ko * ke
        aa0 = sin(ff0)
        bb = (1 + aa0) / (1 - aa0)
        Em_p = Em / (1.0 - mm * mm)
        Er = ERA(Em_p, a, n, r=1.0)
        G = Em / (2. * (1. + mm))
        # tm = Elab * 0.75 * (1 + ko) / 2 * (0.5 - mm) / (0.5 - 0.2) * (2.5 + ku) * aa0
        # tm = Em * (0.5 - mm) / (0.5 - 0.2) * (2.5 + ku) * aa0

        tm = np.NaN # None
        T = np.NaN #None

        # T =  ( ssm_c - ssm_c*ssgp) / ttk  # способ новый. но не работает(

        C = [1, 1, 1]  # для условия прочности и дилатансионного соотношения
        C1 = [1, 1, 1, 1, 1]  # для прочности
        C2 = [1, 1, 1, 1, 1]  # для дилатансионного соотношения

        # sh = Shaft(self.name, S * kp, kp, Elab, Em, Em_p, Er, ssl_c, ssl_t, ssgp, ko, ke, ku, ssm_c, mm, mm / (1.0 - mm),
        #            ra, rb, n, a, k, b,
        #            gamma=gg, G=G, beta=bb, fi0=ff0, fi1=ff1, theta_l=ttk, T=T, L=tm, ksr=ksr, kd=kd, uw=uw, C=C, C1=C1, C2=C2,
        #            alpha = 0.5, K = 2)

        S = S*kp
        lam = S_h/S                 # отнош гориз, к верт. давлению
        S_p =S * (1+lam)/2          # компонета ест. давления не зависящя от угла

        #                        +!                                                       !                     
        sh = Shaft(self.name, S, S_p, kp, Elab, Em, Em_p, Er, ssl_c, ssl_t, ssgp, ko, ke, ku, ssm_c, mm, mm / (1.0 - mm),   ra, rb, n, a, k, b,
                   #                                                            !
                   gamma=gg, G=G, beta=bb, fi0=ff0, fi1=ff1, theta_l=ttk, T=T, L=tm, ksr=ksr, kd=kd, uw=uw, C=C, C1=C1, C2=C2, alpha = 0.5, 
                   K = 2,   # коэф определяющий сферич или плоск. осесимметрич задачу. 1 - сфера, 2 - цилиндр (по-умолч)
                   lam=lam  # для неосесим. задачи
                   )
        if verbose:
            print("Готово")

        # из неосесим. задачи
        # sh = Shaft(self.name, S, S_p, kp, Elab, Em, Em_p, Er, ssl_c, ssl_t, ssgp, ko, ke,     ssm_c, mm, mm / (1.0 - mm),   ra, rb, n, a, k, b,
        #            gamma=gg, G=G, beta=bb, fi0=ff0, fi1=ff1, theta_l=ttk, T=T,       ksr=ksr, kd=kd, uw=uw, C=C, C1=C1, C2=C2, lam=lam)
        self.sh = sh
        return sh


    def calc_t_tm(self, C1, C2):
        """
        Вычисляет T и L (tm)
        :param C1: для условия прочности
        :param C2: для дилат соотношения
        :return: new sh
        """
        tm = 2.5 * self.sh.Em * (0.5 - self.sh.mu) / (0.5 - 0.2) * (C1[1] - 1) / (C1[1] + 1)
        T = tm / abs(1 - C1[1])
        self.sh = self.sh._replace(C=C1, C1=C1, C2=C2, L = tm, T = T, ssgp=0.01)
        return self.sh


    def to_text(self):
        text = self.name + "\n"
        text +="Выработка: %s" % self.filename + "\n"
        text +="Радиус выработки Ra: %4.2f мм; Rb: %2.1f ra" % (self.sh.Ra, self.sh.Rb / self.sh.Ra) + "\n"
        # text +=f"Ширина выработки: {}, высота: {}, площадь поперечного сечения (в черне),\n"

        text += f"Естественное давление {self.sh.S/self.sh.kp:2.4f} МПа, с учётом коэф. перегрузки {self.sh.kp:1.2f}), S: {self.sh.S:2.4f} МПа" + "\n"
        text += f"Плотность пород {self.sh.gamma:.4f}\n"
        text +="Предел прочности на одноосное сжатие (лабораторный, (к.ослабления. массива, к.длительной проч.)  массива):\n"
        text +="    %2.4f МПа  x %0.3f x %0.3f  =  %2.4f МПа" % (self.sh.SSl_c, self.sh.ko, self.sh.ke, self.sh.SSm_c) + "\n"
        text +="Предел прочности на одноосное растяжение (лабораторный): {0:7.2f} МПа".format(self.sh.SSl_t) + "\n"
        text +="Коэффициент Пуассона: %0.2f; %2.4f (для плоской деформации)" % (self.sh.mu, self.sh.mu_p) + "\n"
        text += f"Предельная остаточная прочность = (к_ост * Предел прочности на одноосное сжатие массива: " \
                f"{self.sh.ssgp} * {self.sh.SSm_c} = {self.sh.ssgp * self.sh.SSm_c:.4f} МПа " + "\n"
        text += f"Предельный уровень разрыхления: {self.sh.theta_l}" + "\n"
        text +="Буровзрывные работы: n = %1.3f,  k = %1.3f, a = %1.3f, b = %1.3f" % \
               ( self.sh.n, self.sh.k, self.sh.a, self.sh.b) + "\n"
        text +="""Модуль упругости. Лабораторный {0:7.2f} МПа; массива {1:7.2f} МПа; 
           массива, скорректированный для плоского деформированного состояния  {2:7.2f} МПа"""\
                   .format(self.sh.Elab, self.sh.Em, self.sh.Em_p) + "\n"
        text +="Угол внутр. трения fi0: %2.2f град." % (self.sh.fi0 * 180 / pi) + "\n"

        text += f"K = {self.sh.K} ({'сфера' if self.sh.K==2 else 'цилиндр' })" + "\n"
        text += f"alpha = {self.sh.alpha}" + "\n"
        text += f"коэф. С 0,1,2 (для дилат. соотнош: ) =    {self.sh.C1[0]:.4f} {self.sh.C1[1]:.4f} {self.sh.C1[2]:.4f}" + "\n"
        text += f"коэф. С 0,1,2 (для условия прочности: ) = {self.sh.C2[0]:.4f} {self.sh.C2[1]:.4f} {self.sh.C2[2]:.4f}" + "\n"
        text += f"T = {self.sh.T:.2f}\n"
        text += f"L = {self.sh.L:.2f}\n"

        text += "lam: %2.2f " % (self.sh.lam) + "\n"

        return text




def pd_plus_ed_old(pd, ed):
    """ объединяет массивы величин для запредельной и упругоих областей """
    D = Detail(pd.r + ed.r, pd.ro + ed.ro, pd.drRo+ed.drRo, pd.Er+ed.Er, pd.SScm+ed.SScm, pd.SSo+ed.SSo, pd.SSr+ed.SSr, pd.SSt+ed.SSt,
               pd.Tau + ed.Tau, pd.drTau + ed.drTau,
               pd.U+ed.U, pd.V+ed.V,
               pd.EEr+ed.EEr, pd.EEt+ed.EEt, pd.drSSr+ed.drSSr, pd.droSSr+ed.droSSr,
               pd.drU+ed.drU, pd.drV+ed.drV,
               pd.TT+ed.TT, pd.EEt_k+ed.EEt_k, pd.EEt_h+ed.EEt_h)
    return D


def pd_plus_ed(pd, ed):
    """ объединяет массивы величин для запредельной и упругоих областей """
    D = Detail(np.append(pd.r, ed.r),
               np.append(pd.ro, ed.ro),
               np.append(pd.drRo, ed.drRo),
               np.append(pd.Er, ed.Er),
               np.append(pd.SScm, ed.SScm),
               np.append(pd.SSo, ed.SSo),
               np.append(pd.SSr, ed.SSr),
               np.append(pd.SSt, ed.SSt),
               np.append(pd.Tau, ed.Tau),
               
               # эти переменные из неосесимметричной задачи
               np.append(pd.SS1,   ed.SS1),
               np.append(pd.SS3,   ed.SS3),
               np.append(pd.SSeq,  ed.SSeq),
               np.append(pd.SSsub, ed.SSsub),

               np.append(pd.drTau, ed.drTau),
               np.append(pd.U, ed.U),
               np.append(pd.V, ed.V),
               np.append(pd.EEr, ed.EEr),
               np.append(pd.EEt, ed.EEt),
               np.append(pd.drSSr, ed.drSSr),
               np.append(pd.droSSr, ed.droSSr),
               np.append(pd.drU, ed.drU),
               np.append(pd.drV, ed.drV),
               np.append(pd.TT, ed.TT),
               np.append(pd.EEt_k, ed.EEt_k),
               np.append(pd.EEt_h, ed.EEt_h)
               )
    return D



def dfNDS_2_dfRP(df_nds: pandas.DataFrame):
    """
    Преобразует DataFrame для NDS - nds_df в DataFrame для Горного давления
    :param df_nds: набор значений НДС для разных значений Rz, но одного значений task.
    см. функцию el_pl_df.  она выдаёт DataFrame для НДС только для одного Rz
    :return: DataFrame для Горного давления
    """

    n = len(df_nds['Rz'].unique())

    df_rp = pandas.DataFrame(np.NaN, columns = RP_COLUMNS, index = [i for i in range(n)])

    rp, rk = 0, 0
    n = len(df_nds['Rz'].unique())

    for  i, rz in enumerate( df_nds['Rz'].unique() ):
        df = df_nds[df_nds['Rz'] == rz]
        df_rp['Rz'][i] = rz
        df_rp['P[kPa]'][i] = - df['SSr[MPa]'].iloc[0]*1000
        df_rp['Pob[kPa]'][i] = 0
        df_rp['U_a[mm]'][i] = df['u[mm]'].iloc[0]
    return df_rp



def el_pl_df(sh, Rz, N, omega =pi / 2, func = func_2d_quad, el_system = eFDM, pl_system = pFDM2, x0=None):
    """
    Запускает вычисление НДС в упругой и запредельной зоне. Возвращает DataFrame c данными о НДС
    :param sh:
    :param Rz:
    :param N:
    :param omega:
    :param NLP_func:
    :param el_system:
    :param pl_system:
    :return: DataFrame(columns=NDS_COLUMNS),  rk,  rp, x
    x - значения вычисленное методом оптимизации. может пригодится как начальное значение для следующего вычисления
    """

    if x0 is None:
        Xrz = calc_at_rz(sh, sh.Ra)
        x0 = [Xrz[3], Xrz[1]]

    Nz = calc_Nz(sh, Rz, N)
    Ne = N - Nz
    rk,rp = 0,0

    if func is not None:  # в вычислениях Баклашова целевая функция не используется
        res = minimize(func, x0, args=(sh, Rz, Ne, el_system))
        x0 = res.x
    else:
        x0 = None
    data_e = el_system(x0, sh, Rz, Ne)
    if pl_system is not None:
        data_p, rk, rp = pl_system(sh, data_e, Rz, Nz, omega)
    else:
        n = 0
        data_p = Detail([Rz] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n,
                        [0] * n,
                        [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, 1, 1)

    data = pd_plus_ed(data_p, data_e)

    ssr_ln = data.SSr[:-1]
    sst_ln = data.SSt[:-1]
    u_ln = data.U[:-1]
    r_ln = [r / sh.Ra for r in data.r[:-1]]

    n = len(data.r[:-1])
    df = pandas.DataFrame(columns=NDS_COLUMNS)
    df['Rz'] = [Rz] * n
    df['r[r_a]'] = list(np.array(data.r[:-1]) / sh.Ra)
    df['r[mm]'] = data.r[:-1]
    df['Er[MPa]'] = data.Er[:-1]
    df['SScm[MPa]'] = data.SScm[:-1]
    df['u[mm]'] = u_ln
    df['SSr[MPa]'] = ssr_ln
    df['SSt[MPa]'] = sst_ln
    df['EEr'] = data.EEr[:-1]
    df['EEt'] = data.EEt[:-1]
    df['TT'] = data.TT[:-1]
    df['SSo[MPa]'] = data.SSo[:-1]

    return  df, rk, rp, x0

    # df = pandas.DataFrame(
    #     columns=['Rz', 'r[r_a]', 'r[mm]', 'Er[MPa]', 'SScm[MPa]', 'u[mm]', 'SSr[MPa]', 'SSt[MPa]',
    #              'EEr', 'EEt', 'TT', 'SSo[MPa]'])
    #
    # df['task'] = [task] * n
    # df['Rz'] = [Rz] * n
    # df['r[r_a]'] = list(np.array(data.r[:-1]) / sh.Ra)
    # df['r[mm]'] = data.r[:-1]
    # df['Er[MPa]'] = data.Er[:-1]
    # df['SScm[MPa]'] = data.SScm[:-1]
    # df['u[mm]'] = u_ln
    # df['SSr[MPa]'] = ssr_ln
    # df['SSt[MPa]'] = sst_ln
    # df['EEr'] = data.EEr[:-1]
    # df['EEt'] = data.EEt[:-1]
    # df['TT'] = data.TT[:-1]
    # df['SSo[MPa]'] = data.SSo[:-1]
    #
    # return df


def el_pl_df_ll(sh, Rz, N, omega =pi / 2, func = func_2d_quad, el_system = eFDM, pl_system = pFDM2, x0=None, id:int=0,
                results: Queue = None):
    """
    Запускает вычисление НДС в упругой и запредельной зоне. Возвращает DataFrame c данными о НДС
    :param sh:
    :param Rz:
    :param N:
    :param omega:
    :param NLP_func:
    :param el_system:
    :param pl_system:
    :return: DataFrame(columns=NDS_COLUMNS),  rk,  rp, x
    x - значения вычисленное методом оптимизации. может пригодится как начальное значение для следующего вычисления
    """
    # print(f"{id} started")
    if x0 is None:
        Xrz = calc_at_rz(sh, sh.Ra)
        x0 = [Xrz[3], Xrz[1]]

    Nz = calc_Nz(sh, Rz, N)
    Ne = N - Nz
    rk,rp = 0,0

    if func is not None:  # в вычислениях Баклашова целевая функция не используется
        res = minimize(func, x0, args=(sh, Rz, Ne, el_system))
        x0 = res.x
    else:
        x0 = None
    data_e = el_system(x0, sh, Rz, Ne)
    if pl_system is not None:
        data_p, rk, rp = pl_system(sh, data_e, Rz, Nz, omega)
    else:
        n = 0
        data_p = Detail([Rz] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n,
                        [0] * n,
                        [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, [0] * n, 1, 1)

    data = pd_plus_ed(data_p, data_e)

    ssr_ln = data.SSr[:-1]
    sst_ln = data.SSt[:-1]
    u_ln = data.U[:-1]
    r_ln = [r / sh.Ra for r in data.r[:-1]]

    n = len(data.r[:-1])
    df = pandas.DataFrame(columns=NDS_COLUMNS)
    df['Rz'] = [Rz] * n
    df['r[r_a]'] = list(np.array(data.r[:-1]) / sh.Ra)
    df['r[mm]'] = data.r[:-1]
    df['Er[MPa]'] = data.Er[:-1]
    df['SScm[MPa]'] = data.SScm[:-1]
    df['u[mm]'] = u_ln
    df['SSr[MPa]'] = ssr_ln
    df['SSt[MPa]'] = sst_ln
    df['EEr'] = data.EEr[:-1]
    df['EEt'] = data.EEt[:-1]
    df['TT'] = data.TT[:-1]
    df['SSo[MPa]'] = data.SSo[:-1]

    # return
    # print(f"{id} done")
    results.put( (id, df, rk, rp, x0) )


def get_ssc_ttm_ex(ss_t_lab, ss_cmp_lab, ko, kp, S, prog=Pasp13i, verbose=False):
    """
    Вычисляет SSc, TTm (2 варианта) по программе passp13i. Подбирает nt. Выдаёт результат для первого подходящего nt

    :param ss_t_lab:
    :param ss_cmp_lab:
    :param ko:
    :param kp:
    :param S:
    :param prog:
    :param verbose:
    :return:
    :return:
    """
    s, t1, t2 = 0,0,0
    error=False
    for nt in np.linspace(0.21, 0.99, 10):
        if verbose:
            print("nt = %3.2f"%nt, end = " ")
        s, t1, t2, error = get_ssc_ttm(ss_t_lab,ss_cmp_lab,ko,kp,S=S,nt = nt, prog=prog, verbose=verbose)
        if error==False:
            break
        else: 
            if verbose:
                print("error")
    return s, t1, t2, error
    # if error == False:
    #     return s, t1, t2
    # else:
    #     return s, t1, t2, True


def get_ssc_ttm(ss_t_lab, ss_cmp_lab, ko, kp, S, nt=0.4, prog="./pasp13i", verbose=False):
    """
    Вычисляет SSc, TTm (2 варианта) по программе passp13i.

    :param ss_t_lab: лабораторный предел прочности на растяжение
    :param ss_cmp_lab: лабораторный предел прочности на сжатие
    :param ko: коэффициент ослабления
    :param kp: коэффициент перегрузки
    :param S: давление
    :param nt: параметр нелинейного программирования
    :param prog: название исполняемого файла программы pasp13i
    :param verbose: показывать дополнительную информацию?
    :return: SSs, TTm1, TTm2, error (true\flase)
    """
    sscttm_reg = re.compile("sigmac=.*taum=.*\n")
    params = [prog, str(ss_t_lab), str(ss_cmp_lab), str(ko), str(kp), str(S), str(nt)]
    p = Popen(params, stdout=PIPE, stdin=PIPE, stderr=PIPE)
    p.stdin.write(b'\r')
    p.stdin.write(b'9\r')
    # p.stdin.write(b'0.4\r')
    stdout_data, _ = p.communicate()
    out = stdout_data.decode()
    if out.find('ВHИМАHИЕ: неупpугая зона не обpазуется') != -1:
        if verbose:
            print("ВHИМАHИЕ: неупpугая зона не обpазуется")
        return None,None,None, True
    p.wait()
    error = False
    if verbose:
        print(stdout_data.decode())
        print("="*64)
    result = sscttm_reg.findall(stdout_data.decode())
    if len(result) == 0:
        if verbose:
            print("Error getting output from %s"%prog)
        return None,None,None, True
    if out.find('Одно из ff вышло за допустимые пределы.') != -1:
        if verbose:
            print( "== Одно из ff вышло за допустимые пределы. ==")
        error = True
    if out.find("An unhandled exception occurred") != -1:
        if verbose:
            print("Exception")
        error = True
        # return None, None, None, True

    ssc =  float(result[0][result[0].find('= ')+1:result[0].find(' t')])
    ttm1 = float(result[0][result[0].find('m= ')+2:-1])
    try: ttm2 = float(result[1][result[0].find('m= ')+2:-1])
    except IndexError: ttm2 = 0  # если возникло исключение и данные о ttm2 не напечатаны
    return ssc, ttm1, ttm2, error


def print_annotation(shaft, calc_prm):
    sh = shaft.sh
    N = calc_prm.N
    print("Выработка: %s"%shaft.filename)
    print("Половина ширины выработки Ra: %4.2f мм; Rb: %2.1f ra" % (sh.Ra, sh.Rb/sh.Ra))
    print("Число точек на радиусе N: %i; шаг dr: %0.2f мм" % (N, (sh.Rb - sh.Ra)/N))
    print("Естественное давление (с учётом коэф. перегрузки), S: %2.4f МПа" %sh.S)
    print("Предел прочности на одноосное сжатие (лабораторный, (к.ост. прочности, к.длительной проч.)  массива):\n"
          "    %2.4f МПа  x %0.3f x %0.3f  =  %2.4f МПа"% (sh.SSl_c, sh.ko, sh.ke, sh.SSm_c))
    print("Предел прочности на одноосное растяжение (лабораторный): {0:7.2f} МПа".format(sh.SSl_t))
    print("Коэффициент Пуассона: %0.2f; %2.4f (для плоской деформации)"%(sh.mu, sh.mu_p))
    print("Влияние буровзрывных работ " + ("НЕ "if sh.a==sh.b==0 else "" )+"учитываеться" +
          ((": n = %1.3f,  k = %1.3f, a = %1.3f, b = %1.3f" % (sh.n,  sh.k,sh.a, sh.b)) if not sh.a==sh.b==0 else "" ))
    print("""Модуль упругости. Лабораторный {0:7.2f} МПа; массива {1:7.2f} МПа;"
    массива, скорректированый для плоского деформиованного состония {2:7.2f} МПа""".format(sh.Elab, sh.Em, sh.Em_p))
    print("Угол внутр. трения fi0: %2.2f град." % (sh.fi0*180/pi))
    # print("Коэффициенты c0, c1, c2 (общие для дилат соотношения и усл. прочности): {0}, {1}, {2}".format(sh.C[0], sh.C[1], sh.C[2]))
    # print("Радиус rb:  %1.2f ra\n"%(sh.Rb/sh.Ra))
# sh = load_params("asp/bin/params/Квершлаг_№1_гор_750.ini")


def intersection_P_Pob(P, Pob, Ua):
    """
    :param P:
    :param Pob:
    :param Ua:
    :return: Ux, Px - Точка пересечения кривых горного давления и давления в случае обрушения пород
    """
    px, ux = 0,0
    ix = 9
    for i in range(len(P)):
        if Pob[i] > P[i]:
            ix = i
            break

    x1 = abs(Ua[ix - 1])
    y1 = P[ix - 1]

    x2 = abs(Ua[ix])
    y2 = P[ix]

    x3 = x1
    x4 = x2

    y3 = Pob[ix-1]
    y4 = Pob[ix]

    t = ( (x1-x3)*(y3-y4) - (y1-y3)*(x3-x4) ) / ( (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4) )
    u = ( (x1-x2)*(y1-y3) - (y1-y2)*(x1-x3) ) / ( (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4) )

    ux = x1 + t * (x2 - x1)
    px = y1 + t * (y2 - y1)

    return ux, px



def get_func_info(fdm_system):
    """Вовзвращает краткое описание функции (для упругой или запредельной зоны) - первую строку документации
    :param fdm_system:
    :return:
    """
    s = ""
    if (fdm_system is not None):
        s =  fdm_system.__doc__
        s = s.split('\n')[0] if s.split('\n')[0] != "" else s.split('\n')[1]


    return s.strip('\n ')


def print_Rzproblem(sh, res, d, Rz, rp, rk):
    """
    Выводит результаты решения задачи определения НДС: значения параметров для нескольких значений радиуса
    :param sh: Shaft
    :param res: result of minimize function from scipy.optimize
    :param d: Detail
    """
    print("")
    print("Радиус запредельной зоны: %0.2f мм ( %0.4f Ra)" % (Rz, Rz/sh.Ra) )
    print("Значение целевой функции: %e"%res.fun)
    print("Радиус предельно разрушенной зоны: %1.2f ra (%0.4f мм)"%(rp/sh.Ra, rp))
    print("Радиус предельно разрызленной зоны: %1.2f ra (%0.4f мм)"%(rp/sh.Ra, rp))
    print("")
    print("r[ra]     u[мм]    SSr[MPa] ([S units])      SSt[MPa] ([S units])")
    N = len(d.r)
    Nz = calc_Nz(sh,Rz,len(d.r))
    I = [0, calc_Nz(sh,Rz,len(d.r))] + [calc_Nz(sh, sh.Ra*k/100, len(d.r)) for k in [105, 110, 120, 130, 150]]  # номера точек
    I = list(set(I));  I.sort()
    for i in I:
        print(("%1.2f" + ("*" if i==Nz else " " ) +"   %8.4f   %8.4f  (%8.4f)     %8.4f  (%8.4f)")%(d.r[i]/sh.Ra, d.U[i], d.SSr[i],d.SSr[i]/sh.S, d.SSt[i], d.SSt[i]/sh.S))
    print()


def make_label(main_label, other):
    pass

# def save_results_many_d(D, Note, sh, RZ, N, prog_param, Rb=7, show=0, save=1):
#     """
#     Сохраняет графики и данные.
#     Напряжения и перемещения показаны положительными (изменён знак)
#
#     :param D:  list of Detail
#     :param Note: list of string notes
#     :param sh: Shaft
#     :param RZ: - array of Rz [mm]
#     :param plt_dir: dir for plots saving
#     :param Rb [Ra units]
#     :param show: show plots?
#     :return:
#     """
#     global LWmain, LWaux
#     # префикс для имён файлов
#     result_dir, data_filename, shaft_name, DPI = prog_param
#     curdir = os.getcwd()
#     fn_pref = dt.now().strftime("%Y.%h%d-%H.%M.%S=")
#     R = []
#     Rb = Rb*sh.Ra
#     ra_ = 0.9  # начало оси абсцисс
#     Nb = calc_Nz(sh, Rb, N)  # последняя на графиках после которой нет необходимости строить кривые
#     for d in D:
#         R += [[r/sh.Ra for r in d.r[:-1]]]
#
#     os.chdir(result_dir)
#     DTfiles = []
#     header = "r[r_a] r[mm] u[mm] SSr[MPa] SSt[MPa] EEr EEt TT Er[MPa] SScm[MPa] SSo[MPa]"
#     for r,d,Rz in zip(R, D, RZ):
#         if len(data_filename)>40: data_filename=data_filename[:39]+"..."
#         fn = fn_pref+data_filename + ".Rz=%1.2f"%(Rz/sh.Ra)+".csv"
#         DTfiles += [fn]
#         data = numpy.asarray([r, d.r[:-1], d.U[:-1], d.SSr[:-1], d.SSt[:-1], d.EEr[:-1], d.EEt[:-1], d.TT[:-1],
#                               d.Er[:-1], d.SScm[:-1], d.SSo[:-1],])
#         if save:
#             numpy.savetxt(fn,  numpy.transpose(data), delimiter=" ", fmt="%3.4f", header = header)
#     fig = plt.figure()
#     fig.canvas.set_window_title('Напряжения. %s'%shaft_name)
#     for i in range(len(D)):
#         note = Note[i]
#         label_ssr = label="%sрадиальные напряжения" % note
#         label_sst = label="%sтангенциальные напряжения" % note
#         if len(D) > 1: # если для одной величины есть несколько графиков - подпишем каждый, чтобы отличать
#             label_ssr += ". $r^* = $%0.2f $r_a$" % (RZ[i]/sh.Ra)
#             label_sst += ". $r^* = $%0.2f $r_a$" % (RZ[i]/sh.Ra)
#         plt.plot(R[i][:Nb], list(map(lambda x: -x, D[i].SSr[:-1][:Nb])), "-" if i%2==0 else "--",
#                  label=label_ssr, linewidth = 2)
#         plt.plot(R[i][:Nb], list(map(lambda x: -x, D[i].SSt[:-1][:Nb])), "-" if i%2==0 else "--",
#                  label=label_sst, linewidth = 2)
#     # plt.xticks(arange(int(Rb / sh.Ra + 1)))
#     plt.xticks(list(arange(1, int(Rb/sh.Ra+1))) + [RZ[0]/sh.Ra, ra_])
#     xt = plt.xticks()[1][1].set_visible(False)  # TODO: удалять подписи, которые мешают подписывать r*
#     xt = plt.xticks()[1][-1].set_visible(False) # удалим подпись для ra_
#     plt.yticks( list(plt.yticks()[0]) + [sh.S])
#     plt.xlabel("r, [$r_a$]")
#     plt.ylabel("Напряжение [МПа]")
#     plt.axhline(sh.S,ls ="dashed",color="red", linewidth = LWaux)
#     plt.text(ra_, sh.S, 'S')
#     for Rz in RZ: plt.axvline(Rz/sh.Ra, ls='dashed', color = 'black', linewidth = LWaux)
#     plt.grid(True)
#     plt.legend(loc='best', fontsize=12)
#     plt.title("радиальные и тангенциальные напряжения $\sigma_r, \sigma_{\\theta}$",fontsize=16)
#     if save: savefig(fn_pref+'ssr,sst.png', dpi=DPI)
#     if show: plt.show()
#
#     fig = plt.figure()
#     fig.canvas.set_window_title('Перемещения. %s'%shaft_name)
#
#     for i in range(len(D)):
#         note = Note[i]
#         plt.plot(R[i][:Nb],list(map(lambda x: -x, D[i].U[:-1][:Nb])), "-" if i%2==0 else "--", linewidth = LWmain,
#                  label = "%s$r^*$ =%0.2f" % (note, RZ[i]/sh.Ra))
#     for Rz in RZ: plt.axvline(Rz/sh.Ra, ls='dashed', color = 'black', linewidth = LWaux)
#     plt.xlabel("r, [$r_a$]")
#     if len(D)>1: plt.legend(loc='best', fontsize = 12)
#     plt.ylabel("Перемещения [мм]")
#     plt.grid(True)
#     # plt.xticks(arange(ra_, int(Rb/sh.Ra+1)))
#     plt.xticks(list(arange(1, int(Rb / sh.Ra + 1))) + [RZ[0] / sh.Ra, ra_])
#     xt = plt.xticks()[1][1].set_visible(False)  # TODO: удалять подписи, которые мешают подписывать r*
#     xt = plt.xticks()[1][-1].set_visible(False) # удалим подпись для ra_
#     plt.title("радиальные перемещения",fontsize=16)
#     if save: savefig(fn_pref+'u.png', dpi=DPI)
#     if show: plt.show()
#
#     fig = plt.figure()
#     fig.canvas.set_window_title('. %s'%shaft_name)
#
#     i = 0
#     while i<Nb and D[0].Er[i]==0: i+=1
#     plt.plot(R[0][i:Nb],D[0].Er[:-1][i:Nb], linewidth = LWmain, label = "E(r)")
#     plt.plot(R[0][:Nb], list(map(lambda x:x*1000,D[0].SScm[:-1][:Nb])), linewidth = LWmain, label = "SSc(r) x 1000")
#     # for Rz in RZ: plt.axvline(Rz/sh.Ra, ls='dashed', color = 'black')
#     plt.xlabel("r, [$r_a$]")
#     plt.legend(loc='best', fontsize=12)
#     plt.ylabel(" [МПа]")
#     plt.grid(True)
#     plt.xticks(arange(int(Rb/sh.Ra+1)))
#     plt.title("Прочность массива",fontsize=16)
#     if save: savefig(fn_pref+'Er,SSc.png', dpi=DPI)
#     if show>1: plt.show()
#     os.chdir(curdir)
#     if save: print("Графики и данные сохранены в каталог: %s"%(result_dir))
#     print(DTfiles)


def print_result_RP(RPdata, sh, extra_points):
    """
    :param RPdata:
    :param sh:
    :param plt_dir:
    :param data_filename:
    :param extra_points: Дополнительные точки на R* значения для которых нужно напечатать с пометкой. [ (R в ед. Ra, текст. метка) ...]
    :return:
    """
    RZ,RP,RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega = RPdata
    print("=== Значения велечин на контуре выработки и соответствующих им r*")
    print("r*[ra]   u[мм]  Pob [MPa]  P[MPa] ([S units])  SSt[MPa] ([S units])")
    N = len(Ua)
    # extra = [round((r-1)/(RZ[-1]-RZ[0])*sh.Ra*N) for r,m in extra_points]
    I = [0, ceil(N/20), ceil(N/10), ceil(N/3), ceil(N/2)]  #+extra
    I = list(set(I))
    I = list(map(lambda x: int(x),I)); I.sort(); I+=[-1]
    for i in I:
        # mark = ",".join(j for j in filter(lambda x: y[1] if x[0]==RZ[i]/sh.Ra else None, extra_points))
        print(("%1.2f   %8.4f  %8.4f  %8.4f (%7.4f)    %8.4f (%7.4f)")%(RZ[i]/sh.Ra, Ua[i],
            Pob[i]/1000, P[i]/1000, P[i]/1000/sh.S, SSt_a[i], SSt_a[i]/sh.S))
    print()


def save_result_RP(RPdata, sh, result_dir, data_filename, show=0, save=True):
    global prog_dir, DPI, LWmain, LWaux
    shaft_name = sh.name
    RZ,RP,RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega = RPdata[0]
    os.chdir(result_dir)
    Rz_ = list(map(lambda x: x/sh.Ra, RZ[:len(Ua)]))
    header = "U_a[mm] Rz[Ra-u.] Rk[Ra-u.] Rp[Ra-u.] P[KPa] P_ob[KPa] Uz[mm] SSrz[MPa] SStz[MPA] EErz EEtz"
    data = numpy.asarray([Ua, Rz_, RK, RP, P, Pob, Uz, SSrz, SStz, EErz, EEtz])
    numpy.savetxt(data_filename,  numpy.transpose(data), delimiter=" ", fmt="%3.4f", header = header)
    print("Графики и данные сохранены в каталог: %s"%(result_dir))
    print("Файл: "+data_filename)

    fig = plt.figure()
    fig.canvas.set_window_title('Горное давление. %s'%shaft_name)
    for data in RPdata:
        Ua, P, Pob, omega = data[5], data[3], data[4], data[-1]
        plt.plot(Ua, P, label="Давление на контуре выработки $P$" + ((". Omega = %3.0f°"%(omega/pi*180)) if len(RPdata)>1 else ""), linewidth = 2)
    RZ,RP,RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega = RPdata[0]
    plt.plot(Ua, Pob, label="Давление в случае обрушения пород $P_{об}$", linewidth = LWmain)
    # plt.xticks(arange(int(Rb/sh.Ra+1)))
    plt.xlabel("Перемещение контура выработки, $u_a$ [мм]")
    plt.ylabel("Давление [КПа]")
    # plt.axvline(sh.uw, ls='dashed', color = 'black', label="uw", linewidth = LWaux)
    plt.grid(True)
    plt.legend(loc='best', fontsize=12)
    plt.title("Горное давление $P$ и давление обрушения $P_{об}$",fontsize=16)
    if save: savefig('P,Pob.(%s).png'%shaft_name, dpi=DPI)
    if show: plt.show()

    fig = plt.figure()
    fig.canvas.set_window_title('Радиус пред. разрыхления и радиус пред. разрушения. %s'%shaft_name)
    plt.plot(Ua, RK, label="Радиус, на котором массив предельно разрыхлен $r_k$", linewidth = 2)
    plt.plot(Ua, RP, label="Радиус, на котором массив предельно разрушен $r_p$", linewidth = 2)
    plt.plot(Ua, Rz_, label="Радиус запредельной зонны $r^*$", linewidth = 2)
    # plt.xticks(arange(int(Rb/sh.Ra+1)))
    plt.xlabel("Перемещение контура выработки, $u_a$ [мм]")
    plt.ylabel("Расстояние от центра выработки [ед. Ra]")
    plt.grid(True)
    plt.legend(loc='best', fontsize=12)
    plt.title("Радиус пред. разрыхления и радиус пред. разрушения",fontsize=16)
    if save: savefig('Rp,Rk.(%s).png'%shaft_name, dpi=DPI)
    if show>1: plt.show()

    fig = plt.figure()
    fig.canvas.set_window_title('Радиальные и касательные напряжения на r*. %s'%shaft_name)
    plt.plot(Ua, SSrz, label=" $\sigma_r^*$", linewidth = 2)
    plt.plot(Ua, SStz, label="$\sigma_\\theta^*$", linewidth = 2)
    # plt.xticks(arange(int(Rb/sh.Ra+1)))
    plt.xlabel("Перемещение контура выработки, $u_a$ [мм]")
    plt.ylabel("Напряжения [MPa]")
    plt.grid(True)
    plt.legend(loc='best', fontsize=12)
    plt.title("Радиальные и касательные напряжения на r*",fontsize=16)
    if save: savefig('SSrz,SStz.(%s).png'%shaft_name, dpi=DPI)
    if show>1: plt.show()
    os.chdir(prog_dir)



def check_result(sh, d):
    """
    Проверка правильности вычислений
    Радиальные напряжения:
    - монотонно убывают
    Разрыв на границе запредельной зоны?
    :param sh:
    :param d:
    :return:
    """
    # print ("Замечания" + "="*40)
    # if abs(d.SSr[-1] - sh.S) < eps:
    #     print("- Радиальные напряжения на Rb сильно отличаются от S = %3.3f MPa. (S - SSr) = %3.3f МПа;" % (sh.S, sh.S - d.SSr[-1]))
    # if abs(d.SSt[-1] - sh.S) < eps:
    #     print("- Косательные напряжения на Rb сильно отличаются от S = %3.3f MPa. (S - SSt) = %3.3f МПа;" % (sh.S, sh.S - d.SSt[-1]))
    if d.U[0] > 0:
        print("- Перемещения на контуре выработки положительны;")
    if d.SSr[0] > 0:
        print("- Радиальные напряжения на контуре выработки положительны;")
    if d.SSt[0] > 0:
        print("- Косательные напряжения на контуре выработки положительны;")

    dr = (d.r[-1] - d.r[0]) / len(d.r)
    Cont = [Ur_ok, SSrr_ok, SStr_ok] = [], [], [] # функции без скачков
    too_big = [100, 100, 100]

    for X, cont, name, max in zip([d.U, d.SSr, d.SSt], Cont, ["U(r)", "SSr(r)", "SSt(r)"], too_big):
        is_big=False
        for i in range(1,len(d.r)-1):
            if max > X[i]: is_big = True
            if abs( (X[i+1] - X[i])/dr - (X[i] - X[i-1])/dr) > 0.007:
                cont += [ (d.r[i]/sh.Ra, X[i])]
        # if len(cont):
        #     print("- график %s имеет разрывы в т. "%name + '; '.join(map(lambda x: "( %2.2f, %2.2f )" % x, cont[:5])) +
        #           ("" if len(cont)<5 else "...") + " ?")
        if is_big: pass
    print("="*49, end='\n\n')


# возвращает перемещения соответ-е радиусу, и т.п.
def get_by(Key,Value, key):
    for i in range(1, min(len(Key), len(Value))):
        if Key[i-1] <= key < Key[i]: return Value[i]
    return 0