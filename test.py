#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Минимизация целевой функции происхожит с помощью метода BFGS (Алгоритм Бройдена — Флетчера — Гольдфарба — Шанно),
квазиньютоновский метод.

Данные (графики и CSV файлы сохраняются в каталог results)

функции начинающиеся на main выполняют законченную работу, можно вызывать из main функции

"""
from matplotlib.contour import QuadContourSet

__author__ = 'sotona'
from datetime import datetime as dt
from sys import exit
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import numpy
from geom import *
from geom_aux import *
from sst_ttm import *
from main import main_el_pl_general

import os

"""
Проверочные данные
  0. описание задачи
     - что определяется?
     - зоны деформации
     - системы уравнений
     - целевая функция
     - функция прочности, дилатансионное соотношение
  1. исходные данные
     - название выработки
     - свойства масива
     - учитываются БВР?
     - число точек, Rb, Rz
  2. набор значений напряжений и перемещений для разных радиусов
"""


def test_el(sh, N, Rz):
    D, res = el(sh, RZ[0], N, eFDM, func_1d)


def test_difference(val1, val2, val_name, eps = 0.001):
    if abs(val1 - val2) > eps:
        print(f"{val_name:7s} - FAIL;  delta = {val1 - val2:4f}")
        return False
    else:
        print(f"{val_name:7s}- OK")
        return True


def test_all():
    """
    Тестирует только вычисления
    Запускает все функции вычислений
    :return:
    """
    eps = 0.001

    global result_dir,data_file,shaft_name,show_plots
    print("\n===  ТЕСТ  ===\n")
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.'+shaft_name
    show_plots = 0  # Показывать графики во время работы программы?

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)
    sh = shaft.sh

    N, Nz = 300, 100  # число точек на радиусе, число шагов по Rz
    if not sh: print("Eror opening %s"%filename); sys.exit()
    # print_annotation(filename,sh,N)
    RZ=[1.8*sh.Ra]


    # print("1.  Проверка решения упругой задачи")
    # print("2.1 Проверка решения задачи с запредельной зоной. Линейная функция разупрочнения")
    # print("2.2 Проверка решения задачи с запредельной зоной. Нелинейная функция разупрочнения")
    # print("3.  Проверка решения задачи с запредельной зоной. "\
    #       "Нелинейная функция разупрочнения, нелинейная функция дилатансионных состояний")
    # print("4. Логарифмические деформации. "\
    #         "Линейная функция разупрочнения, линейная функция дилатансионных состояний")

    print("\n"+"="*80);
    #2,737, #4,936 3-й гор
    C=[-sh.SSm_c, 6.41, 0.16]
    C1 = [-sh.SSm_c, 2.737, 0.16]  # для дилатансии
    C2 = [-sh.SSm_c, 4.936, 0.16]  # для прочности
    sh = sh._replace(C=[-sh.SSm_c, 6.4, 0.0135], C2 = [-sh.SSm_c, 5.103, 0.0868])

    # Определение НДС. логарифмические деформации
    DD = main_el_pl_general(sh, N, RZ, omega=pi/2, NLP_func=func_2d_lin, el_system=eFDM_ln, pl_system=pFDM2_ln)
    # Будем считать эти данные эталонными
    # r[ra]     u[мм]    SSr[MPa] ([S units])      SSt[MPa] ([S units])
    # 1.00    -12.0523     0.0831  (  0.0069)       0.4722  (  0.0392)
    # 1.06    -11.2757     0.1178  (  0.0098)       0.6946  (  0.0576)
    # 1.12    -10.5629     0.1638  (  0.0136)       0.9892  (  0.0820)
    # 1.22     -9.5989     0.2386  (  0.0198)       0.9982  (  0.0828)
    # 1.31     -8.7812     0.2510  (  0.0208)      -0.1072  ( -0.0089)
    # 1.52     -7.5150    -0.2806  ( -0.0233)      -7.1022  ( -0.5889)
    # 1.80*    -6.6141    -2.5322  ( -0.2100)     -20.7243  ( -1.7184)

    print('='*20)

    test_difference(DD[0].SSr[0],  0.0831,  val_name = 'SSr_a', eps = 0.0001)
    test_difference(DD[0].SSt[0],  0.4722,  val_name = 'SSt_a', eps = 0.0001)
    test_difference(DD[0].U[0],   -12.0523, val_name = 'U_a',   eps = 0.0001)

    Nz = calc_Nz(sh, RZ[0], len(DD[0].r))
    test_difference(DD[0].SSr[Nz],  -2.5322 ,  val_name = 'SSr*', eps = 0.0001)
    test_difference(DD[0].SSt[Nz],  -20.7243,  val_name = 'SSt*', eps = 0.0001)
    test_difference(DD[0].U[Nz],    -6.6141,   val_name = 'U*',   eps = 0.0001)




result_dir = 'result\\test'
data_file = ''  # значение присваевается в функции main
shaft_name = '' # значение присваевается в функции main
show_plots = 1  # Показывать графики во время работы программы?
DPI=300

test_all()


# TODO
# сравнивать с предыдущим результатом
# тесты для основных вычислений


# NOTES
# нельзя сохранить данные в другой каталог, ибо запускаются функции из main которые используют свои настройки пути сохранения


# Похоже это должно быть правильно. Потому, что оно похоже на данные статьи ФТПРПИ. Только в статье есть ошибка
 # SSR        -0.0165
# SSt[MPa]:   -0.1693