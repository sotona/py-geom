"""
Для решения неососимметричной задачи
Это самый новый вариант расчётов
"""


from multiprocessing import Process, Queue
import pandas  # для DataFrame
from geom import *
from geom_aux import *
from calc import *
from report import *
from sst_ttm import *
from draw import *



# Эта функция потом должна стать основой для более общей. Надо бы тут те классы заюзать
# noinspection PyPep8Naming
def compare_general(Cases, show=1, save=1):
    """ Сравнивает несколько задач (cases) упругости
    улучшенная по сравнению с compare_general. Юзать именно эту
    :param Cases: [ (sh, N, Rz,  Omega, el_sys, pl_sys, func, name, label), (,,,,,,)]  func - целевая функция
    name - краткое обозначение задачи. далее используется в DataFrame
    label  используется дял подписи графиков
    :param show:
    :param save:
    :return:
    """
    global prog_dir, shaft_name
    print("""
       Вычисление ... 
       """)

    # DataFrame отлично подходит для обработки данных
    # task - краткое обозначение решаемой задачи
    DF = pandas.DataFrame(
        columns=['task', 'Rz', 'r[r_a]', 'r[mm]', 'Er[MPa]', 'SScm[MPa]', 'u[mm]', 'SSr[MPa]', 'SSt[MPa]',
                 'EEr', 'EEt', 'TT', 'SSo[MPa]'])

    # task - условное обозначений решаемой задачи (например цилиндр или сфера);
    # здесь (уже нет) используется Г - геометр. нелинейность; ГФ - геом. и физ. нелинейность

    print("Решаемые задачи: ")
    for case in Cases:
        el_sys, pl_sys = case[4:6]
        print(get_func_info(el_sys))
        print(get_func_info(pl_sys))
    print()

    for case in Cases:
        sh, N, Rz, omega, el_sys, pl_sys, func, task, label = case
        print(f"{task:10s} ", end="", flush=True)

        df,rk,rp, _ = el_pl_df(sh, Rz, N, omega, func, el_sys, pl_sys)  # вычисления
        df['task'] = [task] * len(df)

        DF = DF.append(df, sort=False)
        print("Готово")

    print()

    sh = Cases[0][0]
    Rz = Cases[0][2]

    print_compared_NDS(sh, Rz, DF)
    NDS_plots(sh, Rz, DF, Cases, shaft_name, show, save)

    os.chdir(result_dir)

    file = f"{shaft_name}.cvs"
    DF.to_csv("DF." + file, sep='\t', float_format='%.5f')
    # DF.to_html("DF." + file1+".html", classes=["table-bordered", "t    able-striped", "table-hover"])
    os.chdir(prog_dir)
    print(f"Данные сохранены в   {result_dir}")
    print(f"Файл: {file}")



def main_elpl():
    """
    Для статьи в ГИАБ: 2019.
    Определение НДС. Сравнение нескольких случаев.
    сферическая\цилиндрическая полость. Учёт TTz
    :return:
    """
    # TODO: кажется тут нужно заюзать те классы для графиков НДС массива в статье

    global data_file, result_dir, shaft_name, show_plots
    result_dir = 'result/sph_paper2019'
    print("""\nСравнение деформаций Коши и логарифмических деформаций на примере:
    1. задачи с запредельной зоной
    Данные сохраняються в каталог %s\n""" % result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.' + shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)
    shaft.sh = shaft.sh._replace(alpha=0.5, K=1)# 2 - сфера; 1 - цилиндр.  alpha = 1 (старое решение)

    C = [-5.96774232,  5.2382325, 0.1221227 ]  # мар. 2020. по МНК

    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01)
    shaft.sh = shaft.calc_t_tm(C1=C, C2=C)
    # для проверки
    # shaft.sh = shaft.sh._replace(theta_l=2.75)

    sh = shaft.sh  # для краткости.

    print(shaft.to_text())  # параметры
    Omega = [0]
    N, Nz = 3000, 3000  # число точек на радиусе, число шагов по Rz
    if not sh: print("Error opening %s" % filename); sys.exit()
    # print_annotation(filename,sh,N)
    print("\n   1. НДС массива\n")
    show_plots = False

    RZ = 2.0 * sh.Ra  # из графика горно давления - за точкой пересечения кривых P и Pоб
    Cases = [
             (sh, N, RZ, Omega[0], eFDM_sph, pFDM21_ln_spher_a, func_2d_quad, 'log1', 'лог. деформ.'),
             (sh, N, RZ, Omega[0], eFDM,     pFDM2, func_2d_quad, 'koshi', 'деформ. Коши'),
             ]
    compare_general(Cases, show=2)



def main_elpl_nottl():
    """
    Для статьи в ГИАБ: 2019.
    Определение НДС. Сравнение нескольких случаев.
    сферическая\цилиндрическая полость. Учёт TTz
    TT не ограничено
    :return:
    """
    # TODO: кажется тут нужно заюзать те классы для графиков НДС массива в статье

    global data_file, result_dir, shaft_name, show_plots
    result_dir = 'result/sph_paper2019'
    print("""\nСравнение деформаций Коши и логарифмических деформаций на примере:
    1. задачи с запредельной зоной
    Данные сохраняються в каталог %s\n""" % result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.' + shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)
    shaft.sh = shaft.sh._replace(alpha=0.5, K=1)

    C = [-5.96774232,  5.2382325, 0.1221227 ]  # мар. 2020. по МНК

    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # 2 - сфера; 1 - цилиндр.  alpha = 1 (старое решение)
    # для проверки
    # shaft.sh = shaft.sh._replace(theta_l=2.75)
    shaft.sh = shaft.calc_t_tm(C1=C, C2=C)

    sh = shaft.sh  # для краткости.

    sh2 = sh._replace(theta_l=100)

    print(shaft.to_text())  # параметры
    Omega = [0]
    N, Nz = 3000, 3000  # число точек на радиусе, число шагов по Rz
    if not sh: print("Error opening %s" % filename); sys.exit()
    # print_annotation(filename,sh,N)
    print("\n   1. НДС массива\n")
    show_plots = False

    RZ = 2.0 * sh.Ra  # из графика горно давления - за точкой пересечения кривых P и Pоб
    Cases = [
             (sh, N, RZ, Omega[0], eFDM_sph, pFDM21_ln_spher_a, func_2d_quad, 'log1', 'лог. деформ.'),
             (sh, N, RZ, Omega[0], eFDM, pFDM2, func_2d_quad, 'koshi', 'деформ. Коши'),
             # (sh2, N, RZ, Omega[0], eFDM_sph, pFDM21_ln_spher_a, func_2d_quad, 'log1_t', 'лог. деформ; $\Delta\\theta$ не ограничено'),
             (sh2, N, RZ, Omega[0], eFDM, pFDM2, func_2d_quad, 'koshi_t', 'деформ. Коши; $\Delta\\theta$ не ограничено'),
             ]
    compare_general(Cases, show=2)


def main_elpl_lin():
    """
    Для статьи в ГИАБ: 2019. Сравнение со случаем, где c2 = 0
    Определение НДС. Сравнение нескольких случаев.
    сферическая\цилиндрическая полость. Учёт TTz
    :return:
    """
    # TODO: кажется тут нужно заюзать те классы для графиков НДС массива в статье

    global data_file, result_dir, shaft_name, show_plots
    result_dir = 'result/sph_paper2019'
    print("""\nСравнение деформаций Коши и логарифмических деформаций на примере:
    1. задачи с запредельной зоной
    Данные сохраняються в каталог %s\n""" % result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.' + shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)
    shaft.sh = shaft.sh._replace(alpha=0.5, K=1)

    C = [-5.96774232,  5.2382325, 0.1221227 ]  # мар. 2020. по МНК

    N, Nz = 3000, 3000  # число точек на радиусе, число шагов по Rz
    RZ = 2.0 * shaft.sh.Ra  # из графика горно давления - за точкой пересечения кривых P и Pоб

    # beta, phi = calc_beta_sinfi(shaft.sh, RZ, calc_Nz(shaft.sh, RZ, N), eFDM_sph, func_2d_quad)

    print(f"beta = {beta:.2f}")
    # Clin = [-5.96774232, beta, 0]  # мар. 2020. по МНК

    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # 2 - сфера; 1 - цилиндр.  alpha = 1 (старое решение)
    # для проверки
    # shaft.sh = shaft.sh._replace(theta_l=2.75)

    sh = shaft.sh  # для краткости.
    sh_lin = sh._replace(C=Clin, C1=Clin, C2=Clin, ssgp=0.01)  # 2 - сфера; 1 - цилиндр.  alpha = 1 (старое решение)

    print(shaft.to_text())  # параметры
    Omega = [0]

    if not sh: print("Error opening %s" % filename); sys.exit()
    # print_annotation(filename,sh,N)
    print("\n   1. НДС массива\n")
    show_plots = False

    Cases = [
             (sh, N, RZ, Omega[0], eFDM_sph, pFDM21_ln_spher_a, func_2d_quad, 'log1', 'лог. деформ.'),
             (sh_lin, N, RZ, Omega[0], eFDM_sph, pFDM21_ln_spher_a, func_2d_quad, 'log2', 'лог. деформ. лин'),
             ]
    compare_general(Cases, show=2)


# noinspection PyPep8Naming
def compare_RP_general(Cases:list, p_lining:float, show=1, save=1):
    """ Сравнивает несколько задач (cases) определения ГД

    :param Cases: [sh, (1 * sh.Ra, 3 * sh.Ra), Omega, el_pl2, func_2d_quad, eFDM_sph, pFDM2_ln_spher_a, name='RP.koshi', title='деформ. Коши' ]
    name - краткое обозначение задачи. далее используется в DataFrame
    label  используется дял подписи графиков
    :param p_lining: отпор крепи (кПа). рисуется на графике горного давления
    :param show:
    :param save:
    :return:
    """
    global prog_dir, shaft_name
    print("""
       Решение задачи Горного давления... 
       """)

    # DataFrame отлично подходит для обработки данных
    # task - краткое обозначение решаемой задачи

    DF_RP = pandas.DataFrame(columns=['task'] + RP_COLUMNS)

    # task - условное обозначений решаемой задачи (например цилиндр или сфера);
    # здесь (уже нет) используется Г - геометр. нелинейность; ГФ - геом. и физ. нелинейность

    print("Решаемые задачи: ")
    print_RPcases_info(Cases)  # краткая информация о решаемом случае (используемые функции)
    # for case in Cases:
    #     el_sys, pl_sys = case[4:6]
    #
    #     print(get_func_info(el_sys))
    #     print(get_func_info(pl_sys))
    print()
    for case in Cases:
        sh, Rz_Rz, N, Nz, omega, el_pl, el_sys, pl_sys, func, task, label = case

        DF_NDS = pandas.DataFrame(columns= NDS_COLUMNS)

        print(f"Вычисление для {label}")
        drz = (Rz_Rz[1] - Rz_Rz[0]) / Nz
        RZ = [Rz_Rz[0] + drz * i for i in range(Nz)]
        print("""Диапазон изменения Rz: %1.2f - %1.2f; шаг drz: %1.4f ra (%0.2f мм); число точек: %i""" %
              (RZ[0] / sh.Ra, RZ[-1] / sh.Ra, drz / sh.Ra, drz, Nz))
        DD = []  # details [ed + pd]

        RP, RK = [], []
        Pob = []

        i = 0
        d = 20  # печатать прогресс после прохождения каждой 1/d части вычислений

        rp_, rk, Rp_, Rk_ = 0, 0, 0, 0  # с каких значений начинаються rp, rk. Ед. Ra
        print("Выполнение %: ", end="")
        x = None
        for Rz in RZ:
            df_nds, rk, rp, x = el_pl(sh, Rz, N, omega, func, el_sys, pl_sys, x)  # вычисления
            df_nds['task'] = [task] * len(df_nds)
            df_nds['Rz'] = [Rz] * len(df_nds)

            DF_NDS = DF_NDS.append(df_nds, sort=False)

            if rp != sh.Ra and Rp_ == 0:  Rp_ = rp
            if rk != sh.Ra and Rk_ == 0:  Rk_ = rk
            RP += [rp / sh.Ra]
            RK += [rk / sh.Ra]
            Pob += [(sh.kd * sh.gamma * (Rz - sh.Ra)) / sh.ksr * 1000]
            i += 1

            if i % (len(RZ) / d) == 0:
                print(f"{i /len(RZ) * 100:.0f}|", end="", flush=True)
            if df_nds['SSr[MPa]'][0] < -10 or i == len(RZ):
                break

        df = dfNDS_2_dfRP(DF_NDS)
        df['Pob[kPa]'] = Pob
        df['task'] = [task]*len(df)
        df['rk'] = RK
        df['rp'] = RP
        DF_RP = DF_RP.append(df, sort=False)

        print()

    pandas.set_option('display.expand_frame_repr', False)
    print(DF_RP)

    os.chdir(result_dir)
    Ux = report_compared_RP(sh, DF_RP, Cases, p_lining=p_lining)
    RP_plots(sh, DF_RP, Cases, shaft_name,  p_lining=p_lining, Ux=Ux, show=True, save=True)


    # DF.to_csv(data_file + ".csv")
    # print_result_RP(rp_data[0], sh, extra_points=[(Rp_ / sh.Ra, "rp"), (Rk_ / sh.Ra, "rk")])
    # show_plots = 1
    # save_result_RP(rp_data, sh, result_dir, data_filename=data_file + ".csv", show=show_plots)
    os.chdir(prog_dir)


def main_rock_pressure():
    """
    Определение горного давления. Для статьи 2019.
    :return:
    """
    global data_file, result_dir, shaft_name
    result_dir = 'result/sph_paper2019'
    print("""\n
    Данные сохраняються в каталог %s\n""" % result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini", "1й_гор_850.ini", "Трансп_Квершлаг_(вост).ini"][2]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.RP.' + shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)
    shaft.sh = shaft.sh._replace(alpha=0.5, K=1)  # 2 - сфера; 1 - цилиндр. alpha = 1 (старое решение)
    shaft.sh = shaft.sh._replace(kp=2.1)



    # транспортный квершлаг
    # C1 = [-4.97115591, 8.14426637, 0.82731468 ]  # для дилат соотношения. вар1
    # C2 = [-5.04669175, 9.28246425, 0.92677933  ]    # для условия прочности. вар2

    # # 3й_гор_750.ini
    # C1 = [-12.16046422, 3.16497573, 0.04876827]  # для дилат соотношения. вар1
    # C2 = [-12.16002281, 5.09286254, 0.08612381]  # для условия прочности. вар2


    #
    # 1й_гор_850.ini
    C1 = [-7.29696468, 3.40581572, 0.0512782   ]  # для дилат соотношения. вар1
    C2 = [-7.29693346, 5.33242644, 0.089152  ]    # для условия прочности. вар2

    # # Квершлаг_№1_гор_750.ini
    # C1 = [- 5.96797557, 5.23496187,   0.1206308   ]  # для дилат соотношения. вар1
    # C2 = [- 5.96935644, 6.60361532, 0.19914921  ]    # для условия прочности. вар2

    shaft.sh = shaft.sh._replace(C=C1, C1=C1, C2=C2, ssgp=0.01)  # 2 - сфера; 1 - цилиндр.  alpha = 1 (старое решение)
    shaft.sh = shaft.calc_t_tm(C1=C1, C2=C2)

    sh = shaft.sh
    print(shaft.to_text())  # параметры
    Omega = [0]
    N, Nz = 3000, 30  # число точек на радиусе, число шагов по Rz
    if not sh:
        print("Error opening %s" % filename)
        sys.exit()
    # print_annotation(filename,sh,N)
    show_plots = False
    P_lining = 114  # kPa - отпор крепи. рисуется на графиках
    Cases = [
        [sh, (1.2 * sh.Ra, 3 * sh.Ra), N, Nz, Omega, el_pl_df, eFDM_sph, pFDM21_ln_spher_a, func_2d_quad,
         'RP.log.', 'лог. деформ.'],
        # [sh, (1.2 * sh.Ra, 3 * sh.Ra), N, Nz, Omega, el_pl_df, eFDM,     pFDM2,             func_2d_quad,
        #  'RP.koshi', 'деформ. Коши'],
    ]
    compare_RP_general(Cases, P_lining)


def main_rock_pressure_lab():
    """
    Определение горного давления. Для статьи 2019. lab - для экспериментов
    :return:
    """
    global data_file, result_dir, shaft_name
    result_dir = 'result/sph_paper2019'
    print("""\n
    Данные сохраняються в каталог %s\n""" % result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini", "1й_гор_850.ini"][0]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.RP.' + shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)
    shaft.sh = shaft.sh._replace(alpha=0.5, K=1)  # 2 - сфера; 1 - цилиндр. alpha = 1 (старое решение)
    shaft.sh = shaft.sh._replace(kp=2.1)

    Cnew = [- 5.96774232, 5.2382325,   0.1221227]  # мар. 2020. по МНК, Квершлаг_№1_гор_750
    # Cnew = [- 5.96774232, 6.191166859334617,   0]  # Квершлаг_№1_гор_750. beta через sin(fi) угол из условия прочности
    # Cnew = [ -5.96935644, 6.60361532, 0.19914921]  # Квершлаг_№1_гор_750. pasp13i (по варианту 2)

    # Cnew = [-12.1601316,  3.32701508,  0.050918 ]    # 3й_гор_750.ini  kp=1.8 ( pasp13i  вар 1)
    # Cnew = [-12.1599497,  3.16561436, 0.0489618 ]    # 3й_гор_750.ini  kp=2.1 ( pasp13i  вар 1)
    # Cnew = [-12.1599497,  4.928034518420049, 0 ]    # 3й_гор_750.ini  beta через sin(fi) угол из условия прочности
    # Cnew = [-12.1599497,  3.0874132786839246, 0 ]    # 3й_гор_750.ini  beta через sin(fi) угол из дилат соотношения

    # Cnew = [-12.16038781, 5.09394968, 0.08639434]  # 3й_гор_750.ini  kp=2.1 ( pasp13i  вар 2)

    # Cstrength = [-12.16038781, 5.09394968, 0.08639434]  # 3й_гор_750.ini  kp=2.1 ( pasp13i  вар 2)  условие прончости
    # Cdilat =    [-12.1599497,  3.16561436, 0.0489618]   # 3й_гор_750.ini  kp=2.1 ( pasp13i  вар 1)       дилат. соот.


    # shaft.sh = shaft.sh._replace(C=Cold, C1=Cold, C2=Cold,
    #                              ssgp=0.01)  # 2 - сфера; 1 - цилиндр.  alpha = 1 (старое решение)
    # для проверки
    # shaft.sh = shaft.sh._replace(theta_l=2.75)

    sh_old = shaft.sh  # для краткости.
    #                               дилат.соот.    усл. прочн.
    sh_new = sh_old._replace(C=Cnew, C1=Cdilat, C2=Cstrength, ssgp=0.01)  # 2 - сфера; 1 - цилиндр.  alpha = 1 (старое решение)

    print(shaft.to_text())  # параметры
    Omega = [0]
    N, Nz = 3000, 25  # число точек на радиусе, число шагов по Rz
    if not sh_new:
        print("Error opening %s" % filename)
        sys.exit()
    # print_annotation(filename,sh,N)
    show_plots = False
    P_lining = 113  # kPa - отпор крепи. рисуется на графиках
    Cases = [
        # [sh_new, (1.4 * sh_old.Ra, 2.7 * sh_old.Ra), N, Nz, Omega, el_pl_df, eFDM_sph, pFDM21_ln_spher_a, func_2d_quad, 'RP.log',
        #  'лог. деформ.'],
        [sh_new, (1.4 * sh_old.Ra, 2.7 * sh_old.Ra), N, Nz, Omega, el_pl_df, eFDM_sph, pFDM21_ln_spher_a, func_2d_quad,
         'RP.log.', 'лог. деформ.'],
        # [sh_new, (1.4 * sh_old.Ra, 2.7 * sh_old.Ra), N, Nz, Omega, el_pl_df, eFDM,     pFDM2, func_2d_quad,
        #  'RP.koshi', 'деформ. Коши'],

        # [sh_old, (1.1 * sh_old.Ra, 3 * sh_old.Ra), N, Nz, Omega, el_pl2, eFDM,     pFDM2, func_2d_quad, 'RP.koshi',
        #  'деформ. Коши'],

        # [sh_new, (1.1 * sh_old.Ra, 3 * sh_old.Ra), N, Nz, Omega, el_pl2, eFDM,     pFDM2, func_2d_quad, 'RP.koshi2',
        #  'деформ. Коши. нов'],
    ]
    compare_RP_general(Cases, P_lining)


# TODO: сделать основной функцией
# noinspection PyPep8Naming
def compare_RP_general_ll(Cases:list, p_lining:float, show=1, save=1):
    """ Сравнивает несколько задач (cases) определения ГД

    :param Cases: [sh, (1 * sh.Ra, 3 * sh.Ra), Omega, el_pl2, func_2d_quad, eFDM_sph, pFDM2_ln_spher_a, name='RP.koshi', title='деформ. Коши' ]
    name - краткое обозначение задачи. далее используется в DataFrame
    label  используется дял подписи графиков
    :param p_lining: отпор крепи (кПа). рисуется на графике горного давления
    :param show:
    :param save:
    :return:
    """
    global prog_dir, shaft_name
    print("""
       Решение задачи Горного давления... 
       """)

    # DataFrame отлично подходит для обработки данных
    # task - краткое обозначение решаемой задачи

    DF_RP = pandas.DataFrame(columns=['task'] + RP_COLUMNS)

    # task - условное обозначений решаемой задачи (например цилиндр или сфера);
    # здесь (уже нет) используется Г - геометр. нелинейность; ГФ - геом. и физ. нелинейность

    print("Решаемые задачи: ")
    print_RPcases_info(Cases)  # краткая информация о решаемом случае (используемые функции)
    # for case in Cases:
    #     el_sys, pl_sys = case[4:6]
    #
    #     print(get_func_info(el_sys))
    #     print(get_func_info(pl_sys))
    print()
    for case in Cases:
        sh, Rz_Rz, N, Nz, omega, _, el_sys, pl_sys, func, task, label = case

        DF_NDS = pandas.DataFrame(columns= NDS_COLUMNS)

        print(f"Вычисление для {label}")
        drz = (Rz_Rz[1] - Rz_Rz[0]) / Nz
        RZ = [Rz_Rz[0] + drz * i for i in range(Nz)]
        print("""Диапазон изменения Rz: %1.2f - %1.2f; шаг drz: %1.4f ra (%0.2f мм); число точек: %i""" %
              (RZ[0] / sh.Ra, RZ[-1] / sh.Ra, drz / sh.Ra, drz, Nz))

        rp_, rk, Rp_, Rk_ = 0, 0, 0, 0  # с каких значений начинаються rp, rk. Ед. Ra
        print("Выполнение %: ", end="")
        x = None
        results_q = Queue()
        procs = []  # список процессов
        for i,Rz in enumerate(RZ):
            procs += [ Process(target=el_pl_df_ll, args=(sh, Rz, N, omega, func, el_sys, pl_sys, x, i, results_q)) ]
            # df_nds, rk, rp, x = el_pl(sh, Rz, N, omega, func, el_sys, pl_sys, x)  # вычисления
            procs[-1].start()
            print(">", end="")

        print()
        print("waiting...")

        Results = []
        for _ in range(len(RZ)):
            Results += [ results_q.get() ]
            print("|", end="")

        print()

        RP, RK = [], []
        Pob = []
        Results.sort(key = lambda x: x[0])
        print("len results = ", len(Results))
        for r,Rz in zip(Results, RZ):
            id, df_nds, rk, rp, x = r
            df_nds['task'] = [task] * len(df_nds)
            df_nds['Rz'] = [Rz] * len(df_nds)

            DF_NDS = DF_NDS.append(df_nds, sort=False)

            if rp != sh.Ra and Rp_ == 0:  Rp_ = rp
            if rk != sh.Ra and Rk_ == 0:  Rk_ = rk
            RP += [rp / sh.Ra]
            RK += [rk / sh.Ra]
            Pob += [(sh.kd * sh.gamma * (Rz - sh.Ra)) / sh.ksr * 1000]

            if df_nds['SSr[MPa]'][0] < -10:
                break

        df = dfNDS_2_dfRP(DF_NDS)
        df['Pob[kPa]'] = Pob
        df['task'] = [task]*len(df)
        df['rk'] = RK
        df['rp'] = RP
        DF_RP = DF_RP.append(df, sort=False)

        print()

    pandas.set_option('display.expand_frame_repr', False)
    print(DF_RP)

    os.chdir(result_dir)
    Ux = report_compared_RP(sh, DF_RP, Cases, p_lining=p_lining)
    RP_plots(sh, DF_RP, Cases, shaft_name,  p_lining=p_lining, Ux=Ux, show=True, save=True)
    os.chdir(prog_dir)


# TODO: сделать основной функцией
def main_rock_pressure_lab_ll():
    """
    Определение горного давления. Для статьи 2019. lab - для экспериментов
    :return:
    """
    global data_file, result_dir, shaft_name
    result_dir = 'result/sph_paper2019'
    print("""\n
    Данные сохраняються в каталог %s\n""" % result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini", "1й_гор_850.ini"][0]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.RP.' + shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)
    shaft.sh = shaft.sh._replace(alpha=0.5, K=1)  # 2 - сфера; 1 - цилиндр. alpha = 1 (старое решение)
    shaft.sh = shaft.sh._replace(kp=2.1)

    Cnew = [- 5.96774232, 5.2382325,   0.1221227]  # мар. 2020. по МНК, Квершлаг_№1_гор_750
    # Cnew = [- 5.96774232, 6.191166859334617,   0]  # Квершлаг_№1_гор_750. beta через sin(fi) угол из условия прочности
    # Cnew = [ -5.96935644, 6.60361532, 0.19914921]  # Квершлаг_№1_гор_750. pasp13i (по варианту 2)

    # Cnew = [-12.1601316,  3.32701508,  0.050918 ]    # 3й_гор_750.ini  kp=1.8 ( pasp13i  вар 1)
    # Cnew = [-12.1599497,  3.16561436, 0.0489618 ]    # 3й_гор_750.ini  kp=2.1 ( pasp13i  вар 1)
    # Cnew = [-12.1599497,  4.928034518420049, 0 ]    # 3й_гор_750.ini  beta через sin(fi) угол из условия прочности
    # Cnew = [-12.1599497,  3.0874132786839246, 0 ]    # 3й_гор_750.ini  beta через sin(fi) угол из дилат соотношения

    # Cnew = [-12.16038781, 5.09394968, 0.08639434]  # 3й_гор_750.ini  kp=2.1 ( pasp13i  вар 2)

    # Cstrength = [-12.16038781, 5.09394968, 0.08639434]  # 3й_гор_750.ini  kp=2.1 ( pasp13i  вар 2)  условие прончости
    # Cdilat =    [-12.1599497,  3.16561436, 0.0489618]   # 3й_гор_750.ini  kp=2.1 ( pasp13i  вар 1)       дилат. соот.


    # shaft.sh = shaft.sh._replace(C=Cold, C1=Cold, C2=Cold,
    #                              ssgp=0.01)  # 2 - сфера; 1 - цилиндр.  alpha = 1 (старое решение)
    # для проверки
    # shaft.sh = shaft.sh._replace(theta_l=2.75)

    sh_old = shaft.sh  # для краткости.
    #                               дилат.соот.    усл. прочн.
    sh_new = sh_old._replace(C=Cnew, C1=Cnew, C2=Cnew, ssgp=0.01)  # 2 - сфера; 1 - цилиндр.  alpha = 1 (старое решение)

    print(shaft.to_text())  # параметры
    Omega = [0]
    N, Nz = 3000, 5  # число точек на радиусе, число шагов по Rz
    if not sh_new:
        print("Error opening %s" % filename)
        sys.exit()
    # print_annotation(filename,sh,N)
    show_plots = False
    P_lining = 100  # kPa - отпор крепи. рисуется на графиках
    Cases = [
        # [sh_new, (1.4 * sh_old.Ra, 2.7 * sh_old.Ra), N, Nz, Omega, el_pl_df, eFDM_sph, pFDM21_ln_spher_a, func_2d_quad, 'RP.log',
        #  'лог. деформ.'],
        [sh_new, (1.4 * sh_old.Ra, 2.7 * sh_old.Ra), N, Nz, Omega, el_pl_df_ll, eFDM_sph, pFDM21_ln_spher_a, func_2d_quad,
         'RP.log.', 'лог. деформ.'],
        # [sh_new, (1.4 * sh_old.Ra, 2.7 * sh_old.Ra), N, Nz, Omega, el_pl_df, eFDM,     pFDM2, func_2d_quad,
        #  'RP.koshi', 'деформ. Коши'],

        # [sh_old, (1.1 * sh_old.Ra, 3 * sh_old.Ra), N, Nz, Omega, el_pl2, eFDM,     pFDM2, func_2d_quad, 'RP.koshi',
        #  'деформ. Коши'],

        # [sh_new, (1.1 * sh_old.Ra, 3 * sh_old.Ra), N, Nz, Omega, el_pl2, eFDM,     pFDM2, func_2d_quad, 'RP.koshi2',
        #  'деформ. Коши. нов'],
    ]
    compare_RP_general_ll(Cases, P_lining)