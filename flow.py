"""
Для платического течения
"""

from geom import *
from geom_aux import *


def main_RP_flow(sh, N, Omega):
    """
    Пластическое течение
    Построение кривых горного давления
    :param sh:
    :param N: Число точек для шага по R*
    :param NNz: Число точек для шага по R*
    :param Rz_Rz:
    :param Omega:
    :return:
    """
    global result_dir, data_file, shaft_name, show_plots
    print("""Задача определения горного давления.
    Нелинейное условие прочности.
    нелинейная функция дилатансионных состояний.
    Пластическое течение.""")

    MaxUa = 1000  # максимальное значение перемещения при котором завершаются вычисления. мм

    dr = (sh.Rb - sh.Ra)/N  # шаг вдоль r и r*
    # RZ = [Rz_Rz[0] + drz*i for i in range(NNz)]
    # print("""Диапазон изменения Rz: %1.2f - %1.2f; шаг drz: %1.4f ra (%0.2f мм); число точек: %i""" %
    #       (RZ[0]/sh.Ra, RZ[-1]/sh.Ra, drz/sh.Ra, drz, NNz) )
    omega = pi/2
    DD = []  # details [ed + pd]
    RP,RK = [],[]
    P, Pob, Ua, SSt_a, EEra, EEta, SSoa = [],[],[],[],[],[],[]
    Uz, SSrz, SStz, EErz, EEtz = [],[],[],[],[]
    rp,rk = 0,0
    i = 0
    print("Вычисление...")
    RPdata = []
    Rp_, Rk_ = 0,0  # с каких значений начинаються rp, rk. Ед. Ra
    ssr_positive = False

    Rz = sh.Ra

    # Rz = sh.Ra + dr # чтобы пропустить область неединственного решения
    print ("Начальное значение r* = {:7.2f} ед. Ra".format(Rz/sh.Ra))

    RZ = []
    ed, res = el(sh, sh.Ra, N, eFDM, func_2d_quad)  # Ra - начало,  N - число точек в упр. зоне
    pd = ed
    _pd = Detail([sh.Ra],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],1,1)
    _pd.SSr[0] = ed.SSr[0]
    _pd.EEt[0] = ed.EEt[0]
    _pd.TT[0] = ed.TT[0]
    print("r * = ", end = "")
    while (True):
        Rz = Rz + dr
        RZ += [Rz]

        Nz = calc_Nz(sh,Rz,N)  # число точек в запредельной области (считаю точку на r*)
        Ne = N - Nz
        ed, res = el(sh, Rz, Ne, eFDM, func_2d_quad)
        # pd, rk, rp = pl_flow(sh, Rz, Nz, dr, ed, _pd, omega) # <- передавать запредельную зону для предыдущего r*
        pd, rk, rp = pFDM2_flow(sh, ed, Rz, Nz, omega) # <- передавать запредельную зону для предыдущего r*

        D = pd_plus_ed(pd, ed)
        if rp != sh.Ra and Rp_ == 0:  Rp_ = rp
        if rk != sh.Ra and Rk_ == 0:  Rk_ = rk
        RP += [rp/sh.Ra]; RK += [rk/sh.Ra]
        Uz += [D.U[Nz]]; SSrz += [D.SSr[Nz]]; SStz += [D.SSt[Nz]]; EErz += [D.EEr[Nz]]; EEtz += [D.EEt[Nz]];
        P += [-D.SSr[0]*1000]; SSt_a += [D.SSt[0]]
        Ua += [-D.U[0]]
        Pob += [(sh.kd * sh.gamma * (Rz-sh.Ra))/sh.ksr * 1000]
        i+=1
        _pd = pd
        if D.SSr[0] > 0:
            print(" Радиальные напряжения на контуре выработки положительны")
            break
        if Ua[-1] > MaxUa:
            print (" Перемещения контура выработки достигли предельного значения: {0:7.2f} > {1:7.2f}".format(Ua[-1],MaxUa) )
            break
        if Rz % sh.Ra <= dr: print("%i|" % (Rz/sh.Ra), end="", flush=True)

        if P[-1] < -10 or Nz + 1 == N: break
    print("\nПредельно разрушенная зона образуеться:  r* = %1.2f; u = %3.2f" % (Rp_/sh.Ra, get_by(RZ, Ua, Rp_)))
    print("Предельно разрыхленная зона образуеться: r* = %1.2f; u = %3.2f" % (Rk_/sh.Ra, get_by(RZ, Ua, Rk_)))
    Ux, PxPob = findPxP(P,Pob,Ua)
    print("Кривые горного давления и давления в случае обрушения пород пересекаються:\n %5.2f КПа; %2.2f мм" % (PxPob*1000, Ux))
    print()
    RPdata += [(RZ,RP,RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega)]
    print_result_RP(RPdata[0], sh, extra_points=[(Rp_/sh.Ra, "rp"), (Rk_/sh.Ra,"rk")])
    save_result_RP (RPdata, sh, result_dir, data_filename=data_file + ".csv", show=show_plots)


# для пластического течения
def main_flow_vs():
    global data_file, result_dir, shaft_name
    result_dir = 'result/koshi.vs.log'
    print("""\nЗадача с пластическим течением:
Данные сохраняються в каталог %s\n"""%result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.'+shaft_name
    sh = load_params(filename, nobur=False)
    C=[sh.SSm_c, sh.beta, 0]
    # C = [5.96, 6.649, -0.216]  # Квершлаг 1, гор 750
    sh = sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # С1, С2 - наборы констант
    # sh = sh._replace(C=C, C1=C, C2=C)
    Omega = [0]
    N, Nz = 300 , 300  # число точек на радиусе, число шагов по Rz
    if not sh: print("Eror opening %s"%filename); sys.exit()
    print_annotation(filename,sh,N)
    print("\n   Горное давление. Лин задача.\n")
    data_file_ = data_file
    data_file = data_file_ + ".ln"
    main_RP_ex(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=Omega, elpl=el_pl2, NLP_func=func_2d_quad,
               el_system=eFDM, pl_system=pFDM2)
    main_RP_flow(sh, N, Omega)
    print("\nГорное давление. Платическое течение.\n")
    data_file = data_file_ + ".koshi"
    # main_RP_ex(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=Omega, elpl=el_pl2, NLP_func=func_2d_quad,
    #            el_system=eFDM, pl_system=pFDM2)
    # main_RPll(sh, N, Nz, Rz_Rz = (1*sh.Ra, 3*sh.Ra), Omega=Omega) # Rz-Rz диапазон изменения Rz в здаче определения горного давления