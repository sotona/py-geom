# сферическая и цилиндрическая задача упругости (упругая зона и задача)
# для конференции на каф. СМиМ осень 2018

import seaborn
import pandas  # для DataFrame
import numpy
import matplotlib.pyplot as plt



from geom import *
from geom_aux import *
from calc import *
from report import *
from sst_ttm import *


# Эта функция потом должна стать основой для более общей. Надо бы тут те классы заюзать
def compare_general(Cases, show=1, save=1):
    """ Сравнивает несколько задач (cases) упругости
    :param cases: [ (sh, N, Rz,  Omega, el_sys, func, name, label), (,,,,,,)]  func - целевая функция
    name - краткое обозначение задачи. далее используется в DataFrame
    label  используется дял подписи графиков
    :param show:
    :param save:
    :return:
    """
    global prog_dir, shaft_name
    print("""
       Сравнение ... 
       """)

    # DataFrame отлично подходит для обработки данных
    # task - краткое обозначение рещаемой задачи
    DF = pandas.DataFrame(
        columns=['task', 'Rz', 'r[r_a]', 'r[mm]', 'Er[MPa]', 'SScm[MPa]', 'u[mm]', 'SSr[MPa]', 'SSt[MPa]',
                 'EEr', 'EEt', 'TT', 'SSo[MPa]'])

    # task - условное обозначений решаемой задачи (например цилиндр или сфера);
    # здесь (уже нет) используется Г - геометр. нелинейность; ГФ - геом. и физ. нелинейность

    for case in Cases:
        sh, N, Rz, omega, el_sys, func, task, label = case
        Xrz = calc_at_rz(sh, sh.Ra)
        x0 = [Xrz[3], Xrz[1]]

        Nz = calc_Nz(sh, Rz, N)
        Ne = N - Nz

        res_ln = minimize(func, x0, args=(sh, Rz, Ne, el_sys))
        data = el_sys(res_ln.x, sh, Rz, Ne)

        # print("Rz = %0.2f Ra (%0.0f мм)" % (Rz / sh.Ra, Rz))
        # print("Omega = %3.0f °" % (omega * 180 / pi))
        # print("Omega = %3.0f °" % (omega * 180 / pi))

        # print and show result
        # print(
        #     "\nРассматриваются: Ф - физическая нелинейность,\nГ - геометрическая нелинейность,\nГФ - геометрическая и физическая ")
        # print("\nУравнения: Лог | Коши     | delta")

        # print ("Целев. функция: %e | %e | %e" % (res1.fun, res_ln.fun, abs(res1.fun-res_ln.fun)) )
        # print(f"Целев. функция: {res_ln.fun:.2e} | {res_koshi.fun:.2e} | {abs(res_ln.fun - res_koshi.fun):.2e}")

        ssr_ln = data.SSr[:-1]
        sst_ln = data.SSt[:-1]
        u_ln = data.U[:-1]
        r_ln = [r / sh.Ra for r in data.r[:-1]]

        r_ln = [r / sh.Ra for r in data.r[:-1]]

        # check_result(sh, data)
        # check_result(sh, d_koshi)

        'u[mm]', 'SSr[MPa]', 'SSt[MPa]',
        'EEr', 'EEt', 'TT', 'SSo [MPa]'

        n = len(data.r[:-1])
        df = pandas.DataFrame(columns=DF.columns)
        df['task'] = [task] * n
        df['Rz'] = [Rz]  * n
        df['r[r_a]'] = list(np.array(data.r[:-1]) / sh.Ra)
        df['r[mm]'] = data.r[:-1]
        df['Er[MPa]'] = data.Er[:-1]
        df['SScm[MPa]'] = data.SScm[:-1]
        df['u[mm]'] = u_ln
        df['SSr[MPa]'] = ssr_ln
        df['SSt[MPa]'] = sst_ln
        df['EEr'] = data.EEr[:-1]
        df['EEt'] = data.EEt[:-1]
        df['TT'] = data.TT[:-1]
        df['SSo[MPa]'] = data.SSo[:-1]

        DF = DF.append(df)
    # compare_d_ex(sh, Rz, data, d_koshi)
    # compare_rz(sh, df)

    os.chdir(result_dir)
    fig = plt.figure()
    #
    # leg = plt.legend(fontsize=10)

    sh0 = Cases[0][0]

    prepare_plot(x_lim=[0, 7], xlabel="r, [$r_a$]", ylabel="Напряжение [МПа]",
                 plot_title="радиальные и тангенциальные напряжения $\sigma_r, \sigma_{\\theta}$",
                 window_title="'Напряжения. %s'%shaft_name", vline=sh0.S, rz=Rz / sh0.Ra)
    i = 0
    LineStyles = ['-', '--']
    for *_ ,task, label in Cases:

        data = DF[ DF['task'] == task]
        plt.plot(data['r[r_a]'], np.abs(data['SSr[MPa]']), LineStyles[i%len(LineStyles)],label="радиальные напряжения. "+label)
        plt.plot(data['r[r_a]'], np.abs(data['SSt[MPa]']), LineStyles[i%len(LineStyles)],label="тангенциальные напряжения. " + label)
        i += 1
    leg = plt.legend(loc='best', fontsize=10)
    if save: savefig('ssr,sst.%s.png' % shaft_name)
    if show: plt.show()

    fig = plt.figure()
    fig.canvas.set_window_title('Перемещения. %s' % shaft_name)
    plt.title("радиальные перемещения", fontsize=16)
    plt.xlim([0, 7])
    for *_ ,task, label in Cases:
        data = DF[ DF['task'] == task]
        plt.plot(data['r[r_a]'], np.abs(data['u[mm]']),  label=label)
    # plt.plot(r_koshi, np.abs(u_koshi), label="коши. С. сфера")
    # plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]");
    plt.ylabel("Перемещения [мм]")
    plt.grid(True);
    plt.legend(loc='best')
    if save: savefig('u.%s.png' % shaft_name, dpi=DPI)
    if show:
        plt.show()
    #
    # plt.title("Относительное объёмное разрыхление $\Delta\Theta$", fontsize=16)
    # plt.xlim([0.75, RZ[-1] / sh.Ra + 0.5])
    # plt.plot(r_ln, df[(df.task == 'лог. С') & (df.Rz == RZ[-1])]['TT'], '--', label="лог. С. сфера")
    # plt.plot(r_koshi, df[(df.task == 'коши. С') & (df.Rz == RZ[-1])]['TT'], label="коши. С. сфера")
    # plt.axhline(0.12, ls="dashed", color="gray", linewidth=1)
    # plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    # plt.xlabel("r, [$r_a$]");
    # plt.ylabel("$\Delta\Theta$")
    # plt.grid(True);
    # plt.legend(loc='best')
    # if save: savefig('TT.%s.png' % shaft_name, dpi=DPI)
    # if show > 1: plt.show()
    #
    # plt.title("Остаточная прочность $\sigma_o$", fontsize=16)
    # plt.xlim([0.75, RZ[-1] / sh.Ra + 0.5])
    # plt.plot(r_ln, df[(df.task == 'лог. С') & (df.Rz == RZ[-1])]['SSo[MPa]'], '--', label="лог. С. сфера")
    # # plt.plot(r_g,  DF[ (DF.task=='коши. С' ) & (DF.Rz == RZ[-1]) ]['SSo[MPa]'],       label="коши. С. сфера")
    # plt.axhline(sh.ssgp * sh.SSm_c, ls="dashed", color="gray", linewidth=1)
    # plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    # plt.xlabel("r, [$r_a$]");
    # plt.ylabel("$\sigma_o, [MPa]$")
    # plt.grid(True);
    # plt.legend(loc='best')
    # if save: savefig('SSo.%s.png' % shaft_name, dpi=DPI)
    # if show > 1: plt.show()
    #
    # plt.title("Модуль упругости $E(r)$", fontsize=16)
    # plt.xlim([0.75, 7])
    # plt.plot(r_ln, df[(df.task == 'лог. С') & (df.Rz == RZ[-1])]['Er[MPa]'], '--', label="лог. С. сфера")
    # plt.plot(r_koshi, df[(df.task == 'коши. С') & (df.Rz == RZ[-1])]['Er[MPa]'], label="коши. С. сфера")
    # plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    # plt.xlabel("r, [$r_a$]");
    # plt.ylabel("$E(r), [MPa]$")
    # plt.grid(True);
    # plt.legend(loc='best')
    # if save: savefig('Er.%s.png' % shaft_name, dpi=DPI)
    # if show > 1: plt.show()
    #
    file = f"{shaft_name}.cvs"
    DF.to_csv("DF." + file, sep='\t', float_format='%.5f')
    # DF.to_html("DF." + file1+".html", classes=["table-bordered", "t    able-striped", "table-hover"])
    os.chdir(prog_dir)
    print(f"Данные сохранены в   {result_dir}")
    print(f"Файл: {file}")


# сфера и цилиндр для задач упругости
def compare_spher_cyl(sh, N, RZ, omega=pi / 2, show=1, save=1):
    """
    Сравнить логарифмические и линейные деформациии.
    Сохраняет данные в csv и в картинки
    :param sh:
    :param N:
    :param RZ:
    :param omega:
    :param show:
    :param save:
    """
    global prog_dir, shaft_name
    print("""
    Сравнение ... 
    """)

    # DataFrame отлично подходит для обработки данных
    DF = pandas.DataFrame(
        columns=['task', 'Rz', 'r[r_a]', 'r[mm]', 'Er[MPa]', 'SScm[MPa]', 'u[mm]', 'SSr[MPa]', 'SSt[MPa]',
                 'EEr', 'EEt', 'TT', 'SSo[MPa]'])

    # task - условное обозначений решаемой задачи (например цилиндр или сфера);
    # здесь (уже нет) используется Г - геометр. нелинейность; ГФ - геом. и физ. нелинейность


    for Rz in RZ:
        df = pandas.DataFrame( columns=DF.columns )
        Xrz = calc_at_rz(sh, sh.Ra)
        x0 = [Xrz[3], Xrz[1]]

        Nz = calc_Nz(sh, Rz, N)
        Ne = N - Nz


        # сфера
        sh = sh._replace(K = 2)
        # логарифмические деформации
        # с геометрической и физической нелинейностью. константы C должны быть заданы
        res_ln = minimize(func_2d_quad, x0, args=(sh, Rz, Ne, eFDM_sph))
        # sh._replace(C=C, C1=C, C2=C)
        ed_ln = eFDM_sph(res_ln.x, sh, Rz, Ne)
        # pd_gf, rk, rp = pFDM2_ln_spher(sh, ed_gf, Rz, Nz, omega)

        #  деормации коши
        res_koshi = minimize(func_2d_quad, x0, args=(sh, Rz, Ne, eFDM_sph_koshi))
        ed_koshi = eFDM_sph_koshi(res_koshi.x, sh, Rz, Ne)

        d_ln = ed_ln # d_e pd_plus_ed(pd_gf, ed_gf)
        d_koshi =  ed_koshi  # pd_plus_ed(pd_g, ed_g)


        print("Rz = %0.2f Ra (%0.0f мм)" % (Rz/sh.Ra, Rz))
        print("Omega = %3.0f °" % (omega * 180 / pi))
        print("Omega = %3.0f °" % (omega * 180 / pi))

        # print and show result
        print("\nРассматриваются: Ф - физическая нелинейность,\nГ - геометрическая нелинейность,\nГФ - геометрическая и физическая ")
        print ("\nУравнения: Лог | Коши     | delta")

        # print ("Целев. функция: %e | %e | %e" % (res1.fun, res_ln.fun, abs(res1.fun-res_ln.fun)) )
        print(f"Целев. функция: {res_ln.fun:.2e} | {res_koshi.fun:.2e} | {abs(res_ln.fun - res_koshi.fun):.2e}")
        compare_d_ex(sh,Rz, d_ln, d_koshi)

        ssr_ln, ssr_koshi = d_ln.SSr[:-1], d_koshi.SSr[:-1]
        sst_ln, sst_koshi = d_ln.SSt[:-1], d_koshi.SSt[:-1]
        u_ln, u_koshi = d_ln.U[:-1], d_koshi.U[:-1]
        r_ln, r_koshi = [r/sh.Ra for r in d_ln.r[:-1]], [r/sh.Ra for r in d_koshi.r[:-1]]

        r_ln, r_koshi = [r / sh.Ra for r in d_ln.r[:-1]], [r / sh.Ra for r in d_koshi.r[:-1]]

        check_result(sh, d_ln)
        check_result(sh, d_koshi)

        'u[mm]', 'SSr[MPa]', 'SSt[MPa]',
        'EEr', 'EEt', 'TT', 'SSo [MPa]'

        n = len(d_ln.r[:-1])
        df['task'] = ['лог. С'] * n + ['коши. С'] * n
        df['Rz']        = [Rz] * 2*n
        df['r[r_a]']    = list(np.array(d_ln.r[:-1])/sh.Ra) + list(np.array(d_koshi.r[:-1])/sh.Ra)
        df['r[mm]']     = d_ln.r[:-1] + d_koshi.r[:-1]
        df['Er[MPa]']   = d_ln.Er[:-1] + d_koshi.Er[:-1]
        df['SScm[MPa]'] = d_ln.SScm[:-1] + d_koshi.SScm[:-1]
        df['u[mm]']     = u_ln + u_koshi
        df['SSr[MPa]']  = ssr_ln + ssr_koshi
        df['SSt[MPa]']  = sst_ln + sst_koshi
        df['EEr']       = d_ln.EEr[:-1] + d_koshi.EEr[:-1]
        df['EEt']       = d_ln.EEt[:-1] + d_koshi.EEt[:-1]
        df['TT']        = d_ln.TT[:-1]  + d_koshi.TT[:-1]
        df['SSo[MPa]']  = d_ln.SSo[:-1] + d_koshi.SSo[:-1]

        DF = DF.append(df)

    # compare_rz(sh, df)

    os.chdir(result_dir)
    fig = plt.figure()

    # leg = plt.legend(fontsize=10)
    prepare_plot(x_lim=[0,7], xlabel= "r, [$r_a$]", ylabel="Напряжение [МПа]",
                 plot_title="радиальные и тангенциальные напряжения $\sigma_r, \sigma_{\\theta}$",
                 window_title="'Напряжения. %s'%shaft_name", vline=sh.S, rz=Rz/sh.Ra)
    plt.plot(r_ln,     np.abs(ssr_ln), '--', label="радиальные напряжения. лог. дефеормации")
    plt.plot(r_ln,     np.abs(sst_ln), '--', label="тангенциальные напряжения. лог. дефеормации")
    plt.plot(r_koshi,  np.abs(ssr_koshi),    label="радиальные напряжения. деформации Коши")
    plt.plot(r_koshi,  np.abs(sst_koshi),    label="тангенциальные напряжения. деформации Коши")
    leg = plt.legend(loc='best', fontsize=10)
    if save: savefig('ssr,sst.%s.png' % shaft_name)
    if show: plt.show()


    fig = plt.figure()
    fig.canvas.set_window_title('Перемещения. %s'%shaft_name)

    plt.title("радиальные перемещения", fontsize=16)
    plt.xlim([0,7])
    plt.plot(r_ln,     np.abs(u_ln), '--', label = "лог. дефеормации")
    plt.plot(r_koshi,  np.abs(u_koshi),        label = "деформации Коши")
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]"); plt.ylabel("Перемещения [мм]")
    plt.grid(True); plt.legend(loc='best')
    if save: savefig('u.%s.png'%shaft_name, dpi=DPI)
    if show:
        plt.show()


    plt.title("Относительное объёмное разрыхление $\Delta\Theta$", fontsize=16)
    plt.xlim([0.75, RZ[-1] / sh.Ra + 0.5])
    plt.plot(r_ln, df[ (df.task=='лог. С') & (df.Rz == RZ[-1]) ]['TT'], '--', label="лог. С. сфера")
    plt.plot(r_koshi,  df[ (df.task=='коши. С' ) & (df.Rz == RZ[-1]) ]['TT'],        label="коши. С. сфера")
    plt.axhline(0.12, ls="dashed", color="gray", linewidth=1)
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]");
    plt.ylabel("$\Delta\Theta$")
    plt.grid(True);
    plt.legend(loc='best')
    if save: savefig('TT.%s.png' % shaft_name, dpi=DPI)
    if show > 1: plt.show()


    plt.title("Остаточная прочность $\sigma_o$", fontsize=16)
    plt.xlim([0.75, RZ[-1] / sh.Ra + 0.5])
    plt.plot(r_ln, df[ (df.task=='лог. С') & (df.Rz == RZ[-1]) ]['SSo[MPa]'], '--', label="лог. С. сфера")
    # plt.plot(r_g,  DF[ (DF.task=='коши. С' ) & (DF.Rz == RZ[-1]) ]['SSo[MPa]'],       label="коши. С. сфера")
    plt.axhline(sh.ssgp*sh.SSm_c, ls="dashed", color="gray", linewidth=1)
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]");
    plt.ylabel("$\sigma_o, [MPa]$")
    plt.grid(True);
    plt.legend(loc='best')
    if save: savefig('SSo.%s.png' % shaft_name, dpi=DPI)
    if show > 1: plt.show()


    plt.title("Модуль упругости $E(r)$", fontsize=16)
    plt.xlim([0.75, 7])
    plt.plot(r_ln, df[ (df.task=='лог. С') & (df.Rz == RZ[-1]) ]['Er[MPa]'], '--', label="лог. С. сфера")
    plt.plot(r_koshi,  df[ (df.task=='коши. С' ) & (df.Rz == RZ[-1]) ]['Er[MPa]'],       label="коши. С. сфера")
    plt.axvline(Rz / sh.Ra, ls="dashed", color="gray", linewidth=1)
    plt.xlabel("r, [$r_a$]");
    plt.ylabel("$E(r), [MPa]$")
    plt.grid(True);
    plt.legend(loc='best')
    if save: savefig('Er.%s.png' % shaft_name, dpi=DPI)
    if show > 1: plt.show()


    file = f"{shaft_name}.cvs"
    DF.to_csv("DF." + file, sep='\t', float_format = '%.5f')
    # DF.to_html("DF." + file1+".html", classes=["table-bordered", "t    able-striped", "table-hover"])
    os.chdir(prog_dir)
    print(f"Данные сохранены в   {result_dir}")
    print(f"Файл: {file}")





# сферические деформации
# кажется тут нужно заюзать те классы
def main_spher():
    global data_file, result_dir, shaft_name
    result_dir = 'result/sph_vs_cyl.fall2018'
    print("""\nСравнение деформаций Коши и логарифмических деформаций на примере:
1. задачи с запредельной зоной
2. задачай определения горного давления
Данные сохраняються в каталог %s\n"""%result_dir)
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.'+shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)

    # C=[sh.SSm_c, sh.beta, 0]
    C = [5.96, 6.649, -0.216]  # Квершлаг 1, гор 750
    # C1 - c
    # C2 - c'
    shaft.sh = shaft.sh._replace(C=C, C1=C, C2=C, ssgp=0.01)  # С1, С2 - наборы констант
    # 2 - сфера; 1 - цилиндр.
    # alpha = 1 (старое решение)
    shaft.sh = shaft.sh._replace(alpha = 0.5, K = 2)
    shaft.sh = shaft.sh._replace(S = 24)

    sh = shaft.sh  # для краткости.

    print( shaft.to_text() )  # параметры
    # sh = sh._replace(C=C, C1=C, C2=C)
    Omega = [0]
    N, Nz = 3000 , 300  # число точек на радиусе, число шагов по Rz
    if not sh: print("Eror opening %s"%filename); sys.exit()
    # print_annotation(filename,sh,N)
    print("\n   1. НДС массива\n")
    # RZ = [1*sh.Ra, 1.1*sh.Ra, 1.2*sh.Ra, 1.3*sh.Ra,  1.4*sh.Ra, 1.5*sh.Ra, 1.8*sh.Ra, 2.5*sh.Ra, 3*sh.Ra, 4*sh.Ra]
    # RZ = [1*sh.Ra, 1.1*sh.Ra, 1.2*sh.Ra, 1.3*sh.Ra,  1.4*sh.Ra]
    # RZ = [4.0*sh.Ra]
    RZ = [ 3.0*sh.Ra ]
    show_plots = False

    sh2 = sh._replace(K=1)

    compare_spher_cyl(sh, N, RZ, Omega[0], show = 1)
    # Cases = [ (sh,  N, 3.0*sh.Ra, Omega[0], eFDM_sph, func_2d_quad, 'sph_ln', 'сфера. лог'),
    #           (sh2, N, 3.0*sh.Ra, Omega[0], eFDM_sph, func_2d_quad, 'cyl_ln', 'цилиндр. лог'),
    #           (sh,  N,  3.0*sh.Ra,Omega[0], eFDM_sph_koshi, func_2d_quad, 'sph_k', 'сфера. Коши'),
    #           (sh2, N, 3.0*sh.Ra, Omega[0], eFDM_sph_koshi,   func_2d_quad, 'cyl_k', 'цилиндр. Коши'),
    #           ]
    #
    # compare_general( Cases )
    # print("\n2.   Горное давление при логарифмических деформациях.\n")
    # data_file_ = data_file
    # data_file = data_file_ + ".ln"

