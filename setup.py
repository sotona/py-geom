from setuptools import setup, find_packages

setup(
    name="pygeom",
    description='Geomechanics library',
    packages=find_packages(),
    install_requires=[
        "numpy",
        "matplotlib",
        "scipy",
    ],
    # ...
)

# как устанавливать зависимости?
# python setup.py develop
# error: no lapack/blas resources found
#
# python setup.py install
# ошибка

