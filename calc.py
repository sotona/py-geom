from geom import *
from geom_aux import *

class CalcParamsRP:
    N = 300
    dr = None
    r1, r2 = None, None
    nds_calc_param = None


    def __init__(self, r1, r2, nds_param, dr):
        self.r1 = r1
        self.r2 = r2
        # self.dr = (self.r2 - self.r1) / self.N
        self.dr = dr
        self.N = int((r2-r1)/self.dr)
        self.nds_calc_param = nds_param



class CalcParamsNDS:
    Ra = None
    Rb = None
    N = 300   # число точек вдоль радиуса
    RZ = []   # список радиусов, для которых будет решена задача определени НДС
    Omega = []
    dr = None # шаг вдоль радиуса

    def __init__(self, Ra, Rb, RZ, N = 300, omega=[pi/2]):
        self.RZ = RZ[::]
        self.Ra = Ra
        self.Rb = Rb
        self.N = N
        self.Omega = omega[::]
        self.dr = (self.Rb - Ra)/N


    def to_text(self):
        text = "Параметры расчёта" + "\n"
        text += "Число точек на радиусе N: %i; шаг dr: %0.2f мм" % (self.N, (self.Rb - self.Ra) / self.N) + "\n"
        return text



class Calculator:
    """ Класс, отвечающий за расчёты """
    el_system = None
    pl_system = None
    NLPfunc    = None

    def __init__(self, el_sys, pl_sys, nlp_func):
        self.el_system = el_sys
        self.pl_system = pl_sys
        self.NLPfunc = nlp_func



    def calc_nds(self, shaft, calc_params_nds):
        """ Задача определения НДС z для нескольких Rz
        :param shaft: ShaftParams
        :param calc_params_nds: CalcParams
        :return: DD
        """
        sh = shaft.sh
        N = calc_params_nds.N
        NLP_func = self.NLPfunc
        el_system = self.el_system
        pl_system = self.pl_system
        omega = calc_params_nds.Omega[0]


        # print("Решение задчаи с запредельной зоной" +
        #       "Упругая зона: \n   " + (
        #       "деформации Коши" if el_system == eFDM else "логарифмичесие деформации") + "\n " +
        #       ("Параболическое условие прочности." if NLP_func == func_2d_quad else "Линейное условие прочности."))
        # print("Данные получены для угла в {0} градусов ".format(omega / pi * 180) + (
        # "(кровля выработки)" if omega == pi / 2 else ""))
        DD = []  # details [ed + pd]
        RP, RK = [], []
        Res = []
        rp, rk = 0, 0
        for Rz in calc_params_nds.RZ:
            # ed, res, pd, rp, rk = el_pl_calc(sh, Rz, N, Omega=pi/2, NLP_func=NLP_func)
            if pl_system == None:
                Ne =N
            else :
                Nz = calc_Nz(sh, Rz, N)
                Ne = N - Nz
            Xrz = calc_at_rz(sh, sh.Ra)  # вычислить начальные значения для целевой функции
            x0 = [Xrz[3], Xrz[1]]
            res = minimize(NLP_func, x0, args=(sh, Rz, Ne, el_system))
            ed = el_system(res.x, sh, Rz, Ne)

            if pl_system ==None:
                DD+=[ed]
            else:
                pd, rk, rp = pl_system(sh, ed, Rz, Nz, omega=omega)
                D = pd_plus_ed(pd, ed)
                DD += [D]
            RP += [rp];
            RK += [rk]
            Res += [res]
        return DD, Res, RP, RK


    def calc_rp(self, shaft, calc_params_rp):
        print("""Задача определения горного давления.
           Линейное условие прочности.
           Линейная функция дилатансионных состояний.""")
        rz1, rz2 = calc_params_rp.r1, calc_params_rp.r2
        sh = shaft.sh
        N = calc_params_rp.N
        drz = calc_params_rp.dr
        RZ = [rz1 + drz * i for i in range(N)]
        print("""Диапазон изменения Rz: %1.2f - %1.2f; шаг drz: %1.4f ra (%0.2f мм); число точек: %i""" %
              (RZ[0] / sh.Ra, RZ[-1] / sh.Ra, drz / sh.Ra, drz, N))
        omega = calc_params_rp.nds_calc_param.Omega[0]
        DD = []  # details [ed + pd]
        RP, RK = [], []
        P, Pob, Ua, SSt_a, EEra, EEta, SSoa = [], [], [], [], [], [], []
        Uz, SSrz, SStz, EErz, EEtz = [], [], [], [], []
        rp, rk = 0, 0
        i = 0
        print("Вычисление...")
        RPdata = []
        Rp_, Rk_ = 0, 0  # с каких значений начинаються rp, rk. Ед. Ra
        ssr_positive = False
        for Rz in RZ:
            Nz = calc_Nz(sh, Rz, N)
            # ed, res, pd, rp, rk = elpl(sh, Rz, N, omega, NLP_func, el_system, pl_system)
            nds_calc_param = calc_params_rp.nds_calc_param
            nds_calc_param.RZ = [Rz]
            D, Res, Rp, Rk = self.calc_nds(shaft, nds_calc_param)
            D, rp, rk= D[0], Rp[0], Rk[0]
            # D = pd_plus_ed(pd, ed)
            if rp != sh.Ra and Rp_ == 0:  Rp_ = rp
            if rk != sh.Ra and Rk_ == 0:  Rk_ = rk
            RP += [rp / sh.Ra];
            RK += [rk / sh.Ra]
            Uz += [D.U[Nz]];
            SSrz += [D.SSr[Nz]];
            SStz += [D.SSt[Nz]];
            EErz += [D.EEr[Nz]];
            EEtz += [D.EEt[Nz]];
            P += [-D.SSr[0] * 1000];
            SSt_a += [D.SSt[0]]
            Ua += [-D.U[0]]
            Pob += [(sh.kd * sh.gamma * (Rz - sh.Ra)) / sh.ksr * 1000]
            i += 1
            if D.SSr[0] > 0:
                print(" Радиальные напряжения на контуре выработки положительны")
                break
            if i % (N / 10) == 0: print("%i|" % (i / N * 100), end="", flush=True)
            if P[-1] < -10: break
        print("\nПредельно разрушенная зона образуеться:  r* = %1.2f; u = %3.2f" % (Rp_ / sh.Ra, get_by(RZ, Ua, Rp_)))
        print("Предельно разрыхленная зона образуеться: r* = %1.2f; u = %3.2f" % (Rk_ / sh.Ra, get_by(RZ, Ua, Rk_)))
        Ux, PxPob = findPxP(P, Pob, Ua)
        print("Кривые горного давления и давления в случае обрушения пород пересекаються:\n %5.2f КПа; %2.2f мм" % (
        PxPob * 1000, Ux))
        print()
        RPdata += [(RZ, RP, RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega)]
        return RPdata
        # print_result_RP(RPdata[0], sh, extra_points=[(Rp_ / sh.Ra, "rp"), (Rk_ / sh.Ra, "rk")])
        # save_result_RP(RPdata, sh, result_dir, data_filename=data_file + ".csv", show=show_plots)


    def  calc_rp_flow(self, shaft, calc_params_rp):
        """
        Пластическое течение
        Построение кривых горного давления
        :param sh:
        :param N: Число точек для шага по R*
        :param NNz: Число точек для шага по R*
        :param Rz_Rz:
        :param Omega:
        :return:
        """
        # global result_dir, data_file, shaft_name, show_plots
        print("""Задача определения горного давления.
        Нелинейное условие прочности.
        нелинейная функция дилатансионных состояний.
        Пластическое течение.""")
        MaxUa = 1000  # максимальное значение перемещения при котором завершаются вычисления. мм

        sh = shaft.sh
        N = calc_params_rp.N
        dr = calc_params_rp.dr
        omega = calc_params_rp.nds_calc_param.Omega[0]
        print("drz = {:.4f}".format(dr))
        # RZ = [Rz_Rz[0] + drz*i for i in range(NNz)]
        # print("""Диапазон изменения Rz: %1.2f - %1.2f; шаг drz: %1.4f ra (%0.2f мм); число точек: %i""" %
        #       (RZ[0]/sh.Ra, RZ[-1]/sh.Ra, drz/sh.Ra, drz, NNz) )
        DD = []  # details [ed + pd]
        RP,RK = [],[]
        P, Pob, Ua, SSt_a, EEra, EEta, SSoa = [],[],[],[],[],[],[]
        Uz, SSrz, SStz, EErz, EEtz = [],[],[],[],[]
        rp,rk = 0,0
        i = 0
        print("Вычисление...")
        RPdata = []
        Rp_, Rk_ = 0,0  # с каких значений начинаються rp, rk. Ед. Ra
        ssr_positive = False

        Rz = sh.Ra

        # Rz = sh.Ra + dr # чтобы пропустить область неединственного решения
        print ("Начальное значение r* = {:7.2f} ед. Ra".format(Rz/sh.Ra))

        RZ = []
        ed, res = el(sh, sh.Ra, N, eFDM, func_2d_quad)  # Ra - начало,  N - число точек в упр. зоне
        pd = ed
        _pd = Detail([sh.Ra],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],1,1)
        _pd.SSr[0] = ed.SSr[0]
        _pd.EEt[0] = ed.EEt[0]
        _pd.TT[0] = ed.TT[0]
        print("r * = ", end = "")
        while (True):
            RZ += [Rz]

            Nz = calc_Nz(sh,Rz,N)  # число точек в запредельной области (считаю точку на r*)
            Ne = N - Nz
            ed, res = el(sh, Rz, Ne, eFDM, func_2d_quad)
            # pd, rk, rp = pl_flow(sh, Rz, Nz, dr, ed, _pd, omega) # <- передавать запредельную зону для предыдущего r*
            pd, rk, rp = pFDM2_flow(sh, ed, Rz, Nz, omega) # <- передавать запредельную зону для предыдущего r*

            D = pd_plus_ed(pd, ed)
            if rp != sh.Ra and Rp_ == 0:
                Rp_ = rp
            if rk != sh.Ra and Rk_ == 0:
                Rk_ = rk
            RP += [rp/sh.Ra]; RK += [rk/sh.Ra]
            Uz += [D.U[Nz]]; SSrz += [D.SSr[Nz]]; SStz += [D.SSt[Nz]]; EErz += [D.EEr[Nz]]; EEtz += [D.EEt[Nz]];
            P += [-D.SSr[0]*1000]; SSt_a += [D.SSt[0]]
            Ua += [-D.U[0]]
            Pob += [(sh.kd * sh.gamma * (Rz-sh.Ra))/sh.ksr * 1000]
            i+=1
            _pd = pd
            Rz = Rz + dr

            # Условия выхода
            if D.SSr[0] > 0:
                print(" Радиальные напряжения на контуре выработки положительны")
                break
            if Ua[-1] > MaxUa:
                print (" Перемещения контура выработки достигли предельного значения: {0:7.2f} > {1:7.2f}".format(Ua[-1],MaxUa) )
                break
            if Rz % sh.Ra <= dr:
                print("%i|" % (Rz/sh.Ra), end="", flush=True)

            if P[-1] < -10 or Nz + 1 == N:
                break
        RPdata += [(RZ, RP, RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega)]
        return RPdata

        # print("\nПредельно разрушенная зона образуеться:  r* = %1.2f; u = %3.2f" % (Rp_/sh.Ra, get_by(RZ, Ua, Rp_)))
        # print("Предельно разрыхленная зона образуеться: r* = %1.2f; u = %3.2f" % (Rk_/sh.Ra, get_by(RZ, Ua, Rk_)))
        # Ux, PxPob = findPxP(P,Pob,Ua)
        # print("Кривые горного давления и давления в случае обрушения пород пересекаються:\n %5.2f КПа; %2.2f мм" % (PxPob*1000, Ux))
        # print()
        # RPdata += [(RZ,RP,RK, P, Pob, Ua, SSt_a, EEra, EEta, SSoa, Uz, SSrz, SStz, EErz, EEtz, omega)]
        # print_result_RP(RPdata[0], sh, extra_points=[(Rp_/sh.Ra, "rp"), (Rk_/sh.Ra,"rk")])
        # save_result_RP (RPdata, sh, result_dir, data_filename=data_file + ".csv", show=show_plots)
