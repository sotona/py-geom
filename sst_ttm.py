# -*- coding: utf-8 -*-
__author__ = 'sotona'


import matplotlib.pyplot as pl
import numpy as np
from scipy.stats.stats import pearsonr
import os

from geom_aux import get_ssc_ttm_ex, ShaftParams, Pasp13i


def plot(ss3, ss1, ss3_2, ss1_2, ssc, ttm1, ttm2, p):
    # n=100
    # dx = (ss3[-1]-ss3[0])/n
    # x = [ss3[0]-n/20*dx + dx*i for i in range(n+40)]
    # y1 = [p(xi) for xi in x]
    # y2 = [p(xi) for xi in x]
    # pl.autoscale(True, tight=False)
    # pl.grid(True)
    # pl.title("ss1 = f(ss3)")
    # pl.xlabel('ss3 = $\sigma_c + \\tau_m$')
    # pl.ylabel('ss1')
    # pl.plot(x,y1,'b--', label='P(5)')
    # pl.plot(x,y2,'g--', label='P(1)')
    # pl.plot(ss3,ss1,'ro', label='exp. data')
    # pl.legend(loc='best')
    # pl.show()


    pl.autoscale(True)
    pl.grid(True)
    pl.title("$\\tau_m$ = f($\sigma_c$)")
    pl.xlabel('$\sigma_c$ [МПа]')
    pl.ylabel('$\\tau_m$ [МПа]')
    pl.plot(ssc, ttm1, 'ro-', label='exp. data ($\\tau_{m1}$)')
    pl.plot(ssc, ttm2, 'go-', label='exp. data ($\\tau_{m2}$)')
    pl.legend(loc='best')
    pl.show()



def calc_coefs_test():
    global data_file, result_dir, shaft_name, show_plots
    result_dir = 'result/sph_paper2019'
    if not os.path.exists(result_dir): os.makedirs(result_dir)
    filename = "../asp/bin/params/" + ["3й_гор_750.ini", "Квершлаг_№1_гор_750.ini"][1]
    shaft_name = filename.split('/')[-1]
    data_file = 'data.' + shaft_name

    shaft = ShaftParams(filename)
    shaft.sh = shaft.load_params(nobur=False)
    shaft.sh = shaft.sh._replace(alpha=0.5, K=1)

    sh = shaft.sh

    SSc = [ sh.SSl_c*sh.ko*sh.ke/2, sh.S/sh.kp, (sh.SSl_c*sh.ko*sh.ke/2 + sh.S/sh.kp)/2]
    # TTm = [-sh.SSl_c*sh.ko*sh.ke/2 ]

    coefs2, coefs2 = calc_coeffs3(sh, SSc, kp = [2, 2, 2])
    print(coefs2, coefs2)


def calc_coeffs3_old(p, ssc, prog=Pasp13i):
    """
    Вычисляет коэффициенты полинома третьей степени аппроксимирующего огибаюшию кругов Мора
    P - params
    sigma_c = [list of sigma_c]
    :return:
     coefs1 - коэффициенты полинома для ttm1 (для описания разрыхления)
     coefs2 - для описания разупрочнения
     e1,e2 - ошибки работы МНК
    """
    ko_ = p.ko * p.ke
    # ko_ = 1
    ttm1, ttm2 = [],[]
    for c in ssc:
        s, t1, t2, error = get_ssc_ttm_ex(p.SSl_t, p.SSl_c, ko=ko_, kp=1, S=c, prog=Pasp13i, verbose=False)
        if error:
            print("Ошибка при вычислении напряжений в pasp13i")
            s, t1, t2, error = get_ssc_ttm_ex(p.SSl_t, p.SSl_c, ko=ko_, kp=1, S=c, prog=Pasp13i, verbose=True)
            # return None, None, None, None
        ttm1.append(t1)
        ttm2.append(t2)

    # !!!!!!!
    # ttm1 = [2.981,  6.022, 8.963]
    # ttm2 = [2.981,  6.288, 9.501]

    ss1 = [s + t for s, t in zip(ssc, ttm1)]
    ss3 = [s - t for s, t in zip(ssc, ttm1)]
    ss1_2 = [s + t for s, t in zip(ssc, ttm2)]
    ss3_2 = [s - t for s, t in zip(ssc, ttm2)]


    # error2 = [abs(pl1(ss3[-1])-ss1[-1])]

    c2 = np.polyfit(ss3,ss1,2)
    c2_2 = np.polyfit(ss3_2,ss1_2,2)
    p = np.poly1d(c2); e1 = abs(p(ss3[-1]) - ss1[-1])
    p = np.poly1d(c2_2); e2 = abs(p(ss3_2[-1]) - ss1_2[-1])
    coefs1, coefs2 = c2.tolist(),c2_2.tolist()
    return coefs1, coefs2, e1, e2



def calc_coeffs3(sh, ssc_, kp, prog=Pasp13i):
    """
    Вычисляет коэффициенты полинома третьей степени аппроксимирующего огибаюшию кругов Мора.

    В sigma_c задаётся предел прочности на сжатие именно массива!

    sh - Shaft
    sigma_c = [list of sigma_c] три значения (-SSc_m/2, -S, среднее арифметическое медлу первыми двумя)
    :type kp: список коэффициентов перегрузки
    :param sh:
    :param ssc:
    :param prog:
    :return:
    :return:
            coefs1 - коэффициенты полинома для ttm1 (для описания разрыхления)
            coefs2 - для описания разупрочнения
            e1,e2 - ошибки работы МНК
    """
    ko_ = sh.ko * sh.ke
    # ko_ = 1
    ttm1, ttm2 = [-ssc_[0]],[-ssc_[0]]
    ssc = [ssc_[0]]
    for c, k in zip(ssc_[1:], kp[1:]):
        print(f"SSc = {c:2.4f}")
        s, t1, t2, error = get_ssc_ttm_ex(sh.SSl_t, sh.SSl_c, ko=ko_, kp=k, S=c, prog=Pasp13i, verbose=False)
        if error:
            print("Ошибка при вычислении напряжений в pasp13i")
            s, t1, t2, error = get_ssc_ttm_ex(sh.SSl_t, sh.SSl_c, ko=ko_, kp=k, S=c, prog=Pasp13i, verbose=True)
            # return None, None, None, None
        ttm1.append(t1)
        ttm2.append(t2)
        ssc.append(s)

    ss1 = [s + t for s, t in zip(ssc, ttm1)]
    ss3 = [s - t for s, t in zip(ssc, ttm1)]
    ss1_2 = [s + t for s, t in zip(ssc, ttm2)]
    ss3_2 = [s - t for s, t in zip(ssc, ttm2)]

    # error2 = [abs(pl1(ss3[-1])-ss1[-1])]

    # c2 = np.polyfit(ss3,ss1,2)
    # c2_2 = np.polyfit(ss3_2,ss1_2,2)
    # p = np.poly1d(c2); e1 = abs(p(ss3[-1]) - ss1[-1])
    # p = np.poly1d(c2_2); e2 = abs(p(ss3_2[-1]) - ss1_2[-1])
    # coefs1, coefs2 = c2.tolist(),c2_2.tolist()

    A = np.matrix([[1, ss1[0], ss1[0] ** 2],
                   [1, ss1[1], ss1[1] ** 2],
                   [1, ss1[2], ss1[2] ** 2]])

    B = np.matrix([[ss3[0]], [ss3[1]], [ss3[2]]])

    C = np.linalg.solve(A, B)


    return [C[0,0], C[1,0], C[2,0]], [C[0,0], C[1,0], C[2,0]]


def main_5points():
    #Камера ЦПП
    ko = 0.11  # коэф. структурного ослабления
    kd = 0.85  # коэф. длительной прочности
    kp = 2.1  # коэф. перегрузки
    ss_c = 120  #67.4  # предел прочности на сжатие [МПа]
    ss_t = 6.3  # предел прочности на растяжение [МПа]
    sigma_v = 16.3  # вертикальное напряжение [МПа]

    min_error = 1/1000  # МПа

    ss_cm = ss_c*kd*ko
    sigma_v1 = 1.5 * ss_cm/2
    sigma_vz = kp * sigma_v if sigma_v > sigma_v1 else sigma_v1
    d = (sigma_vz - ss_cm/2)/5
    ssc = [ss_cm/2 + d*i for i in range(1,6)]

    ttm1, ttm2 = [], []
    for c in ssc:
        s, t1, t2, error = get_ssc_ttm(ss_t,ss_c,ko,kp,c,prog=Pasp13i)
        ttm1.append(t1)
        ttm2.append(t2)
    # ttm = [4.789, 6.481, 8.125, 9.756, 11.374]
    ss1 = [s + t for s, t in zip(ssc, ttm1)]
    ss3 = [s - t for s, t in zip(ssc, ttm1)]
    ss1_2 = [s + t for s, t in zip(ssc, ttm2)]
    ss3_2 = [s - t for s, t in zip(ssc, ttm2)]

    print("Предел прочности на сжатие = %2.4f МПа"%ss_cm)
    print("sigma_v  = %2.4f * %0.2f = %2.4f МПа" % (sigma_v, kp, sigma_v*kp))
    print("sigma_v1 = %2.4f МПа" % sigma_v1)
    print("sigma_v* = %2.4f МПа" % sigma_vz)
    print("delta = %2.4f МПа" % d)
    print()
    print("SSc:  "+', '.join(map(lambda x: "%7.3f"%x, ssc)))
    print("TTm1: "+', '.join(map(lambda x: "%7.3f"%x, ttm1)))
    print("TTm2: "+', '.join(map(lambda x: "%7.3f"%x, ttm2)))
    print("")
    print("SS1:   "+', '.join(map(lambda x: "%7.3f"%x, ss1)))
    print("SS3:   "+', '.join(map(lambda x: "%7.3f"%x, ss3)))
    print("SS1_2: "+', '.join(map(lambda x: "%7.3f"%x, ss1_2)))
    print("SS3_2: "+', '.join(map(lambda x: "%7.3f"%x, ss3_2)))
    print("")
    print("Коэффициент корреляции для ttm1 = f(ssc): %0.6f" % pearsonr(ssc, ttm1)[0])
    print("Коэффициент корреляции для ttm2 = f(ssc): %0.6f" % pearsonr(ssc, ttm2)[0])
    print("")
    print("Коэффициенты полинома. Первый - свободный член")
    print(" c[0]     c[1]      c[2]    c[3]     c[4]")
    error = []
    P = None
    min_i = 3
    for i in range(4,0,-1):
        c = np.polyfit(ss3,ss1, i)
        p = np.poly1d(c) # ss1 = f(ss3)
        print(', '.join(map(lambda x: "%7.4f"%x, p.c[::-1])))
        error += [abs(p(ss3[-1])-ss1[-1])]
        if error[-1]<min_error:
            min_i=i
            P = p
        print("Ошибка для ssc5: %3.5f КПа"%(error[-1]*1000))
    print('')
    print("Минимальная допустимая ошибка %7.4f (< %1.4f) КПа для полинома степени %i: "%(error[-min_i]*1000, 1000*min_error, min_i)+
          ', '.join(map(lambda x: "%7.4f"%x, P)))
    plot(ss3, ss1, ss3_2, ss1_2, ssc, ttm1, ttm2, P)


def test_pasp13(verbose = False):
    print("\nПроверка путём сравнения вновь полученных значений Tau и Sigma с уже вычесленными.\n"
          "Последние, а также параметры массива и вычислений заданы как константы. \n")
    ko = 0.11  # коэф. структурного ослабления
    kd = 0.85  # коэф. длительной прочности
    kp = 2.1  # коэф. перегрузки
    ss_c = 120  #67.4  # предел прочности на сжатие [МПа]
    ss_t = 6.3  # предел прочности на растяжение [МПа]
    sigma_v = 16.3  # вертикальное напряжение [МПа]

    min_error = 1/1000  # МПа

    ss_cm = ss_c*kd*ko
    sigma_v1 = 1.5 * ss_cm/2
    sigma_vz = kp * sigma_v if sigma_v > sigma_v1 else sigma_v1
    d = (sigma_vz - ss_cm/2)/5
    ssc = [ss_cm/2 + d*i for i in range(1,6)]

    STT = []
    stt_true = (23.801, 14.716, 15.561) # тру значения. точно тру?
    calc_error = False  # были ли ошибки при разных nt?
    prog_error = False  # были ли ошибки про работе программы?
    from geom_aux import get_ssc_ttm
    for n in range(0, 20):
        nt = n/20+0.001
        if verbose: print(f"nt = {nt:3.4f}")
        s, t1, t2, error = get_ssc_ttm(ss_t,ss_c,ko,kp,S=ssc[0],nt = nt, prog=Pasp13i, verbose=True)
        if error: prog_error = True
        STT += [(s, t1, t2)]
        if error == False:
            if verbose: print("this 'nt' is OK.")
            e = abs(stt_true[0] - s) + abs(stt_true[1] - t1) + abs(stt_true[2] - t2)
            if e > 0.5: calc_error=True
            if verbose: print("sigma_c = %2.3f; tau_m1 = %2.3f; tau_m2 = %2.3f;"%(s, t1, t2) + (" ошибка %2.3f"%e))
            # break
        else: print("error")
    if calc_error: print("\n !!! При некоторых значениях nt были ошибки вычисления !!! ")
    if prog_error: print("\n !!! Были ошибки работы программы !!! ")
    if not calc_error and not prog_error: print("\n [+] Вновь вычисленные значения величин соответствуют проверочным")



# main_5points()
# test_pasp13(verbose=True)

#TODO
# Неупругая зона не образуеться -> Exception: S = sscm/2

# pasp13i не работает при некоторых значениях давления (S ~= 11.34; ) (nt - любое)